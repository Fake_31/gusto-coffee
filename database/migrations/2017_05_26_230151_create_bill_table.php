<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBillTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bill', function (Blueprint $table) {
            $table->increments('id');
            $table->float('amount')->unsigned()->default(0);
            $table->integer('usedpoints')->default(0);
            $table->dateTime('creationdate')->default(null);
            $table->dateTime('refoundingdate')->nullable()->default(null);
            $table->string('details', "500");

            $table->integer('user_id')->unsigned();
            $table->integer('paymentmethod_id')->unsigned()->nullable();
            $table->integer('shop_id')->unsigned()->nullable();
        });

        Schema::table('bill', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('user');
            $table->foreign('paymentmethod_id')->references('id')->on('paymentmethod')
                ->onDelete(\Illuminate\Support\Facades\DB::raw("set null"));
            $table->foreign('shop_id')->references('id')->on('shop')
                ->onDelete(\Illuminate\Support\Facades\DB::raw("set null"));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('bill');
    }
}
