<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsertagTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usertag', function (Blueprint $table) {
            $table->integer('tag_id')->unsigned();
            $table->integer('user_id')->unsigned();

        });

        Schema::table('usertag', function (Blueprint $table) {
            $table->foreign('tag_id')->references('id')->on('tag');
            $table->foreign('user_id')->references('id')->on('user');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('usertag');
    }
}
