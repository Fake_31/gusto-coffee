<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWeekdaylabelTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('weekdaylabel', function (Blueprint $table) {
            $table->string('label');

            $table->integer('weekday_id')->unsigned();
            $table->integer('language_id')->unsigned();
        });

        Schema::table('weekdaylabel', function (Blueprint $table) {
            $table->foreign('weekday_id')->references('id')->on('weekday');
            $table->foreign('language_id')->references('id')->on('language')->onDelete('cascade')->onUpdate("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('weekdaylabel');
    }
}
