<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReservationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservation', function (Blueprint $table) {
            $table->increments('id');
            $table->dateTime('startdate');
            $table->dateTime('enddate');
            $table->boolean('cancelled')->default(false);
            $table->boolean('completed')->default(false);

            $table->integer('workspace_id')->unsigned();
        });

        Schema::table('reservation', function (Blueprint $table) {
            $table->foreign('workspace_id')->references('id')->on('workspace');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('reservation');
    }
}
