<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCreditcardTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('creditcard', function (Blueprint $table) {
            $table->increments('id');
            $table->string('number');
            $table->string('cryptogram');
            $table->integer('month');
            $table->integer('year');

            $table->integer('type_id')->unsigned()->nullable();
            $table->integer('user_id')->unsigned();
        });

        Schema::table('creditcard', function (Blueprint $table) {
            $table->foreign('type_id')->references('id')->on('creditcardtype')
                ->onDelete(\Illuminate\Support\Facades\DB::raw("set null"));

            $table->foreign('user_id')->references('id')->on('user');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('creditcard');
    }
}
