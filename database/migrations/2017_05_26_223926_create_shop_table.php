<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShopTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shop', function (Blueprint $table) {
            $table->increments('id');
            $table->string('label');
            $table->string('address');
            $table->float('latitude');
            $table->float('longitude');
            $table->string('phone', 45);
            $table->boolean('published')->default(false);

            $table->integer('user_id')->unsigned();
            $table->integer('language_id')->unsigned();
        });

        Schema::table('shop', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('user');
            $table->foreign('language_id')->references('id')->on('language');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('shop');
    }
}
