<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServiceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service', function (Blueprint $table) {
            $table->increments('id');
            $table->float('price')->unsigned();
            $table->integer('points')->unsigned();
            $table->integer('studentpoints')->unsigned();
            $table->boolean('optional')->default(false);
            $table->boolean('desactivated')->default(false);
            $table->boolean('included')->default(false);
            $table->string('picto', 2083);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('service');
    }
}
