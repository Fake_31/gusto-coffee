<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBillserviceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('billservice', function (Blueprint $table) {
            $table->integer('bill_id')->unsigned();
            $table->integer('service_id')->unsigned();
            $table->integer('count')->unsigned();
        });

        Schema::table('billservice', function (Blueprint $table) {
            $table->foreign('bill_id')->references('id')->on('bill');
            $table->foreign('service_id')->references('id')->on('service');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('billservice');
    }
}
