<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlackitemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blackitem', function (Blueprint $table) {
            $table->integer('sourceuser_id')->unsigned();
            $table->integer('targetuser_id')->unsigned();
        });

        Schema::table('blackitem', function (Blueprint $table) {
            $table->foreign('sourceuser_id')->references('id')->on('user');
            $table->foreign('targetuser_id')->references('id')->on('user');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('blackitem');
    }
}
