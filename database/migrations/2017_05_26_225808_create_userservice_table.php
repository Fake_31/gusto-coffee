<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserserviceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('userservice', function (Blueprint $table) {
            $table->integer('count')->unsigned();

            $table->integer('user_id')->unsigned();
            $table->integer('service_id')->unsigned();
            $table->integer('shop_id')->unsigned()->nullable();
        });

        Schema::table('userservice', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('user');
            $table->foreign('service_id')->references('id')->on('service');
            $table->foreign('shop_id')->references('id')->on('shop')
                ->onDelete(\Illuminate\Support\Facades\DB::raw("set null"));

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('userservice');
    }
}
