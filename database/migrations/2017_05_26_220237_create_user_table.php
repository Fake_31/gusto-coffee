<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email')->unique();
            $table->string('password');
            $table->string('name')->nullable();
            $table->string('firstname')->nullable();
            $table->string('address')->nullable();
            $table->string('zipcode')->nullable();
            $table->string('companyname')->nullable();
            $table->string('avatar', 2083)->nullable();
            $table->float('notificationdelay')->nullable();
            $table->date('studentdate')->nullable();
            $table->integer('fidelitypoints')->nullable();
            $table->boolean('desactivated')->default(false);
            $table->date('creationdate')->useCurrent();
            $table->boolean('emailchecked')->default(false);
            $table->string('verificationtoken')->nullable();
            $table->string('linkedin')->nullable();

            $table->integer('usertype_id')->unsigned()->nullable();
            $table->integer('country_id')->unsigned()->nullable();
        });

        Schema::table('user', function (Blueprint $table) {
            $table->foreign('usertype_id')->references('id')->on('usertype')
                ->onDelete(\Illuminate\Support\Facades\DB::raw("set null"));

            $table->foreign('country_id')->references('id')->on('country')
                ->onDelete(\Illuminate\Support\Facades\DB::raw("set null"));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user');
    }
}
