<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShopserviceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shopservice', function (Blueprint $table) {
            $table->integer('shop_id')->unsigned()->nullable();
            $table->integer('service_id')->unsigned();
        });

        Schema::table('shopservice', function (Blueprint $table) {
            $table->foreign('shop_id')->references('id')->on('shop')
                ->onDelete(\Illuminate\Support\Facades\DB::raw("set null"));
            $table->foreign('service_id')->references('id')->on('service');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('shopservice');
    }
}
