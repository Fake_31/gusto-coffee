<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMessageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('message', function (Blueprint $table) {
            $table->increments('id');
            $table->string('content', 1000);
            $table->dateTime('creationdate');
            $table->boolean('read');

            $table->integer('sourceuser_id')->unsigned();
            $table->integer('targetuser_id')->unsigned();
        });

        Schema::table('message', function (Blueprint $table) {
            $table->foreign('sourceuser_id')->references('id')->on('user');
            $table->foreign('targetuser_id')->references('id')->on('user');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('message');
    }
}
