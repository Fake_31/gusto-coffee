<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConfigurationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('configuration', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('freehour');
            $table->integer('servicediscount');
            $table->boolean('maintenance')->default(false);
            $table->float('hourprice');
            $table->float('dayprice');
            $table->float('weekprice');
            $table->float('monthprice');
            $table->float('fidelitypointprice');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('configuration');
    }
}
