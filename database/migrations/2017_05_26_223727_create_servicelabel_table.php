<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServicelabelTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('servicelabel', function (Blueprint $table) {
            $table->string('label');
            $table->string('description')->nullable();

            $table->integer('service_id')->unsigned();
            $table->integer('language_id')->unsigned();
        });

        Schema::table('servicelabel', function (Blueprint $table) {
            $table->foreign('service_id')->references('id')->on('service');
            $table->foreign('language_id')->references('id')->on('language');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('servicelabel');
    }
}
