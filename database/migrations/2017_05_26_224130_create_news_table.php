<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('content', 1000);
            $table->string('picture', 2083)->nullable();
            $table->integer('shop_id')->unsigned()->nullable();
            $table->dateTime('creationdate');
            $table->date('eventdate');
        });

        Schema::table('news', function (Blueprint $table) {
            $table->foreign('shop_id')->references('id')->on('shop')
                ->onDelete(\Illuminate\Support\Facades\DB::raw("set null"));
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('news');
    }
}
