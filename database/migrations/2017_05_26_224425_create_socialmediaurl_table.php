<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSocialmediaurlTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('socialmediaurl', function (Blueprint $table) {
            $table->string('url')->nullable();

            $table->integer('shop_id')->unsigned();
            $table->integer('socialmedia_id')->unsigned();
        });

        Schema::table('socialmediaurl', function (Blueprint $table) {
            $table->foreign('shop_id')->references('id')->on('shop');
            $table->foreign('socialmedia_id')->references('id')->on('socialmedia')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('socialmediaurl');
    }
}
