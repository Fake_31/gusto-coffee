<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBillreservationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('billreservation', function (Blueprint $table) {
            $table->integer('bill_id')->unsigned();
            $table->integer('reservation_id')->unsigned();
        });

        Schema::table('billreservation', function (Blueprint $table) {
            $table->foreign('bill_id')->references('id')->on('bill');
            $table->foreign('reservation_id')->references('id')->on('reservation');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('billreservation');
    }
}
