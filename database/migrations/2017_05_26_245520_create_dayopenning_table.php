<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDayopenningTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dayopenning', function (Blueprint $table) {
            $table->boolean('closed')->default(false);
            $table->float('fullhourstart')->unsigned();
            $table->float('fullhourend')->unsigned();
            $table->float('peakhourstart')->unsigned();
            $table->float('peakhourend')->unsigned();

            $table->integer('shop_id')->unsigned()->nullable();
            $table->integer('weekday_id')->unsigned();
        });

        Schema::table('dayopenning', function (Blueprint $table) {
            $table->foreign('shop_id')->references('id')->on('shop')
                ->onDelete(\Illuminate\Support\Facades\DB::raw("set null"));
            $table->foreign('weekday_id')->references('id')->on('weekday')
                ->onDelete('cascade')->onUpdate("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('dayopenning');
    }
}
