<?php

use Illuminate\Database\Seeder;
use App\Util\SeedUtil;
use App\Models\Bill;
use App\Models\BillService;
use App\Models\UserService;


class BillServiceTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$faker = Faker\Factory::create();

		$services        = SeedUtil::getServicesIds();
		$paymentsMethods = SeedUtil::getPaymentsMethodsIds();
		$shops           = SeedUtil::getShopIds();
		$users           = SeedUtil::getUsersId();

		// creer bill

		for ($i = 0; $i <= 15; $i++)
		{

			$user_random          = SeedUtil::random($users);
			$paymentMethod_random = SeedUtil::random($paymentsMethods);
			$shops_random         = SeedUtil::random($shops);

//                dd($user_random. " - " . $paymentMethod_random. " - ". $shops_random);
			$bill = Bill::create([
				"amount"           => rand(15, 400),
				"usedpoints"       => rand(0, 1500),
				"creationdate"     => $faker->dateTimeInInterval('-5 month', 'now'),
				"refoundingdate"   => "",
				"user_id"          => $user_random,
				"shop_id"          => 1,
				"paymentmethod_id" => $paymentMethod_random,
				'details'          => '{"reservations":[],"services":[{"id":3,"quantity":10},{"id":5,"quantity":1}]}'
			]);


			$service_random = SeedUtil::random($services);
			$count          = rand(1, 5);

			BillService::insert([
				"bill_id"    => $bill->id,
				"service_id" => $service_random,
				"count"      => $count
			]);

			$userService = UserService::where(
				[
					"user_id"    => $user_random,
					"shop_id"    => $shops_random,
					"service_id" => $service_random
				]
			);

			if ($userService->count() > 0)
			{

				$userService->update(['count' => DB::raw("count + $count ")]);
			}
			else
			{
				UserService::insert(
					[
						"count"      => $count,
						"user_id"    => $user_random,
						"shop_id"    => $shops_random,
						"service_id" => $service_random
					]
				);
			}
		}
	}

}