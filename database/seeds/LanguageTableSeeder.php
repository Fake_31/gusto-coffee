<?php

use Illuminate\Database\Seeder;
use App\Models\Language;


class LanguageTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        Language::insert([
            [
                "label" => "français",
                "code" => "FR",
            ],
            [
                "label" => "anglais",
                "code" => "GB",
            ],
            [
                "label" => "espagnol",
                "code" => "ES",
            ],
        ]);
    }

}