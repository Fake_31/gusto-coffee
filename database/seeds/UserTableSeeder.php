<?php

use Illuminate\Database\Seeder;
use App\Models\User;


class UserTableSeeder extends Seeder
{
	protected $nbLine = 80;

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$faker = Faker\Factory::create();

		// debug users
		$debugUsers = [
			['usertype_id' => 1, 'email' => 'user', 'name' => 'User'],
			['usertype_id' => 1, 'email' => 'user2', 'name' => 'User2'],
			['usertype_id' => 2, 'email' => 'barman', 'name' => 'Barman'],
			['usertype_id' => 3, 'email' => 'manager', 'name' => 'Manager'],
			['usertype_id' => 4, 'email' => 'admin', 'name' => 'Admin'],
		];

		for ($i = 0; $i < $this->nbLine; $i++)
		{

			// debug
			$email       = isset($debugUsers[ $i ]['email']) ? $debugUsers[ $i ]['email'] : $faker->unique()->email;
			$usertype_id = isset($debugUsers[ $i ]['usertype_id']) ? $debugUsers[ $i ]['usertype_id'] : 1;
			$name        = isset($debugUsers[ $i ]['name']) ? $debugUsers[ $i ]['name'] : $faker->unique()->lastName;

			User::insert([
				"email"             => $email,
				"password"          => md5("test"),
				"name"              => $name,
				"firstname"         => $faker->firstName,
				"address"           => $faker->address,
				"zipcode"           => $faker->postcode,
				"companyname"       => $faker->company,
				"notificationdelay" => $faker->numberBetween(1, 4),
				"studentdate"       => "",
				"fidelitypoints"    => $faker->numberBetween(0, 50),
				"desactivated"      => false,
				"creationdate"      => $faker->dateTimeThisYear(),
				"emailchecked"      => true,
				"verificationtoken" => $this->generateToken($faker),
				"linkedin"          => "http://linkedin.com/".$faker->unique()->lastName,
				"usertype_id"       => $usertype_id,
				"country_id"        => $faker->numberBetween(1, 4),
			]);
		}
	}

	protected function generateToken($faker)
	{
		$int = rand(0, 10);
		if ($int <= 8)
		{
			return "";
		}

		return $faker->randomDigit.$faker->word.$faker->randomAscii.$faker->word.$faker->randomDigit.$faker->word.$faker->randomAscii.$faker->word;
	}

}