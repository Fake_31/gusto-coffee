<?php

use Illuminate\Database\Seeder;
use App\Models\WeekDayLabel;

class WeekDayLabelTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /**
         * Language : [
         *  1 : français
         *  2 : espagnol
         *  3 : anglais
         * ]
         *
         * @return void
         */

        WeekDayLabel::insert([
            [
                "label" => "lundi",
                "weekday_id" => 1,
                "language_id" => 1
            ],
            [
                "label" => "mardi",
                "weekday_id" => 2,
                "language_id" => 1
            ],
            [
                "label" => "mercredi",
                "weekday_id" => 3,
                "language_id" => 1
            ],
            [
                "label" => "jeudi",
                "weekday_id" => 4,
                "language_id" => 1
            ],
            [
                "label" => "vendredi",
                "weekday_id" => 5,
                "language_id" => 1
            ],
            [
                "label" => "samedi",
                "weekday_id" => 6,
                "language_id" => 1
            ],
            [
                "label" => "dimanche",
                "weekday_id" => 7,
                "language_id" => 1
            ],
            [
                "label" => "monday",
                "weekday_id" => 1,
                "language_id" => 2
            ],
            [
                "label" => "tuesday",
                "weekday_id" => 2,
                "language_id" => 2
            ],
            [
                "label" => "wednesday",
                "weekday_id" => 3,
                "language_id" => 2
            ],
            [
                "label" => "thursday",
                "weekday_id" => 4,
                "language_id" => 2
            ],
            [
                "label" => "friday",
                "weekday_id" => 5,
                "language_id" => 2
            ],
            [
                "label" => "saturday",
                "weekday_id" => 6,
                "language_id" => 2
            ],
            [
                "label" => "sunday",
                "weekday_id" => 7,
                "language_id" => 2
            ],
            [
                "label" => "lunes",
                "weekday_id" => 1,
                "language_id" => 3
            ],
            [
                "label" => "martes",
                "weekday_id" => 2,
                "language_id" => 3
            ],
            [
                "label" => "miércoles",
                "weekday_id" => 3,
                "language_id" => 3
            ],
            [
                "label" => "jueves",
                "weekday_id" => 4,
                "language_id" => 3
            ],
            [
                "label" => "viernes",
                "weekday_id" => 5,
                "language_id" => 3
            ],
            [
                "label" => "sábado",
                "weekday_id" => 6,
                "language_id" => 3
            ],
            [
                "label" => "domingo",
                "weekday_id" => 7,
                "language_id" => 3
            ],
        ]);
    }

}