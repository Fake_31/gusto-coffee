<?php

use App\Models\Content;
use Illuminate\Database\Seeder;

/**
 * Created by PhpStorm.
 * User: sam
 * Date: 22/08/2017
 * Time: 18:19
 */
class ContentTableSeeder extends Seeder
{
	public function run()
	{
		Content::insert([
			[
				"contentid"   => "HOME_CONCEPT_1",
				"content"     => "Gusto Coffe est un espace de coworking situé au coeur de Paris regroupant des coworkeurs aux compétences complémentaires. Vous y découvrirez une ambiance convivial, où votre bien-être est notre principale préocupation !",
				"language_id" => 1
			],
			[
				"contentid"   => "HOME_CONCEPT_1",
				"content"     => "Gusto Coffe is a coworking space located in the heart of Paris, with coworkers with complementary skills. You will discover a convivial atmosphere, where your well-being is our main preoccupation !",
				"language_id" => 2
			],
			[
				"contentid"   => "HOME_CONCEPT_2",
				"content"     => "Grâce à notre système de mise en relation sur notre plateforme vous pourrez élargir votre réseau professionnel et rencontrer vos prochains collaborateurs.",
				"language_id" => 1
			],
			[
				"contentid"   => "HOME_CONCEPT_2",
				"content"     => "Thanks to our system of connection on our platform you will be able to enlarge your professional network and to meet your next collaborators.",
				"language_id" => 2
			],
			[
				"contentid"   => "HOME_CONCEPT_3",
				"content"     => "Divers événements sont organisé au sein du café, il peut sagir de café-débat, présentation de projet en crowdfunding, divers ateliers, accompagnement d’indépendant/start-up.",
				"language_id" => 1
			],
			[
				"contentid"   => "HOME_CONCEPT_3",
				"content"     => "Various events are organized within the café, it can be coffee-debate, presentation of project in crowdfunding, various workshops, accompaniment of independent / start-up.",
				"language_id" => 2
			],
			[
				"contentid"   => "HOME_CONCEPT_4",
				"content"     => "Si vous avez des idées d’événements contactez-nous !",
				"language_id" => 1
			],
			[
				"contentid"   => "HOME_CONCEPT_4",
				"content"     => "If you have any event ideas contact us!",
				"language_id" => 2
			],
			[
				"contentid"   => "HOME_CONCEPT_5",
				"content"     => "La réservation peut soit se faire sur internet ou sur place (mais nous ne pourrons pas vous garantir la disponibilité).",
				"language_id" => 1
			],
			[
				"contentid"   => "HOME_CONCEPT_5",
				"content"     => "The reservation can either be made on the internet or on site (but we will not be able to guarantee you the availability).",
				"language_id" => 2
			]
		]);
	}
}