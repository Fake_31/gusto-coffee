<?php

use Illuminate\Database\Seeder;
use App\Models\BlackItem;


class BlackItemTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        $userNb = \App\Models\User::where("usertype_id", 4)->get(["id"]);

        for ($i=1; $i < $userNb->count(); $i++) {

            $sourceuser_id = $faker->numberBetween(1, $userNb->count());
            $targetuser_id = $faker->numberBetween(1, $userNb->count());

            while($sourceuser_id === $targetuser_id) {
                $targetuser_id = $faker->numberBetween(1, $userNb->count());
            }

            BlackItem::insert([
                "sourceuser_id" => $sourceuser_id,
                "targetuser_id" => $targetuser_id
            ]);
        }
    }

}