<?php

use Illuminate\Database\Seeder;
use \App\Util\SeedUtil;
use \App\Models\Message;
use \App\Util\DateUtil;

class MessageTableSeeder extends Seeder
{
    protected $nbLine = 80;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $faker = Faker\Factory::create();

        $users = SeedUtil::getUsersId();
        $now = DateUtil::getFrDateTime("Y-m-d");

        for ($i = 0; $i <= 50; $i++) {

            $usersource_random = $this->getRandom($users);
            $usertarget_random = $this->getRandom($users);

            while ($usersource_random === $usertarget_random) {
                $usersource_random = $this->getRandom($users);
                $usertarget_random = $this->getRandom($users);
            }

            $content = $faker->sentence(8, true);

            Message::create([
                "content" => $content,
                "creationDate" => $now,
                "sourceuser_id" => $usersource_random,
                "targetuser_id" => $usertarget_random,
                "read" => 1
            ]);

        }
    }

    protected function getRandom($users)
    {
        return SeedUtil::random($users);
    }

}