<?php

use Illuminate\Database\Seeder;
use App\Models\Service;

class ServiceTableSeeder extends Seeder
{
	protected $nbLine = 80;

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$faker = Faker\Factory::create();

		Service::insert([
			// Un espace de stockage privé en ligne.
			[
				"price"         => 15,
				"points"        => 2,
				"studentpoints" => null,
				"optional"      => false,
				"desactivated"  => false,
				"included"      => false,
				"picto"         => "/assets/img/icon/cloud.svg",
			],
			// Vente de support numérique.
			[
				"price"         => 10,
				"points"        => 1,
				"studentpoints" => null,
				"optional"      => false,
				"desactivated"  => false,
				"included"      => false,
				"picto"         => "/assets/img/icon/usb_key.svg",
			],
			// Impression.
			[
				"price"         => 0.10,
				"points"        => 0,
				"studentpoints" => null,
				"optional"      => false,
				"desactivated"  => false,
				"included"      => false,
				"picto"         => "/assets/img/icon/printer.svg",
			],
			// Café ou thé à volonté.
			[
				"price"         => 8,
				"points"        => 0,
				"studentpoints" => null,
				"optional"      => false,
				"desactivated"  => false,
				"included"      => true,
				"picto"         => "/assets/img/icon/coffee.svg",
			],
			// Crêpes et gaufres à volonté.
			[
				"price"         => 10,
				"points"        => 1,
				"studentpoints" => null,
				"optional"      => false,
				"desactivated"  => false,
				"included"      => false,
				"picto"         => "/assets/img/icon/waffle.svg",
			],
			// Petit déjeuner.
			[
				"price"         => 4,
				"points"        => 1,
				"studentpoints" => null,
				"optional"      => true,
				"desactivated"  => false,
				"included"      => false,
				"picto"         => "/assets/img/icon/breakfast.svg",
			],
			// Déjeuner.
			[
				"price"         => 14,
				"points"        => 2,
				"studentpoints" => null,
				"optional"      => false,
				"desactivated"  => false,
				"included"      => false,
				"picto"         => "/assets/img/icon/lunch.svg",
			],
			// Dîner.
			[
				"price"         => 18,
				"points"        => 3,
				"studentpoints" => null,
				"optional"      => true,
				"desactivated"  => false,
				"included"      => false,
				"picto"         => "/assets/img/icon/dinner.svg",
			],
		]);
	}

}