<?php

use Illuminate\Database\Seeder;
use App\Models\Tag;


class TagTableSeeder extends Seeder
{
	protected $nbLine = 80;

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$jobs = ['technicien', 'chef', 'conducteur', 'architecte', 'économiste', 'maçon', 'couvreur', 'technicien', 'menuisier', 'serrurier-métallier', 'chargé', 'secrétaire', 'développeur', 'développeur', 'technicien', 'chef', 'chef', 'ingénieur', 'acheteur', 'caissier', 'vendeur', 'vendeur', 'pâtissier', 'boucher', 'agent', 'actuaire', 'agent', 'analyste', 'trader', 'conducteur', 'conducteur', 'technicien', 'paysagiste', 'hôtesse', 'logisticien', 'accompagnateur', 'directeur', 'directeur', 'employé', 'barman', 'designer', 'designer', 'comédien', 'scénariste', 'ingénieur', 'photographe', 'photographe', 'chargé', 'bibliothécaire', 'hôte', 'rédacteur', 'secrétaire', 'enseignant-chercheur', 'professeur', 'professeur', 'enseignant', 'médecin', 'pharmacien', 'pharmacien', 'infirmier', 'auxiliaire', 'masseur', 'orthophoniste', 'technicien', 'visiteur', 'prothésiste', 'esthéticien-cosméticien', 'éducateur', 'éducateur', 'éducateur', 'animateur', 'moniteur', 'accompagnateur', 'sapeur-pompier', 'officier', 'officier', 'sous-officier', 'cartographe', 'ingénieur', 'géographe', 'sportif', 'démographe', 'psychologue', 'greffier', 'toiletteur', 'responsable', 'qualiticien', 'ingénieur', 'verrier', 'conducteur/', 'ingénieur', 'opérateur', 'affûteur', 'soudeur', 'soudeur', 'expert', 'microtechnicien', 'technicien', 'chef', 'monteur', 'technicien', 'technicien', 'technicien', 'technicien', 'technicien', 'ingénieur', 'technicien', 'astrophysicien', 'modéliste', 'cordonnier', 'ébéniste', 'canalisateur', 'éleveur', 'viticulteur', 'technicien', 'dessinateur', 'ingénieur', 'ascensoriste', 'électricien', 'opérateur', 'chef', 'contrôleur', 'assistant', 'technicien', 'technicien', 'technicien', 'commercial', 'secrétaire', 'danseur', 'directeur', 'régisseur', 'animateur', 'ouvrier', 'aquaculteur', 'biologiste', 'ouvrier', 'responsable', 'domoticien', 'techniverrier', 'ingénieur', 'chercheur', 'assistant', 'chef', 'juriste', 'chef', 'formateur', 'hot', 'testeur', 'chercheur', 'technicien', 'ingénieur', 'ingénieur', 'mécanicien', 'gardien', 'militaire', 'rédacteur', 'technicien', 'conseiller', 'économe', 'ingénieur', 'technicien', 'commercial', 'animateur', 'conservateur', 'étalagiste', 'bottier', 'brodeur', 'ferronnier', 'modiste', 'sellier', 'assistant', 'conseiller', 'éducateur', 'infirmier', 'ingénieur', 'courtier', 'souscripteur', 'conseiller', 'mandataire', 'surveillant', 'animateur', 'iconographe', 'géomaticien', 'intégrateur', 'régisseur', 'apiculteur', 'chef', 'conseiller', 'correcteur', 'anesthésiste-réanimateur', 'chargé', 'juge', 'juriste', 'entraîneur', 'fiscaliste', 'agent', 'géologue', 'ingénieur', 'ingénieur'];

		for ($i = 0; $i < count($jobs); $i++)
		{
			try
			{

				Tag::insert([
					"label" => $jobs[ $i ],
				]);
			}
			catch (\Illuminate\Database\QueryException $e)
			{

			}
		}
	}

}