<?php

use Illuminate\Database\Seeder;
use App\Models\UserType;


class UserTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        UserType::insert([
            ["label" => "user"],
            ["label" => "barman"],
            ["label" => "manager"],
            ["label" => "admin"]
        ]);
    }

}