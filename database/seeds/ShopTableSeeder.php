<?php

use Illuminate\Database\Seeder;
use App\Models\Shop;

class ShopTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$manager_id = \App\Util\SeedUtil::getFirstManagersId();

		Shop::insert([
			"label"       => "Gusto Coffee - Paris Nord",
			"address"     => "5 rue Auber, Paris",
			"latitude"    => "48.8711",
			"longitude"   => "2.32985",
			"phone"       => "0556843597",
			"published"   => true,
			"user_id"     => $manager_id->id,
			"language_id" => 1,
		]);
	}

}