<?php

use Illuminate\Database\Seeder;
use App\Models\Country;

class CountryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Country::insert([
            ["label" => "france"],
            ["label" => "espagne"],
            ["label" => "angleterre"],
            ["label" => "amérique"]
        ]);
    }

}