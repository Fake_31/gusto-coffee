<?php

use Illuminate\Database\Seeder;
use App\Models\BillReservation;
use App\Util\SeedUtil;
use App\Models\Bill;
use App\Models\Reservation;


class BillReservationTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$faker = Faker\Factory::create();

		$paymentsMethods = SeedUtil::getPaymentsMethodsIds();
		$users           = SeedUtil::getUsersId();
		$workspaces      = SeedUtil::getWorkspacesIds();

		$now             = new DateTime('now', new DateTimeZone('Europe/Paris'));
		$oneHourAfterNow = clone $now;
		$oneHourAfterNow->add(new DateInterval('PT1H'));

		$debugReservations = [
			[
				'start' => $now,
				'end'   => $oneHourAfterNow,
			]
		];

		for ($i = 0; $i <= 15; $i++)
		{

			$user_random          = SeedUtil::random($users);
			$paymentMethod_random = SeedUtil::random($paymentsMethods);
			$workspace_random     = SeedUtil::random($workspaces);

			$startDate = $faker->dateTimeBetween("-1 months", "+ 1 months");
			$endDate   = clone $startDate;

			$hours = (int) $startDate->format("H");

			while ($hours < 8 || $hours + 1 > 20)
			{
				$startDate = $faker->dateTimeBetween("-1 months", "+ 1 months");
				$hours     = (int) $startDate->format("H");
			}

			$startReservationTime = $startDate->setTime($hours, 0);
			$endReservationTime   = $endDate->setTime($hours + 1, 0);

			// override with debug values
			if (isset($debugReservations[ $i ]['start']))
			{
				$startReservationTime = $debugReservations[ $i ]['start'];
				$endReservationTime   = $debugReservations[ $i ]['end'];
			}

			$isWorkspaceAvailable = SeedUtil::isWorspaceAvailable($workspace_random, $startReservationTime, $endReservationTime);

			if ($isWorkspaceAvailable)
			{
				$now = new \DateTime("now", new DateTimeZone("+2"));

				if ($now > $endReservationTime)
				{
					$completed = true;
				}
				else
				{
					$completed = false;
				}

				$bill = Bill::create([
					"amount"           => rand(15, 400),
					"usedpoints"       => rand(0, 1500),
					"creationdate"     => $faker->dateTimeInInterval('-5 months', 'now'),
					"refoundingdate"   => "",
					"user_id"          => $user_random,
					"paymentmethod_id" => $paymentMethod_random,
					"shop_id"          => 1,
					'details'          => '{"reservations":[],"services":[{"id":3,"quantity":10},{"id":5,"quantity":1}]}'
				]);

				$billReservation = Reservation::create([
					"startdate"    => $startReservationTime,
					"enddate"      => $endReservationTime,
					"cancelled"    => 0,
					"completed"    => $completed,
					"workspace_id" => $workspace_random,
				]);

				BillReservation::insert(
					[
						"bill_id"        => $bill->id,
						"reservation_id" => $billReservation->id,
					]
				);
			}
		}
	}

}