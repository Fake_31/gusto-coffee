<?php

use Illuminate\Database\Seeder;
use App\Models\UserService;
use App\Util\SeedUtil;

class UserServiceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $shops = SeedUtil::getShopIds();
        $services = SeedUtil::getServicesIds();
        $users = SeedUtil::getUsersId();

        
        
        for ($i=0; $i <= 45; $i++) {

            $shop_random = SeedUtil::random($shops);
            $service_random = SeedUtil::random($services);
            $user_random = SeedUtil::random($users);

            $userService = UserService::where(
                [
                    "user_id" => $user_random,
                    "shop_id" => $shop_random,
                    "service_id" => $service_random
                ]
            );
            
            if($userService->count() > 0 ) {

                $userService->update(array('count' => rand(1, 7)));

            } else {
                UserService::insert(
                    [
                        "count" => rand(1,7),
                        "user_id" => $user_random,
                        "shop_id" => $shop_random,
                        "service_id" => $service_random
                    ]
                );
            }




        }
    }

}