<?php

use Illuminate\Database\Seeder;
use App\Models\SocialMedia;

class SocialMediaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        SocialMedia::insert([
            [
                "label" => "facebook",
                "icon" => "assets/img/icon/facebook.svg"
            ],
            [
                "label" => "twitter",
                "icon" => "assets/img/icon/twitter.svg"
            ],
            [
                "label" => "linkedin",
                "icon" => "assets/img/icon/linkedin.svg"
            ],
        ]);

    }

}