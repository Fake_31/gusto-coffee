<?php

use Illuminate\Database\Seeder;
use App\Models\News;

class NewsTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$faker = Faker\Factory::create();

		for ($i = 0; $i <= 15; $i++)
		{
			$eventDate    = $faker->dateTimeThisMonth();
			$creationDate = clone $eventDate;
			$creationDate->sub(new DateInterval('P7D'));

			News::insert([
				"title"        => $faker->text($maxNbChars = 25),
				"content"      => $faker->text($maxNbChars = 300),
				"picture"      => $faker->imageUrl(),
				"shop_id"      => rand(0, 1) > .5 ? 1 : null,
				"eventdate"    => $eventDate,
				"creationdate" => $creationDate,
			]);
		}
	}

}