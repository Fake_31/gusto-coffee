<?php

use Illuminate\Database\Seeder;
use App\Models\ServiceLabel;


class ServiceLabelTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        /**
         * Services : [
         *  1 : Un espace de stockage privé en ligne.
         *  2 : Vente de support numérique.
         *  3 : Impression.
         *  4 : Café ou thé à volonté.
         *  5 : Crêpes et gaufres à volonté.
         *  6 : Petit déjeuner.
         *  7 : Déjeuner.
         *  8 : Dîner.
         * ]
         * Language : [
         *  1 : français
         *  2 : espagnol
         *  3 : anglais
         * ]
         *
         * @return void
         */

        ServiceLabel::insert([
            [
                "label" => "Espace de stockage privé en ligne",
                "description" => null,
                "service_id" => 1,
                "language_id" => 1,
            ],
            [
                "label" => "Vente de support numérique",
                "description" => null,
                "service_id" => 2,
                "language_id" => 1,
            ],
            [
                "label" => "Impression",
                "description" => null,
                "service_id" => 3,
                "language_id" => 1,
            ],
            [
                "label" => "Café ou thé à volonté",
                "description" => null,
                "service_id" => 4,
                "language_id" => 1,
            ],
            [
                "label" => "Crêpes et gaufres à volonté",
                "description" => null,
                "service_id" => 5,
                "language_id" => 1,
            ],
            [
                "label" => "Petit déjeuner",
                "description" => null,
                "service_id" => 6,
                "language_id" => 1,
            ],
            [
                "label" => "Déjeuner",
                "description" => null,
                "service_id" => 7,
                "language_id" => 1,
            ],
            [
                "label" => "Dîner",
                "description" => null,
                "service_id" => 8,
                "language_id" => 1,
            ],
            [
                "label" => "A private online storage space",
                "description" => null,
                "service_id" => 1,
                "language_id" => 2,
            ],
            [
                "label" => "Sale of digital media",
                "description" => null,
                "service_id" => 2,
                "language_id" => 2,
            ],
            [
                "label" => "Printing",
                "description" => null,
                "service_id" => 3,
                "language_id" => 2,
            ],
            [
                "label" => "Coffee or tea at will",
                "description" => null,
                "service_id" => 4,
                "language_id" => 2,
            ],
            [
                "label" => "Pancakes and waffles at will",
                "description" => null,
                "service_id" => 5,
                "language_id" => 2,
            ],
            [
                "label" => "Breakfast",
                "description" => null,
                "service_id" => 6,
                "language_id" => 2,
            ],
            [
                "label" => "Lunch",
                "description" => null,
                "service_id" => 7,
                "language_id" => 2,
            ],
            [
                "label" => "dinner",
                "description" => null,
                "service_id" => 8,
                "language_id" => 2,
            ],
            [
                "label" => "un espacio de almacenamiento en línea privada",
                "description" => null,
                "service_id" => 1,
                "language_id" => 3,
            ],
            [
                "label" => "Venta de los medios digitales",
                "description" => null,
                "service_id" => 2,
                "language_id" => 3,
            ],
            [
                "label" => "Impresión",
                "description" => null,
                "service_id" => 3,
                "language_id" => 3,
            ],
            [
                "label" => "café o té a voluntad",
                "description" => null,
                "service_id" => 4,
                "language_id" => 3,
            ],
            [
                "label" => "crepes y gofres a voluntad",
                "description" => null,
                "service_id" => 5,
                "language_id" => 3,
            ],
            [
                "label" => "desayuno",
                "description" => null,
                "service_id" => 6,
                "language_id" => 3,
            ],
            [
                "label" => "almuerzo doce",
                "description" => null,
                "service_id" => 7,
                "language_id" => 3,
            ],
            [
                "label" => "cena",
                "description" => null,
                "service_id" => 8,
                "language_id" => 3,
            ],
        ]);
    }

}