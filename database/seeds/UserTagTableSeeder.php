<?php

use Illuminate\Database\Seeder;
use App\Models\UserTag;

class UserTagTableSeeder extends Seeder
{
	protected $nbLine = 80;

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$faker = Faker\Factory::create();

		$users = \App\Util\SeedUtil::getUsersId();
		$tags  = \App\Util\SeedUtil::getTagsId();

		foreach ($users as $user)
		{
			UserTag::insert([
				"tag_id"  => $faker->randomElements($tags->toArray())[0]['id'],
				"user_id" => $user->id,
			]);
		}

		foreach ($tags as $tag)
		{
			UserTag::insert([
				"tag_id"  => $tag->id,
				"user_id" => $faker->randomElements($users->toArray())[0]['id'],
			]);
		}
	}

}