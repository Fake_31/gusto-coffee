<?php

use Illuminate\Database\Seeder;
use App\Models\Workspace;

class WorkspaceTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$config = [
			4 => 5,
			6 => 5,
			1 => 120,
		];

		foreach ($config as $placesCount => $workspacesCount)
		{
			$counter = 1;
			for ($i = 0; $i < $workspacesCount; $i++)
			{
				switch ($placesCount)
				{
					default:
						$label = 'Place n°';
						break;
					case 4:
						$label = 'Salon de 4 places n°';
						break;
					case 6:
						$label = 'Salon de 6 places n°';
						break;
				}

				Workspace::insert([
					[
						"label"       => $label.$counter,
						"placescount" => $placesCount,
						"shop_id"     => 1
					]
				]);

				$counter++;
			}
		}
	}

}