<?php

use Illuminate\Database\Seeder;
use App\Models\CreditCard;


class CreditCardTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        $util = new \App\Util\SeedUtil();
        $userNb = \App\Models\User::where("usertype_id", 4)->get(["id"]);

        for ($i=1; $i < $userNb->count() - ($userNb->count()/20); $i++) {

            $cardtype_id = $util->getRandomCreditCardTypeId();

            CreditCard::insert([
                "number" => $faker->creditCardNumber,
                "cryptogram" => $faker->numberBetween(100, 999),
                "month" => $faker->numberBetween(01, 12),
                "year" => $faker->year,
                "type_id" => $cardtype_id,
                "user_id" => $faker->numberBetween($userNb[$i]->id, 1),
            ]);
        }
    }

}