<?php

use Illuminate\Database\Seeder;
use App\Models\Country;

class CreditCardTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\CreditCardType::insert([
            ["label" => "mastercard"],
            ["label" => "visa"]
        ]);
    }

}