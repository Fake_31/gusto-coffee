<?php

use Illuminate\Database\Seeder;
use App\Models\DayOpenning;
use App\Util\SeedUtil;

class DayOpenningTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $weeksDay = SeedUtil::getWeeksDay();

        $fullHoursStart = 8;
        $FullHourEnd = 22;
        $PeakHourStart = 11;
        $PeakHourEnd = 14;
        $closed = false;

        foreach ($weeksDay as $weekday) {

            if($weekday->id == 6) {

                $fullHoursStart = 10;
                $FullHourEnd = 22;
                $PeakHourStart = 13;
                $PeakHourEnd = 17;

            }
            /*if($weekday->id == 7) {

                $fullHoursStart = 0;
                $FullHourEnd = 0;
                $PeakHourStart = 0;
                $PeakHourEnd = 0;
                $closed = true;

            }*/

            DayOpenning::insert([
                [
                    "closed" => $closed,
                    "fullhourstart" => $fullHoursStart,
                    "fullhourend" => $FullHourEnd,
                    "peakhourstart" => $PeakHourStart,
                    "peakhourend" => $PeakHourEnd,
                    "shop_id" => 1,
                    "weekday_id" => $weekday->id
                ]
            ]);

        }
    }

}