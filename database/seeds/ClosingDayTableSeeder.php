<?php

use Illuminate\Database\Seeder;
use App\Models\ClosingDay;

class ClosingDayTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        for ($i=0; $i <= 25; $i++) {
            ClosingDay::insert([
                [
                    "date" => $faker->dateTimeInInterval('-6 month', '+ 12 month'),
                    "shop_id" => 1
                ]
            ]);
        }

    }

}