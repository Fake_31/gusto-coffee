<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{

		$this->call(CountryTableSeeder::class);
		$this->call(PaymentMethodTableSeeder::class);
		$this->call(UserTypeTableSeeder::class);
		$this->call(ConfigurationTableSeeder::class);
		$this->call(CreditCardTypeTableSeeder::class);
		$this->call(UserTableSeeder::class);
		$this->call(CreditCardTableSeeder::class);
		$this->call(BlackItemTableSeeder::class);
		$this->call(MessageTableSeeder::class);
		$this->call(TagTableSeeder::class);
		$this->call(UserTagTableSeeder::class);
		$this->call(ServiceTableSeeder::class);
		$this->call(LanguageTableSeeder::class);
		$this->call(ServiceLabelTableSeeder::class);
		$this->call(ShopTableSeeder::class);
		$this->call(NewsTableSeeder::class);
		$this->call(SocialMediaTableSeeder::class);
		$this->call(SocialMediaUrlTableSeeder::class);
		$this->call(ClosingDayTableSeeder::class);
		$this->call(WeekDayTableSeeder::class);
		$this->call(DayOpenningTableSeeder::class);
		$this->call(WeekDayLabelTableSeeder::class);
		$this->call(WorkspaceTableSeeder::class);
		$this->call(ShopServiceTableSeeder::class);
		$this->call(UserServiceTableSeeder::class);
		$this->call(ReservationTableSeeder::class);
		$this->call(BillServiceTableSeeder::class);
		$this->call(BillReservationTableSeeder::class);
		$this->call(ContentTableSeeder::class);
		//$this->call(BillTableSeeder::class);
	}

}
