<?php

use App\Models\Service;
use Illuminate\Database\Seeder;
use App\Models\ShopService;
use App\Util\SeedUtil;

class ShopServiceTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$shops    = SeedUtil::getShopIds();
		$services = Service::all();

		foreach ($shops as $shop)
		{
			/** @var Service $service */
			foreach ($services as $service)
			{
				if ($service->optional && rand(0, 1) > .5)
				{
					ShopService::insert([
						[
							"shop_id"    => $shop->id,
							"service_id" => $service->id
						]
					]);
				}
			}
		}
	}

}