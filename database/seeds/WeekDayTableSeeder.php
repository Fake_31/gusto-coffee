<?php

use Illuminate\Database\Seeder;
use App\Models\WeekDay;

class WeekDayTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 1; $i <= 7; $i++) {
            WeekDay::insert([
                [
                    "id" => $i,
                ]
            ]);
        }
    }

}