<?php

use Illuminate\Database\Seeder;
use App\Models\SocialMediaUrl;

class SocialMediaUrlTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        /**
         * socialmedia_id : [
         *  1 : facebook
         *  2 : twitter
         *  3 : linkedin
         * ]
         *
         * @return void
         */

        $faker = Faker\Factory::create();
        $name = $faker->lastName;

        SocialMediaUrl::insert([
            [
                "url" => "http://facebook.com/" . $name,
                "shop_id" => 1,
                "socialmedia_id" => 1,
            ],
            [
                "url" => "http://twitter.com/" . $name,
                "shop_id" => 1,
                "socialmedia_id" => 2,
            ],
            [
                "url" => "http://linkedin.com/" . $name,
                "shop_id" => 1,
                "socialmedia_id" => 3,
            ],
        ]);
    }

}