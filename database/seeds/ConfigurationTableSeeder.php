<?php

use Illuminate\Database\Seeder;
use App\Models\Configuration;


class ConfigurationTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Configuration::insert([
			"freehour"           => 3,
			"servicediscount"    => 5,
			"maintenance"        => false,
			"hourprice"          => 4,
			"dayprice"           => 15,
			"weekprice"          => 40,
			"monthprice"         => 120,
			"fidelitypointprice" => .1,
		]);
	}

}