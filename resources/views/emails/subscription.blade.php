<p>Bonjout {{ $name  }} {{ $firstname }},</p>
<p>Vous vous êtes inscrit sur le site de Gusto-Coffee et nous vous en remercions.</p>
<p>Pour commencer à utiliser l'application, vous devez valider votre adresse email en cliquant sur le lien suivant :</p>
<p> <a href="{{ $url }}">{{ $url }}</a></p>