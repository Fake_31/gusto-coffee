<p>Vous avez reçu cet email car une demande de réniatialisation de mot de passe a été faite avec l'addresse suivante : {{ $email }}</p>
<p>Cliquez sur le lien ci dessus pour poursuivre l'opération :</p>
<p> <a href="{{ $url }}">{{ $url }}</a></p>