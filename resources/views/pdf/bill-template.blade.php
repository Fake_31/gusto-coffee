<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Facture</title>
    <style>
        header {
            height: 100px;
        }
        header .companyLogo {
            width: 100%;
        }
        header .companyLogo img {
            max-width:  80px;
        }
        .content .companyName {
            font-weight: bold;
        }
        .content .entity {
            height:150px;
        }
        header .factureNumber {
            float: right;
        }
        .content {
            position: relative;
            height: 100px;
        }
        .content .companyAddress {
            position: absolute;
            top: 0;
            left:0;
        }
        .content .customerAddress {
            position: absolute;
            top: 0;
            right:0;
            text-align: right;
        }
        .content .companyAddress p {
            margin: 0;
            width: 50%;
        }
        .content .customerAddress p {
            margin: 0;
            width: 50%;
        }
        .tg  {border-collapse:collapse;border-spacing:0; word-wrap:break-word;
        }
        .tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}
        .tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}
        .tg .tg-baqh{text-align:center;vertical-align:top}
        .tg .tg-thju{background-color:#ffebd3;text-align:center;vertical-align:top}
        .tg .tg-y0nj{font-weight:bold;background-color:#f8a102;text-align:center;vertical-align:top}
        .tg .tg-xaas{background-color:#ffebd3;vertical-align:top}
        .tg .tg-yw4l{vertical-align:top}

        footer{
            position: absolute;
            bottom: 0;
        }
    </style>
</head>
<body>
<header>
    <div class="companyLogo">
        <img src="https://upload.wikimedia.org/wikipedia/commons/a/ab/Logo_TV_2015.png" alt="">
    </div>

    <div class="factureNumber">
        Facture #1
    </div>
</header>
<div class="content">
    <div class="entity">
        <div class="companyAddress">
            <p class="companyName">
                Gusto Coffee
            </p>
            <p>{{ $shop->address }}</p>
            <p>SIRET 215 5748 651 325</p>
            <p>{{ $shop->phone }}</p>
        </div>
        <div class="customerAddress">
            <p><b>{{ $user->name . " " . $user->firstname }}</b></p>
            <p>{{ $user->address }}</p>
        </div>
    </div>
    <div class="billItems">
        <table class="tg" style="table-layout: fixed; width: 730px">
            <tr>
                <th class="tg-y0nj" width="50%">Désignation</th>
                <th class="tg-y0nj" width="10%">Quantité</th>
            </tr>

            @foreach ($billPdfReservations as $reservation)
                <tr>
                    <td class="tg-xaas" style="max-width: 50px;">Réservation du {{ $reservation["startDate"] }} au {{ $reservation["endDate"] }} </td>
                    <td class="tg-thju">1</td>
                </tr>
            @endforeach

            @foreach ($billPdfServices as $service)
                <tr>
                    <td class="tg-xaas" style="max-width: 50px;">{{ $service["serviceLabel"] }}</td>
                    <td class="tg-thju">{{ $service["count"] }}</td>
                </tr>
            @endforeach
        </table>
    </div>
    <p>Points de fidélités utilisés : {{ $totalBillCount["studentPoint"] }} </p>
    <p>Prix total payé : {{ $totalBillCount["totalPrice"] * 1.2 }} € </p>
</div>


</body>
</html>
