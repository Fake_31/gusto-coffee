# Gusto-Coffee - Serveur API

####### Requirement

* Plugin "mod rewrite_url" d’apache
* Serveur WEB local
* Base de données

## Clone du dépôt
Cloner le dépôt “istef-back” dans le dossier de votre serveur web

* Windows - Wamp 

Dans le dossier www/

* Linux 

Dans un dossier de l’utilisateur puis faire un alias de /var/www/html/istef-back vers le dossier cloné.
 
## Installation des dépendances
 
Se rendre dans le dossier cloné puis exécuter la commande :

~~~
 composer install
~~~

 
## Tests d'accès à l'API

Accéder au site web local http://localhost/istef-back/public
 
Réponse : 

~~~
{"response":{"message":"Laravel Framework Lumen (5.4.6) (Laravel Components 5.4.*)"}}
~~~
 
## Base de données

#### Paramétrage
 
Dupliquer le fichier .env.example en nommant le nouveau fichier “.env”
Ce fichier permet de paramétrer des modules (BDD, route d’API …)

Modifier les paramètres d'accès à la base de données locale :

~~~
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=gusto
DB_USERNAME=root
DB_PASSWORD=
~~~

Il est aussi possible de paramétrer l'url de l'api avec la propriété

~~~
API_DEFAULT_ROUTE=/api/v1
~~~

#### Création de la base de données
(Création d'une dase de données "gusto" en une ligne de commande. Si vous l'avez appelé différement, il vous faut la créée manuellement)
~~~
php artisan db:create 
~~~

#### Migrations

Exécuter les migrations (création des tables dans la base de données)

~~~
php artisan migrate
~~~

#### Seeds

Exécuter les seeds (Ajout de données fake dans les tables)

~~~
php artisan db:seed  
~~~

#### Réinitialiser la base
~~~
php artisan migrate:refresh --seed
~~~

#### Démarer le serveur mail
~~~
php artisan queue:listen
~~~


#### Swagger
Documentation : https://github.com/DarkaOnLine/SwaggerLume
~~~
php artisan swagger-lume:publish 
php artisan swagger-lume:generate 
~~~



#### Tests unitaires
 
Exécuter la commande suivante à la racine du projet.

~~~
phpunit
~~~
  
Si ça ne fonctionne pas, éxécutez à la place

~~~
vendor/bin/phpunit
~~~

Résultat : 

~~~       
PHPUnit 5.7.19 by Sebastian Bergmann and contributors.

.                                                                   1 / 1 (100%)

Time: 32 ms, Memory: 6.00MB

OK (1 test, 1 assertion)
~~~

### Erreurs relevées
Sous linux, il peut y avoir un problème (HTTP erreur 500) de droit sur le dossier storage/, il faut donner l’accès à l’utilisateur “apache” sur ce dossier : 

~~~
sudo chmod -R 0770 storage
~~~