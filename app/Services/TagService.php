<?php

/**
 * Created by PhpStorm.
 * User: kev
 * Date: 08/06/17
 * Time: 22:43
 */

namespace App\Services;

use App\Exceptions\EntityException;
use \App\Models\BlackItem;

class TagService extends BaseService
{
    public static function getBySource($sourceuser_id)
    {
        return BlackItem::where("sourceuser_id", $sourceuser_id)
            ->get();
    }

    public static function getBySourceTarget($sourceuser_id, $targetuser_id)
    {
        return BlackItem::where([
            "sourceuser_id" => $sourceuser_id,
            "targetuser_id" => $targetuser_id
        ])
            ->get();
    }

    public static function searchBySourceTarget($sourceuser_id, $targetuser_id)
    {
        return BlackItem::where([
            "sourceuser_id" => $sourceuser_id,
            "targetuser_id" => $targetuser_id
        ]);
    }

    public static function store($params)
    {
        if(self::exist($params)) {
            throw new EntityException(409);
        }

        $blackItem = new BlackItem;
        $blackItem->fill($params)
            ->save();

        return $blackItem;
    }

    private static function exist($params)
    {
        $count = self::getBySourceTarget($params['sourceuser_id'], $params['targetuser_id'])->count();

        if($count == 0) {
            return false;
        }

        return true;
    }
}