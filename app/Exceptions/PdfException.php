<?php
/**
 * Created by PhpStorm.
 * User: kev
 * Date: 21/05/17
 * Time: 19:49
 */

namespace App\Exceptions;

class PdfException extends \Exception
{
    public function __construct($code = 403, $entityErrorCode)
    {
        $this->code = $code;
        $this->entityErrorCode = $entityErrorCode;
        $this->message = "error". $entityErrorCode;
        $this->entity = "pdf";

    }

    public function getEntity()
    {
        return $this->entity;
    }

    public function getEntityErrorCode()
    {
        return $this->entityErrorCode;
    }


}