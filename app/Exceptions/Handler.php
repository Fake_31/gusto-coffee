<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Database\QueryException;
use Laravel\Lumen\Exceptions\Handler as ExceptionHandler;
use App\Helpers\ApiHelper as API;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
//        AuthorizationException::class,
//        HttpException::class,
//        ModelNotFoundException::class,
//        ValidationException::class,
        BillCancelException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception $e
     * @return void
     */
    public function report(Exception $e)
    {
//        dd($e);
        if ($e instanceof QueryException ||
            $e instanceof NotFoundHttpException ||
            $e instanceof PdfException ||
            $e instanceof UserException ||
            $e instanceof MethodNotAllowedHttpException ||
            $e instanceof BillException
        ) {
            return;
        } else if ($e instanceof ReservationException) {
            return;
        }

        parent::report($e);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Exception $e
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {
//        dd($e);
        if ($e instanceof QueryException) {
            switch ($e->getCode()) {
                //QueryException code(23000) == foreign key problem
                case "23000":
                    $status = 409;
                    break;
                //QueryException code(1062) == Duplicate unique value for column
                case "1062":
                    return API::responseError(500);
                    break;
                case "0":
                    return API::responseError(500);
                    break;
                default:
                    $status = 555;
            }
        } else if ($e instanceof MethodNotAllowedHttpException ) {
            $status = 404;
        } else if ($e instanceof NotFoundHttpException ||
            $e instanceof BillCancelException ||
            $e instanceof ReservationException  ||
            $e instanceof PdfException ||
            $e instanceof UserException ||
            $e instanceof MethodNotAllowedHttpException ||
            $e instanceof BillException
        ) {
            $status = $e->getCode();
        }

        $errorCode = isset($e->entityErrorCode) ? $e->entityErrorCode : null;
        $entity = isset($e->entity) ? $e->entity : null;

        return API::responseError($status, $entity, $errorCode);

    }

}
