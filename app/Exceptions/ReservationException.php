<?php
/**
 * Created by PhpStorm.
 * User: kev
 * Date: 24/05/17
 * Time: 00:56
 */

namespace App\Exceptions;

use App\Helpers\ApiHelper as API;

class ReservationException extends \Exception
{
    /**
     * BillCancelException constructor.
     */
    public function __construct($code = 0)
    {
        $this->code = $code;
        $this->message = "error".$code;

    }
}