<?php
/**
 * Created by PhpStorm.
 * User: kev
 * Date: 17/05/17
 * Time: 10:06
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Shop extends Model
{
    public $timestamps = false;
    protected $table = 'shop';

    protected $fillable = ['label', 'address', 'latitude', 'longitude', 'phone', 'published', 'user_id', 'language_id'];
}