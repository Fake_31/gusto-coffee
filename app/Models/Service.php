<?php
/**
 * Created by PhpStorm.
 * User: kev
 * Date: 17/05/17
 * Time: 10:06
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{

    public $timestamps = false;
    protected $table = 'service';

    protected $fillable = ['price', 'points', 'studentpoints', 'optional', 'desactived', 'included', 'picto'];


}