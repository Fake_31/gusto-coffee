<?php
/**
 * Created by PhpStorm.
 * User: kev
 * Date: 17/05/17
 * Time: 10:06
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ServiceLabel extends Model
{

    public $timestamps = false;
    protected $table = 'servicelabel';

    protected $primaryKey = null;
    public $incrementing = false;

    protected $fillable = ['label', 'description', 'service_id', 'language_id'];


}