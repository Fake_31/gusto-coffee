<?php
/**
 * Created by PhpStorm.
 * User: kev
 * Date: 17/05/17
 * Time: 10:06
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Reservation extends Model
{

    public $timestamps = false;
    protected $table = 'reservation';

    protected $fillable = ['startDate', 'endDate', 'cancelled', 'completed', 'workspace_id'];
}