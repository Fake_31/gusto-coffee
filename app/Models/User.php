<?php
/**
 * Created by PhpStorm.
 * User: kev
 * Date: 18/05/17
 * Time: 22:37
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{

    public $updated_at = null;
    public $created_at = null;
    public $timestamps = false;

    protected $table = 'user';

    protected $fillable = ['id', 'email', 'password', 'name', 'firstname', 'address', 'zipcode', 'companyname', 'avatar', 'notificationdelay', 'studentdate', 'fidelitypoints', 'desactivated',
        'creationdate', 'emailchecked', 'verificationtoken', 'usertype_id', 'country_id', 'linkedin'];

}