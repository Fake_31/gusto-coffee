<?php
/**
 * Created by PhpStorm.
 * User: kev
 * Date: 17/05/17
 * Time: 10:06
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CreditCardType extends Model
{
    public $timestamps = false;
    protected $table = 'creditcardtype';


    protected $fillable = ['label'];
}