<?php
/**
 * Created by PhpStorm.
 * User: kev
 * Date: 17/05/17
 * Time: 10:06
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Content extends Model
{
    public $timestamps = false;
    protected $table = 'content';

    protected $primaryKey = 'contentid';
    public $incrementing = false;

    protected $fillable = ['contentid', 'content', 'language_id'];
}