<?php
/**
 * Created by PhpStorm.
 * User: kev
 * Date: 17/05/17
 * Time: 10:06
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @SWG\Definition(required={"name", "BillReservation"}, type="object", @SWG\Xml(name="BillReservation"))
 */
class BillReservation extends Model
{
    public $timestamps = false;
    protected $table = 'billreservation';
    protected $fillable = ['bill_id', 'reservation_id'];

    protected $primaryKey = null;
    public $incrementing = false;

    /**
     * @SWG\Property(property="bill_id", format="integer")
     * @var integer
     * @property bill_id
     */

    /**
     * @SWG\Property(property="reservation_id", format="integer")
     * @var integer
     * @property reservation_id
     */
}