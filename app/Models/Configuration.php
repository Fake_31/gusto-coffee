<?php
/**
 * Created by PhpStorm.
 * User: kev
 * Date: 17/05/17
 * Time: 10:06
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Configuration extends Model
{
    public $timestamps = false;
    protected $table = 'configuration';

    protected $fillable = ['freehour', 'servicediscount', 'maintenance', 'hourprice', 'dayprice', 'weekprice', 'monthprice', 'fidelitypointprice'];
}