<?php
/**
 * Created by PhpStorm.
 * User: kev
 * Date: 17/05/17
 * Time: 10:06
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    public $created_at = "creationdate";
    protected $table = 'message';


    protected $fillable = ['content', 'creationdate', 'sourceuser_id', 'targetuser_id', 'read'];

    public function setUpdatedAt($value)
    {
    }

    public function getUpdatedAtColumn()
    {
    }
}