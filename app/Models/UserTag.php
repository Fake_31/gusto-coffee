<?php
/**
 * Created by PhpStorm.
 * User: kev
 * Date: 17/05/17
 * Time: 10:06
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserTag extends Model
{

    public $timestamps = false;
    protected $table = 'usertag';

    protected $primaryKey = null;
    public $incrementing = false;

    protected $fillable = ['tag_id', 'user_id'];

}