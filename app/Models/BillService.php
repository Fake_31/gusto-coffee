<?php
/**
 * Created by PhpStorm.
 * User: kev
 * Date: 17/05/17
 * Time: 10:06
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
/**
 * @SWG\Definition(required={"name", "BillService"}, type="object", @SWG\Xml(name="BillService"))
 */
class BillService extends Model
{
    public $timestamps = false;
    protected $table = 'billservice';
    protected $fillable = ['bill_id', 'service_id', 'count'];

    protected $primaryKey = null;
    public $incrementing = false;

    /**
     * @SWG\Property(property="bill_id", format="integer")
     * @var integer
     * @property bill_id
     */
    /**
     * @SWG\Property(property="service_id", format="integer")
     * @var integer
     * @property service_id
     */
    /**
     * @SWG\Property(property="count", format="integer")
     * @var integer
     * @property count
     */
}