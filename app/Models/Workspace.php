<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Workspace extends Model
{
    public $timestamps = false;
    protected $table = 'workspace';

    protected $fillable = ['label', 'placescount', 'shop_id'];

}
?>