<?php
/**
 * Created by PhpStorm.
 * User: kev
 * Date: 17/05/17
 * Time: 10:06
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @SWG\Definition(required={"name", "Country"}, type="object", @SWG\Xml(name="Country"))
 */
class Country extends Model
{
    public $timestamps = false;
    protected $table = 'country';

    protected $fillable = ['label'];

    /**
     * @SWG\Property(property="label", format="string")
     * @var string
     * @property label
     */
}