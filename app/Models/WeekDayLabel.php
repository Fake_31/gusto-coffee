<?php
/**
 * Created by PhpStorm.
 * User: kev
 * Date: 17/05/17
 * Time: 10:06
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WeekDayLabel extends Model
{

    public $timestamps = false;
    protected $table = 'weekdaylabel';

    protected $primaryKey = null;
    public $incrementing = false;

    protected $fillable = ['label', 'weekday_id', 'language_id'];

}