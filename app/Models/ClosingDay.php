<?php
/**
 * Created by PhpStorm.
 * User: kev
 * Date: 17/05/17
 * Time: 10:06
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ClosingDay extends Model
{
    public $timestamps = false;
    protected $table = 'closingday';

    protected $fillable = ['date', 'shop_id'];
}