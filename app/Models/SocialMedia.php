<?php
/**
 * Created by PhpStorm.
 * User: kev
 * Date: 17/05/17
 * Time: 10:06
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SocialMedia extends Model
{

    public $timestamps = false;
    protected $table = 'socialmedia';

    protected $fillable = ['label', 'icon'];

}