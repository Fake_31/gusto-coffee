<?php

namespace App\Models;

use App\Jobs\SendMail;

/**
 * Created by PhpStorm.
 * User: kev
 * Date: 31/05/17
 * Time: 18:54
 */
class Mail
{
    protected $title;
    protected $content;
    protected $contentVariables;
    protected $template;
    protected $from;
    protected $to;

    /**
     * Mail constructor.
     */
    public function __construct()
    {

    }

    /**
     * @return mixed
     */
    public function getContentVariables()
    {
        return $this->contentVariables;
    }

    /**
     * @param mixed $contentVariables
     */
    public function setContentVariables($contentVariables)
    {
        $this->contentVariables = $contentVariables;
    }


    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param mixed $description
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * @return mixed
     */
    public function getTemplate()
    {
        return $this->template;
    }

    /**
     * @param mixed $template
     */
    public function setTemplate($template)
    {
        $this->template = $template;
    }

    /**
     * @return mixed
     */
    public function getFrom()
    {
        return $this->from;
    }

    /**
     * @param mixed $from
     */
    public function setFrom($from)
    {
        $this->from = $from;
    }

    /**
     * @return mixed
     */
    public function getTo()
    {
        return $this->to;
    }

    /**
     * @param mixed $to
     */
    public function setTo($to)
    {
        $this->to = $to;
    }



}