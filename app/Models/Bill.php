<?php
/**
 * Created by PhpStorm.
 * User: kev
 * Date: 17/05/17
 * Time: 10:06
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @SWG\Definition(required={"name", "Bill"}, type="object", @SWG\Xml(name="Bill"))
 */
class Bill extends Model
{
    public $timestamps = false;
    protected $table = 'bill';

    protected $fillable = ['id', 'amount', 'usedpoints', 'creationdate', 'refoundingdate', 'details', 'user_id', 'paymentmethod_id', "shop_id"];

    /**
     * @SWG\Property(property="amount", format="float")
     * @var float
     * @property amount
     */

    /**
     * @SWG\Property(property="usedpoints", format="float")
     * @var float
     * @property usedpoints
     */

    /**
     * @SWG\Property(property="creationdate", format="date")
     * @var string
     * @property creationdate
     */

    /**
     * @SWG\Property(property="refoundingdate", format="date")
     * @var string
     * @property refoundingdate
     */

    /**
     * @SWG\Property(property="details", format="string")
     * @var string
     * @property shop_id
     */

    /**
     * @SWG\Property(property="user_id", format="integer")
     * @var integer
     * @property user_id
     */

    /**
     * @SWG\Property(property="paymentmethod_id", format="integer")
     * @var integer
     * @property paymentmethod_id
     */

    /**
     * @SWG\Property(property="shop_id", format="integer")
     * @var integer
     * @property shop_id
     */

}