<?php
/**
 * Created by PhpStorm.
 * User: kev
 * Date: 17/05/17
 * Time: 10:06
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WeekDay extends Model
{

    public $timestamps = false;
    protected $table = 'weekday';

    protected $fillable = ['id'];

    protected $primaryKey = "id";
    public $incrementing = false;

}