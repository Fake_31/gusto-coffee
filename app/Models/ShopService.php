<?php
/**
 * Created by PhpStorm.
 * User: kev
 * Date: 17/05/17
 * Time: 10:06
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ShopService extends Model
{

    public $timestamps = false;
    protected $table = 'shopservice';

    protected $primaryKey = null;
    public $incrementing = false;

    protected $fillable = ['shop_id', 'service_id'];

}