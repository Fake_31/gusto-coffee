<?php
/**
 * Created by PhpStorm.
 * User: kev
 * Date: 17/05/17
 * Time: 10:06
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @SWG\Definition(required={"name", "BlackItem"}, type="object", @SWG\Xml(name="BlackItem"))
 */
class BlackItem extends Model
{
    public $timestamps = false;
    protected $table = 'blackitem';

    protected $primaryKey = null;
    public $incrementing = false;

    protected $fillable = ['sourceuser_id', 'targetuser_id'];

    /**
     * @SWG\Property(property="sourceuser_id", format="integer")
     * @var integer
     * @property sourceuser_id
     */
    /**
     * @SWG\Property(property="targetuser_id", format="integer")
     * @var integer
     * @property targetuser_id
     */
}