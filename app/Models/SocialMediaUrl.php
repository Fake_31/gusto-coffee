<?php
/**
 * Created by PhpStorm.
 * User: kev
 * Date: 17/05/17
 * Time: 10:06
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SocialMediaUrl extends Model
{

    public $timestamps = false;
    protected $table = 'socialmediaurl';

    protected $primaryKey = null;
    public $incrementing = false;

    protected $fillable = ['url', 'shop_id', 'socialmedia_id'];

}