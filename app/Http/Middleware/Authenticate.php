<?php

/**
 * this class allow checking user access right to a given route
 * multiple access levels are defined, corresponding to application architecture
 * anonymous < user < barman < manager < admin
 */

namespace App\Http\Middleware;

use App\Helpers\ApiHelper as API;
use App\Models\User;
use App\Util\Encryptor;
use Closure;
use Illuminate\Http\Request;

class Authenticate
{
	// sent by client app on each request
	const API_TOKEN_KEY = 'Api-Token';

	// values fit user type id
	const ACCESS_LEVEL_ANONYMOUS = 'anonymous'; // use string here instead of 0 to handle a bug in Lumen middleware argument
	const ACCESS_LEVEL_USER      = 1;
	const ACCESS_LEVEL_BARMAN    = 2;
	const ACCESS_LEVEL_MANAGER   = 3;
	const ACCESS_LEVEL_ADMIN     = 4;

	/**
	 * Handle an incoming request.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @param \Closure                 $next
	 * @param int                      $neededAccessLevel
	 * @return mixed
	 */
	public function handle(Request $request, Closure $next, $neededAccessLevel)
	{
		$neededAccessLevel = $this->getIntFromAccessLevel($neededAccessLevel);

		// get token
		$token = $this->getToken($request);
		if (!isset($token))
		{
			// refuse request
			return $this->sendErrorResponse($neededAccessLevel);
		}

		// get user access level
		$userAccessLevel = $this->getAccessLevelFromToken($token);

		// check user access level
		// if user access level is not high enough
		if ($userAccessLevel < $neededAccessLevel)
		{
			// refuse request
			return $this->sendErrorResponse($neededAccessLevel);
		}

		// else let request pass
		return $next($request);
	}

	protected function getToken(Request $request)
	{
		// from header
		if ($request->hasHeader(self::API_TOKEN_KEY))
		{
			return $request->header(self::API_TOKEN_KEY);
		}
		// from query string
		elseif (isset($_GET[self::API_TOKEN_KEY]))
		{
			return $_GET[self::API_TOKEN_KEY];
		}
		return null;
	}

	protected function sendErrorResponse($neededAccessLevel)
	{
		return API::responseError(401, 'auth', $neededAccessLevel);
	}

	protected function getIntFromAccessLevel($accessLevel)
	{
		return $accessLevel === self::ACCESS_LEVEL_ANONYMOUS ? 0 : (int) $accessLevel;
	}

	protected function getAccessLevelFromToken($token)
	{
		// decode token
		try
		{
			$decodedString = Encryptor::decrypt($token);
		}
		catch (\Exception $e)
		{
			return -1;
		}

		// check integrity
		$parts = explode('_', $decodedString);
		if (count($parts) !== 3)
		{
			return -1;
		}

		// handle special anonymous case
		return $this->getIntFromAccessLevel($parts[1]);
	}

	public static function createTokenFromUser(User $user)
	{
		$stringToEncode = $user->id.'_'.$user->usertype_id.'_'.time();
		return Encryptor::encrypt($stringToEncode);
	}
}
