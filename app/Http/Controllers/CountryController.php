<?php

namespace App\Http\Controllers;

use App\Models\Country;
use App\Models\WeekDay;
use App\Util\ApiUtil;
use App\Helpers\ApiHelper as API;


class CountryController extends Controller
{

    /**
     * @SWG\Get(
     *     path="/country",
     *     summary="Finds all Country",
     *     tags={"Country"},
     *     description="Finds all Country",
     *     operationId="getCountry",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="Api-Token",
     *         in="header",
     *         type="string",
     *         format="string"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="OK"
     *     ),
     *     @SWG\Response(
     *         response="403",
     *         description="Country does not exist"
     *     ),
     *     deprecated=false
     * )
     *
     * @SWG\Get(
     *     path="/country/{country_id}",
     *     summary="Finds a country by id",
     *     tags={"Country"},
     *     description="Finds a country by id",
     *     operationId="getCountry",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         description="ID of country to return",
     *         in="path",
     *         name="country_id",
     *         required=true,
     *         type="integer",
     *         format="int64"
     *     ),
     *     @SWG\Parameter(
     *         name="Api-Token",
     *         in="header",
     *         type="string",
     *         format="string"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Ok"
     *     ),
     *     @SWG\Response(
     *         response="403",
     *         description="Country does not exist",
     *     ),
     *     deprecated=false
     *
     * )
     */
    public function getCountry($country_id = false)
    {
        if ($country_id) {

            $validator = ApiUtil::validate(["id" => $country_id], [
                "id" => "required|numeric|min:1"
            ]);

            if ($validator->fails()) {
                return API::responseErrorForm($validator);
            }

            $country = Country::find($country_id);

        } else {
            $country = Country::all();
        }

        return API::response($country);

    }

    /**
     * @SWG\Post(
     *     path="/country",
     *     tags={"Country"},
     *     operationId="add country",
     *     summary="Add a new country",
     *     description="",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         description="Value of the country",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/Country"),
     *     ),
     *     @SWG\Parameter(
     *         name="Api-Token",
     *         in="header",
     *         type="string",
     *         format="string"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="OK"
     *     ),
     *     @SWG\Response(
     *         response=422,
     *         description="invalid entity",
     *     ),
     * )
     * )
     */
    public function storeCountry()
    {
        $params = json_decode(file_get_contents('php://input'));

        $validator = ApiUtil::validate($params, [
            "label" => "required|string|min:1",
        ]);

        if ($validator->fails()) {
            return API::responseErrorForm($validator);
        }

        $country = new Country;
        $country->fill((array)$params);
        $country->save();

        return API::response($country);
    }

    /**
     * @SWG\Put(
     *     path="/country",
     *     tags={"Country"},
     *     operationId="update country",
     *     summary="Update an existing country",
     *     description="",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         description="Country object",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/Country"),
     *     ),
     *     @SWG\Parameter(
     *         name="Api-Token",
     *         in="header",
     *         type="string",
     *         format="string"
     *     ),
     *     @SWG\Response(
     *         response=400,
     *         description="Invalid ID supplied",
     *     ),
     *     @SWG\Response(
     *         response=404,
     *         description="Pet not found",
     *     ),
     *     @SWG\Response(
     *         response=405,
     *         description="Validation exception",
     *     ),
     *     security={{"petstore_auth":{"write:pets", "read:pets"}}}
     * )
     */
    public function updateCountry($country_id)
    {
        $params = json_decode(file_get_contents('php://input'));
        $objectFromRoute = ApiUtil::getParamsUrlRequest($params);

        $validator = ApiUtil::validate($objectFromRoute, [
            "label" => "required|string|min:1",
        ]);

        if ($validator->fails()) {
            return API::responseErrorForm($validator);
        }

        $country = Country::find($country_id);

        if ($country) {

            $country->update([
                "label" => $objectFromRoute->label
            ]);

            return API::response($country);

        } else {

            return API::responseError(400);

        }
    }

    /**
     * @SWG\Delete(
     *     path="/country/{country_id}",
     *     summary="Deletes a country",
     *     description="",
     *     operationId="deleteCountry",
     *     produces={"application/json"},
     *     tags={"Country"},
     *     @SWG\Parameter(
     *         description="Country id to delete",
     *         in="path",
     *         name="country_id",
     *         required=true,
     *         type="integer",
     *         format="int64"
     *     ),
     *     @SWG\Parameter(
     *         name="api_key",
     *         in="header",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=400,
     *         description="Invalid ID supplied"
     *     ),
     *     @SWG\Response(
     *         response=404,
     *         description="Pet not found"
     *     ),
     *     security={{"petstore_auth":{"write:pets", "read:pets"}}}
     * )
     */
    public function deleteCountry($country_id)
    {
        dd("test");
        $validator = ApiUtil::validate(["id" => $country_id], [
            "id" => "required|numeric|min:1"
        ]);

        if ($validator->fails()) {
            return API::responseErrorForm($validator);
        }

        $country = Country::find($country_id);

        if (!$country) {

            return API::response();

        }
        $deleted = $country->delete();

        if (!$deleted) {

            return API::responseError(304);

        }

        return API::response($country);

    }

}
