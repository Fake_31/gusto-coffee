<?php

namespace App\Http\Controllers;

use App\Helpers\ApiHelper as API;
use App\Models\Configuration;
use App\Util\ApiUtil;

class ConfigurationController extends Controller
{
    public function getConfiguration()
    {

        $configuration = Configuration::first();

        return API::response($configuration);

    }

    public function updateConfiguration()
    {

        $params = json_decode(file_get_contents('php://input'));

        $validator = ApiUtil::validate($params, [
            "freehour" => "int",
            "servicediscount" => "int",
            "maintenance" => "boolean",
            "hourprice" => "numeric",
            "dayprice" => "numeric",
            "weekprice" => "numeric",
            "monthprice" => "numeric",
            "fidelitypointprice" => "numeric"
        ]);

        if ($validator->fails()) {
            return API::responseErrorForm($validator);
        }

        $configuration = Configuration::find(1);

        $configuration->fill((array)$params)
            ->save();

        return API::response($configuration);

    }

}