<?php

namespace App\Http\Controllers;

use App\Models\SocialMedia;
use App\Util\ApiUtil;
use App\Helpers\ApiHelper as API;


class SocialMediaController extends Controller
{
    public function getSocialMedia($socialmedia_id = false)
    {

        if ($socialmedia_id) {

            $validator = ApiUtil::validate(["id" => $socialmedia_id], [
                "id" => "required|numeric|min:1"
            ]);

            if ($validator->fails()) {
                return API::responseErrorForm($validator);
            }

            $socialmedia = SocialMedia::find($socialmedia_id);

        } else {
            $socialmedia = SocialMedia::all();
        }

        return API::response($socialmedia);

    }

    public function storeSocialMedia()
    {
        $params = json_decode(file_get_contents('php://input'));

        $validator = ApiUtil::validate($params, [
            "label" => "required|string|min:1",
            "icon" => "required|string|min:1",
        ]);

        if ($validator->fails()) {
            return API::responseErrorForm($validator);
        }
        $socialmedia = new SocialMedia();
        $socialmedia->fill((array)$params);
        $socialmedia->save();

        return API::response($socialmedia);
    }

    public function updateSocialMedia($id)
    {
        $params = json_decode(file_get_contents('php://input'));
        $objectFromRoute = ApiUtil::getParamsUrlRequest($params);

        $validator = ApiUtil::validate($objectFromRoute, [
            "label" => "required|string|min:1",
            "icon" => "required|string|min:1",
        ]);

        if ($validator->fails()) {
            return API::responseErrorForm($validator);
        }

        $socialmedia = SocialMedia::find($id);

        if ($socialmedia) {

            $socialmedia->fill((array)$objectFromRoute);
            $socialmedia->save();

            return API::response($socialmedia);

        } else {

            return API::responseError(400);

        }
    }

    public function deleteSocialMedia($id)
    {
        $params = json_decode(file_get_contents('php://input'));
        $objectFromRoute = ApiUtil::getParamsUrlRequest($params);

        $validator = ApiUtil::validate($objectFromRoute, [
            "socialmedia_id" => "required|int|min:1",
        ]);

        if ($validator->fails()) {
            return API::responseErrorForm($validator);
        }

        $socialmedia = SocialMedia::find($id);

        if (!$socialmedia) {

            return API::response();

        }

        $deleted = $socialmedia->delete();

        if (!$deleted) {

            return API::responseError(304);

        }

        return API::response($socialmedia);

    }

}
