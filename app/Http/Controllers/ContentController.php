<?php

namespace App\Http\Controllers;

use App\Models\Content;
use App\Util\ApiUtil;
use App\Util\ArrayUtil;
use App\Helpers\ApiHelper as API;

class ContentController extends Controller
{
    public function getContents($language_id = null, $content_id = null)
    {
        if ($language_id && !$content_id) {

            $validator = ApiUtil::validate([
                "language_id" => $language_id,
            ], [
                "language_id" => "required|int|min:1",
            ]);

            if ($validator->fails()) {
                return API::responseErrorForm($validator);
            }

            $content = Content::where([
                "language_id" => $language_id
            ])
                ->get();

        } elseif ($language_id && $content_id) {

            $objectFromRoute = ApiUtil::getParamsUrlRequest();

            $validator = ApiUtil::validate($objectFromRoute, [
                "language_id" => "required|int|min:1",
                "contentid" => "required|int|string",
            ]);

            if ($validator->fails()) {
                return API::responseErrorForm($validator);
            }

            $content = Content::where([
                "language_id" => $language_id,
                "contentid" => $content_id
            ])
                ->first();

        } else {

            $content = Content::all();

        }


        return API::response($content);
    }

    public function deleteContent($language_id, $content_id)
    {
        $validator = ApiUtil::validate([
            "language_id" => $language_id,
            "contentid" => $content_id
        ], [
            "language_id" => "required|int|min:1",
            "contentid" => "required|int|string",
        ]);

        if ($validator->fails()) {
            return API::responseErrorForm($validator);
        }

        $content = Content::where([
            "language_id" => $language_id,
            "contentid" => $content_id
        ])
            ->first();

        if ($content) {
            $content->delete();
            return API::response($content);
        }

        return API::responseError(400);
    }


    public function updateContent($language_id, $content_id)
    {
        $params = json_decode(file_get_contents('php://input'));
        $params = ArrayUtil::merge($params, [
            "language_id" => $language_id,
            "contentid" => $content_id,
        ]);

        $validator = ApiUtil::validate($params, [
            "language_id" => "required|int|min:1",
            "contentid" => "required|int|string",
            "content" => "required|string",
        ]);

        if ($validator->fails()) {
            return API::responseErrorForm($validator);
        }

        $content = Content::where([
            "language_id" => $language_id,
            "contentid" => $content_id,
        ]);

        $contentSearched = $content
            ->first()
            ->fill((array)$params);

        $content->update($contentSearched->toArray());

        return API::response($contentSearched);
    }

    public function storeContent($language_id, $content_id)
    {
        $params = json_decode(file_get_contents('php://input'));
        $params = ArrayUtil::merge($params, [
            "language_id" => $language_id,
            "contentid" => $content_id,
        ]);

        $validator = ApiUtil::validate($params, [
            "language_id" => "required|int|min:1",
            "contentid" => "required|int|string",
            "content" => "required|string",
        ]);

        if ($validator->fails()) {
            return API::responseErrorForm($validator);
        }

        $content = new Content;
        $content->fill((array)$params)
            ->save();

        return API::response($content);

    }
}