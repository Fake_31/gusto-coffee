<?php

namespace App\Http\Controllers;

use App\Models\UserTag;
use App\Util\ApiUtil;
use Illuminate\Http\Request;
use App\Helpers\ApiHelper as API;

class UserTagController extends Controller
{
    public function getUserTag(Request $request, $user_id = false, $tag_id = false)
    {

        $objectFromRoute = ApiUtil::getParamsUrlRequest();

        if ($user_id && !$tag_id) {

            $validator = ApiUtil::validate($objectFromRoute, [
                "user_id" => "required|int|min:1",
            ]);

            if ($validator->fails()) {
                return API::responseErrorForm($validator);
            }

            $userTag = UserTag::where(
                "user_id", $user_id
            )
                ->get();

        } elseif ($user_id && $tag_id) {

            $validator = ApiUtil::validate($objectFromRoute, [
                "user_id" => "required|int|min:1",
                "tag_id" => "required|int|min:1",
            ]);

            if ($validator->fails()) {
                return API::responseErrorForm($validator);
            }

            $userTag = UserTag::where([
                "user_id" => $user_id,
                "tag_id" => $tag_id
            ])
                ->first();

        } else {

            $params = $this->jsonParams($request);

            if(isset($params->user_ids)) {

                $validator = ApiUtil::validate($params, [
                    "user_ids" => "required|array|min:1",
                ]);

                if ($validator->fails()) {
                    return API::responseErrorForm($validator);
                }

                $userTag = UserTag::whereIn(
                    "user_id", $params->user_ids
                )
                    ->get();

            } else {
                $userTag = UserTag::All();
            }

        }


        return API::response($userTag);

    }

    public function storeUserTag()
    {
        /*
         * Todo : Check if userTag alreadyExist
         */

        $params = ApiUtil::getParamsUrlRequest();

        $validator = ApiUtil::validate($params, [
            "user_id" => "required|int|min:1",
            "tag_id" => "required|int|min:1",
        ]);

        if ($validator->fails()) {
            return API::responseErrorForm($validator);
        }

        $userTag = new UserTag;
        $userTag->fill((array)$params);

        if (!$userTag->save()) {
            return API::responseError(400);
        }

        return API::response($userTag);

    }

    public function deteleUserTag($user_id, $tag_id)
    {
        $validator = ApiUtil::validate([
            "user_id" => $user_id,
            "tag_id" => $tag_id
        ], [
            "user_id" => "required|int|min:1",
            "tag_id" => "required|int|min:1",
        ]);

        if ($validator->fails()) {
            return API::responseErrorForm($validator);
        }

        $userTag = UserTag::where([
            "user_id" => $user_id,
            "tag_id" => $tag_id
        ]);

        $userTagSearched = $userTag->first();

        if ($userTag->delete() == 0) {
            return API::response();
        }

        return API::response($userTagSearched);

    }

}