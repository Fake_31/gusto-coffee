<?php

namespace App\Http\Controllers;

use App\Helpers\ApiHelper as API;
use App\Models\Language;
use App\Util\ApiUtil;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

class LanguageController extends Controller
{

    public function getAll()
    {
        $languages = Language::all();

        return API::response($languages);
    }

    public function getLanguage($languageId)
    {
        $paramsRequest = ApiUtil::getParamsUrlRequest();

        $validator = ApiUtil::validate($paramsRequest, [
            "id" => "required|numeric|min:0",
        ]);

        if ($validator->fails()) {
            return API::responseErrorForm($validator);
        }

        $language = Language::find($languageId);

        return API::response($language);
    }

    public function storeLanguage()
    {

        $params = json_decode(file_get_contents('php://input'));
        $label = $params->label;
        $code = $params->code;

        $validator = ApiUtil::validate($params, [
            "label" => "required|string|min:1",
            "code" => "required|string|min:2|max:2",
        ]);

        if ($validator->fails()) {
            return API::responseErrorForm($validator);
        }


        try {

            $language = new Language;
            $language->code = $code;
            $language->label = $label;

            $language->save();

        } catch (QueryException $e) {

            return API::responseError(409);

        }

        return API::response($language);

    }

    public function updateLanguage(Request $request, $languageId)
    {
        $params = json_decode(file_get_contents('php://input'));
        $label = $params->label;

        $validator = ApiUtil::validate($params, [
            "label" => "required|string|min:1",
        ]);

        if ($validator->fails()) {
            return API::responseErrorForm($validator);
        }

        $language = Language::find($languageId);

        if ($language) {

            $language->label = $label;

            try {

                $language->save();
                return API::response($language);

            } catch (QueryException $e) {

                return API::responseError(409);

            }

        } else {

            return API::responseError(206);

        }

    }

    public function deleteLanguage($languageId)
    {
        $paramsRequest = ApiUtil::getParamsUrlRequest();

        $validator = ApiUtil::validate($paramsRequest, [
            "id" => "required|numeric|min:0",
        ]);

        if ($validator->fails()) {
            return API::responseErrorForm($validator);
        }

        /*
            Todo suppression en cascade des clefs étrangères
        */

        $language = Language::find($languageId);

        if ($language) {

            try {

                $language->delete();
                return API::response($language);

            } catch (QueryException $e) {

                return API::responseError(409);

            }

        } else {

            return API::responseError(400);

        }
    }

    public function getLanguageFromCode(Request $request)
    {
        $params = $this->jsonParams($request);

        $validator = ApiUtil::validate($params, [
            "code" => "required|string|min:1",
        ]);

        if ($validator->fails()) {
            return API::responseErrorForm($validator);
        }

        $code = $params->code;

        $language = Language::where([
            "code" => $code
        ])->first();

        return API::response($language);

    }

}