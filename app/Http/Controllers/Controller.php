<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;

/**
 * @SWG\Swagger(
 *     schemes={"http"},
 *     host="gusto-coffee-back",
 *     basePath="/api/v1",
 *     @SWG\Info(
 *         version="1.0.0",
 *         title="Swagger Gusto-Coffee",
 *         description="This is the documentation for the Gusto-Coffe API",
 *  )
 * )
 */

class Controller extends BaseController
{

    public function jsonParams($request) {

        return json_decode($request->get("params"));

    }
}
