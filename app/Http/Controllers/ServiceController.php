<?php

namespace App\Http\Controllers;

use App\Exceptions\UserServiceException;
use App\Models\Service;
use App\Models\UserService;
use App\Util\ApiUtil;
use Illuminate\Http\Request;
use App\Helpers\ApiHelper as API;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class ServiceController extends Controller
{

	public function getShopsService(Request $request)
	{
		$params = $this->jsonParams($request);

		$validator = ApiUtil::validate($params, [
			"shop_id" => "sometimes|int|min:1",
		]);

		if ($validator->fails())
		{
			return API::responseErrorForm($validator);
		}

		$shop_id = isset($params->shop_id) ? $params->shop_id : false;

		$services = Service::select('service.*');

		if ($shop_id)
		{
			$services
				->leftJoin('shopservice', function ($join)
				{
					$join->on('service.id', 'shopservice.service_id');
				})
				->where('service.optional', 0)
				->orWhere("shop_id", $shop_id)
				->groupBy('service.id')
			;
		}

		$services = $services->get();

		return API::response($services);
	}

	public function getService($id)
	{

		$validator = Validator::make(["id" => $id], [
			'id' => 'required|integer',
		]);

		if ($validator->fails())
		{
			return API::validationError($validator);
		}

		$services = Service::find($id);

		return API::response($services);
	}

	public function storeService()
	{
		$params = json_decode(file_get_contents('php://input'));

		$validator = ApiUtil::validate($params, [
			"price"         => "required|numeric|min:1",
			"points"        => "required|int|min:1",
			"studentpoints" => "required|int|min:1",
			"optional"      => "required|boolean",
			"desactivated"  => "required|boolean",
			"included"      => "required|boolean",
			"picto"         => "required|string",
		]);

		if ($validator->fails())
		{
			return API::responseErrorForm($validator);
		}

		$service = new Service;
		$service->fill((array) $params);
		$service->save();

		return API::response($service);
	}

	public function updateService($id)
	{
		$params          = json_decode(file_get_contents('php://input'));
		$objectFromRoute = ApiUtil::getParamsUrlRequest($params);

		$validator = ApiUtil::validate($objectFromRoute, [
			"price"         => "numeric|min:1",
			"points"        => "int|min:1",
			"studentpoints" => "int|min:1",
			"optional"      => "boolean",
			"desactivated"  => "boolean",
			"included"      => "boolean",
			"picto"         => "string",
		]);

		if ($validator->fails())
		{
			return API::responseErrorForm($validator);
		}

		$service = Service::find($id);

		if ($service)
		{

			$service->fill((array) $objectFromRoute);
			$service->save();

			return API::response($service);
		}
		else
		{

			return API::responseError(400);
		}
	}

	public function deleteService($id)
	{
		/*
		 * Todo :  Delete cascade
		 */

		$validator = Validator::make(["id" => $id], [
			'id' => 'required|integer|min:1',
		]);

		if ($validator->fails())
		{
			return API::responseErrorForm($validator);
		}

		$service = Service::find($id);

		if (!$service)
		{

			return API::response();
		}

		$deleted = $service->delete();

		if (!$deleted)
		{

			return API::responseError(304);
		}

		return API::response($service);
	}

	public function getUserServices(Request $request)
	{
		$params = $this->jsonParams($request);

		$validator = ApiUtil::validate($params, [
			"shop_id" => "required|int|min:1",
			"user_id" => "required|int|min:1",
		]);

		if ($validator->fails())
		{
			return API::responseErrorForm($validator);
		}

		$shop_id = $params->shop_id;
		$user_id = $params->user_id;

		$services = Service::leftJoin('userservice', function ($join)
		{
			$join->on('service.id', 'userservice.service_id');
		})
		                   ->where([
			                   "userservice.shop_id" => $shop_id,
			                   "userservice.user_id" => $user_id
		                   ])
		                   ->where("userservice.count", ">", 0)
		                   ->get([
			                   "userservice.service_id",
			                   "userservice.count AS quantity",
		                   ]);

		return API::response($services);
	}

	public function consumeService()
	{
		$params = json_decode(file_get_contents('php://input'));

		$validator = ApiUtil::validate($params, [
			"shop_id"  => "required|int|min:1",
			"user_id"  => "required|int|min:1",
			"services" => "required|array|min:1",
		]);

		if ($validator->fails())
		{
			return API::responseErrorForm($validator);
		}

		$shop_id  = $params->shop_id;
		$user_id  = $params->user_id;
		$services = $params->services;

		DB::beginTransaction();

		try
		{

			foreach ($services as $service)
			{

				$userService = UserService::where([
					"user_id"    => $user_id,
					"shop_id"    => $shop_id,
					"service_id" => $service->service_id
				])
				                          ->update([
					                          "count" => DB::raw('userservice.count - '.$service->quantity),
				                          ]);

				if ($userService == 0)
				{

					throw new UserServiceException();
				}
			}
		}
		catch (UserServiceException $e)
		{

			return API::responseError(400);
		}


		DB::commit();

		return API::response(true);
	}


}