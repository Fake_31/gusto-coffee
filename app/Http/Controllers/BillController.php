<?php

namespace App\Http\Controllers;

use App\Exceptions\BillException;
use App\Models\Bill;
use App\Models\BillReservation;
use App\Models\BillService;
use App\Models\Reservation;
use App\Helpers\ApiHelper as API;
use App\Models\Service;
use App\Models\ServiceLabel;
use App\Models\Shop;
use App\Models\UserService;
use App\Models\User;
use App\Util\ApiUtil;
use App\Util\DateUtil;
use App\Util\PdfUtil;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;

class BillController extends Controller
{

    const RESERVATION_CANCELLED = 1;

    /**
     * @SWG\Get(
     *     path="/bill",
     *     summary="Finds all Bills",
     *     tags={"Bill"},
     *     description="Finds all Bills",
     *     operationId="getBills",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="Api-Token",
     *         in="header",
     *         type="string",
     *         format="string"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="OK"
     *     ),
     *     deprecated=false
     * )
     *
     *
     **/
    public function getBills(Request $request)
    {
        $params = $this->jsonParams($request);

        $validator = ApiUtil::validate($params, [
            "user_id" => "sometimes|int|min:1",
            "limit" => "sometimes|int|min:5",
        ]);

        if ($validator->fails()) {
            return API::responseErrorForm($validator);
        }

        $user_id = isset($params->user_id) ? $params->user_id : false;
        $limit = isset($params->limit) ? $params->limit : false;

        $billList = Bill::orderBy("creationdate", "DESC");

        if ($user_id) {
            $billList = $billList->where("user_id", $user_id);
        }

        if ($limit) {
            $billList = $billList->take($limit);
        }

        $billList = $billList->get();

        return API::response($billList);

    }

    /**
     * @SWG\Get(
     *     path="/bill/{bill_id}",
     *     summary="Finds a bill_id by id",
     *     tags={"Bill"},
     *     description="Finds a bill by id",
     *     operationId="getBill",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         description="ID of bill to return",
     *         in="path",
     *         name="bill_id",
     *         required=true,
     *         type="integer",
     *         format="int64"
     *     ),
     *     @SWG\Parameter(
     *         name="Api-Token",
     *         in="header",
     *         type="string",
     *         format="string"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Ok"
     *     ),
     *     @SWG\Response(
     *         response="403",
     *         description="bill does not exist",
     *     ),
     *     deprecated=false
     *
     * )
     **/
    public function getBill($bill_id)
    {

        $validator = ApiUtil::validate(["bill_id" => $bill_id], [
            'bill_id' => 'required|integer',
        ]);

        if ($validator->fails()) {
            return API::validationError($validator);
        }

        return PdfUtil::streamPdf($bill_id);

    }

    /**
     * @SWG\Get(
     *     path="/bill/user/recurrent/{user_id}",
     *     summary="Finds a user by id",
     *     tags={"Bill"},
     *     description="Finds a user by id",
     *     operationId="getRecurrentOrder",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         description="Get recurrent order for user",
     *         in="path",
     *         name="user_id",
     *         required=true,
     *         type="integer",
     *         format="int64"
     *     ),
     *     @SWG\Parameter(
     *         name="Api-Token",
     *         in="header",
     *         type="string",
     *         format="string"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Ok"
     *     ),
     *     deprecated=false
     *
     * )
     **/
    public function getRecurrentOrder($user_id)
    {
        $validator = ApiUtil::validate(["user_id" => $user_id], [
            "user_id" => "required|int|min:1"
        ]);

        if ($validator->fails()) {
            return API::responseErrorForm($validator);
        }

        $result = DB::select("SELECT COUNT(details) as count, details FROM `bill` WHERE user_id = $user_id GROUP BY details ORDER BY count DESC limit 1");

        // if user has not made any order yet
        // return most recurrent of other users
        if (empty($result)) {
            $result = DB::select("SELECT COUNT(details) as count, details FROM `bill` GROUP BY details ORDER BY count DESC limit 1");
        }

        $details = $result ? $result[0]->details : "";

        return API::response($details);

    }

    /**
     * @SWG\Post(
     *     path="/bill",
     *     tags={"Bill"},
     *     operationId="storeBill",
     *     summary="Add a new bill",
     *     description="",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         description="Value of the bill",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/Bill"),
     *     ),
     *     @SWG\Parameter(
     *         name="Api-Token",
     *         in="header",
     *         type="string",
     *         format="string"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="OK"
     *     ),
     *     @SWG\Response(
     *         response=422,
     *         description="invalid entity",
     *     ),
     * )
     * )
     */


    public function storeBill()
    {
        $params = json_decode(file_get_contents('php://input'));

        $validator = ApiUtil::validate($params, [
            "ranges" => "sometimes|array",
            "services" => "sometimes|array",
            "workspace_id" => "nullable|int|min:1",
            "shop_id" => "required|int|min:1",
            "usedpoints" => "required|int|min:0",
            "paymentmethod_id" => "required|int|min:1",
            "user_id" => "required|int|min:1",
            "amount" => "required|numeric|min:0.1",
            "details" => "nullable|string",
        ]);

        if ($validator->fails()) {
            return API::responseErrorForm($validator);
        }

        $ranges = $params->ranges;
        $services = $params->services;
        $workspace_id = $params->workspace_id;
        $usedpoints = $params->usedpoints;
        $user_id = $params->user_id;
        $shop_id = $params->shop_id;
        $params->creationdate = DateUtil::getFrDateTime();
        $details = isset($params->details) ? $params->details : "";

        // check at least one service or one reservation is present
        if (count($services) === 0 && count($ranges) === 0) {
            API::responseError();
        }

        $bill = new Bill;
        $bill->fill((array)$params);

        $bill = Bill::create($bill->toArray());
        if (count($ranges) > 0) {

            $billReservations = [];
            $billPdfReservations = [];

            foreach ($ranges as $range) {

                $reservation = new Reservation;
                $reservation->startDate = $range->start;
                $reservation->endDate = $range->end;
                $reservation->workspace_id = $workspace_id;

                $reservationId = Reservation::create($reservation->toArray())->id;

                $datetime1 = new \DateTime($reservation->startDate);
                $datetime2 = new \DateTime($reservation->endDate);

                $interval = $datetime2->diff($datetime1);
                $intervalHours = $interval->format('%h');

                array_push($billPdfReservations, [
                    "startDate" => $reservation->startDate,
                    "endDate" => $reservation->endDate,
                    "workspace_id" => $reservation->workspace_id,
                    "nb_heures" => $intervalHours,
                ]);

                array_push($billReservations, array(
                    "bill_id" => $bill->id,
                    "reservation_id" => $reservationId
                ));

            }

            BillReservation::insert($billReservations);

        }

        if (count($services) > 0) {

            $billServices = [];

            $userServices = UserService::where([
                "user_id" => $bill->user_id,
                "shop_id" => $shop_id
            ])
                ->get();

            foreach ($services as $service) {

                $userHaveService = false;
                foreach ($userServices as $userService) {
                    if ($userService->service_id === $service->id) {
                        $userHaveService = true;
                        break;
                    }
                }

                // If user have service, so increment it with $service->quantity
                if ($userHaveService) {
                    $userService = UserService::where("user_id", $bill->user_id)
                        ->where("shop_id", $shop_id)
                        ->where("service_id", $service->id)
                        ->update(array('count' => DB::raw("count+" . $service->quantity)));

                    // Else create news line in BDD
                } else {

                    $userService = new UserService;
                    $userService->user_id = $bill->user_id;
                    $userService->service_id = $service->id;
                    $userService->shop_id = $shop_id;
                    $userService->count = $service->quantity;

                    UserService::create($userService->toArray());

                }

                array_push($billServices, array(
                    "bill_id" => $bill->id,
                    "service_id" => $service->id,
                    "count" => $service->quantity
                ));
            }

            $totalBillCount = [
                "studentPoint" => 0,
                "totalPrice" => 0
            ];

            $billPdfServices = [];

            foreach ($billServices as $billService) {
                $serviceLabel = ServiceLabel::leftJoin('service', function ($join) {
                    $join->on('service.id', 'servicelabel.service_id');
                })
                    ->where("service_id", $billService["service_id"])
                    ->first(["label"]);

                array_push($billPdfServices, [
                    "count" => $billService["count"],
                    "serviceLabel" => $serviceLabel->label
                ]);

            }
            $shop = Shop::find($shop_id);
            $user = User::find($user_id);

            $totalBillCount["studentPoint"] = $usedpoints;
            $totalBillCount["totalPrice"] = $bill->amount;
            
            // update user fidelity points
	        $earnedPoints = 0;
	        foreach ($services as $service)
	        {
		        $serviceEntity = Service::find($service->id);
		        $earnedPointsByUnit = $serviceEntity->points;
		        $earnedPoints += $earnedPointsByUnit * $service->quantity;
	        }

	        $delta = (int) ($earnedPoints - $usedpoints);
	        if ($delta !== 0)
	        {
	        	$user = User::find($user_id);
	        	$user->fidelitypoints += $delta;
	        	$user->save();
	        }

            // Generate Bill PDF
            $html = View::make('pdf.bill-template', [
                "billPdfServices" => $billPdfServices,
                "totalBillCount" => $totalBillCount,
                "billPdfReservations" => $billPdfReservations,
                "shop" => $shop,
                "user" => $user,
            ])->render();
            PdfUtil::savePdf($bill->id, $html);

            BillService::insert($billServices);

        };

        return API::response($bill);

    }

    /**
     * @SWG\Put(
     *     path="/bill/cancel/{bill_id}",
     *     tags={"Bill"},
     *     operationId="update bill",
     *     summary="Cancel existing bill",
     *     description="",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="bill_id",
     *         in="path",
     *         description="bill_id",
     *         required=true,
     *         type="integer",
     *         @SWG\Schema(ref="#/definitions/Bill"),
     *     ),
     *     @SWG\Parameter(
     *         name="Api-Token",
     *         in="header",
     *         type="string",
     *         format="string"
     *     ),
     *     @SWG\Response(
     *         response=400,
     *         description="Invalid ID supplied",
     *     ),
     *     @SWG\Response(
     *         response=405,
     *         description="Validation exception",
     *     ),
     * )
     */
    public function cancelBill($id)
    {

        $validator = ApiUtil::validate(["id" => $id], [
            "id" => "required|numeric|min:1"
        ]);

        if ($validator->fails()) {
            return API::responseErrorForm($validator);
        }

        // Get all services to refound
        $bill = Bill::find($id);

        if ($bill) {

            // If bill is already canceled
            if ($bill->refoundingdate != null) {
                throw new BillException(403, 1);
            }

            // If there is details (services bought)
            $servicesBought = BillService::leftJoin('bill', function ($join) {
                $join->on('bill.id', 'billservice.bill_id');
            })
                ->where("bill.id", $id)
                ->get(["service_id", "count"]);

            try {

                DB::beginTransaction();

                // Then for each decrement service count. If one is < 0, so order can't be refounded.
                // If BillException (a UserService is < 0 ) so rollback and send error to client
                foreach ($servicesBought as $service) {

                    $userService = UserService::where([
                        "user_id" => $bill->user_id,
                        "service_id" => $service->service_id,
                        "shop_id" => $bill->shop_id,
                    ]);

                    $userServiceSearched = $userService->first();

                    if (($userServiceSearched->count - (int)$service->count) < 0) {
                        // 02 User does not have service count to refound
                        throw new BillException(403, 2);
                    }

                    $newValue = $userServiceSearched->fill([
                        "count" => $userServiceSearched->count - $service->count
                    ]);

                    $userService->update($newValue->toArray());

                }

                // Get all reservation
                $reservation = Reservation::leftJoin('billreservation', function ($join) {
                    $join->on('billreservation.reservation_id', 'reservation.id');
                })
                    ->leftJoin('bill', function ($join) {
                        $join->on('bill.id', 'billreservation.bill_id');
                    })
                    ->where("bill.id", $bill->id);

                $reservationSearched = $reservation->get(["reservation.*"]);

                // Check if one of the reservation are past
                foreach ($reservationSearched AS $res) {
                    $reservationStartDate = DateUtil::getFrDateTime("Y-m-d H:m:s", $res->startdate, true);
                    $now = DateUtil::getFrDateTime();

                    if ($reservationStartDate < $now) {
                        // 03 One of the reservation already started of finished
                        throw new BillException(403, 3);
                    }
                }

                $reservation->update([
                    "cancelled" => 1
                ]);

                // database queries here
                DB::commit();
            } catch (BillException $e) {

                DB::rollback();
                throw new BillException(403, $e->getEntityErrorCode());

            }

            // Then refund fidelity points used
            if ($bill->usedpoints > 0) {

                $user = User::find($bill->user_id);

                if ($user) {

                    $user->fidelitypoints += $bill->usedpoints;
                    $user->save();

                }

            }

            // Set bill refoundingdate date at now
            $bill->refoundingdate = DateUtil::getFrDateTime(false, false, false, "+2");
            $bill->save();


        }

        return API::response($bill);

    }

}