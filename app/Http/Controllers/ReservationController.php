<?php

namespace App\Http\Controllers;

use App\Exceptions\ReservationException;
use App\Helpers\ApiHelper as API;
use App\Models\Reservation;
use App\Util\ApiUtil;
use App\Util\DateUtil;
use Illuminate\Database\DetectsDeadlocks;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class ReservationController extends Controller
{
    const RESERVATION_COMPLETED = 1;

    const RESERVATION_UNCOMPLETED = 0;

    const RESERVATION_CANCELED = 1;

    const RESERVATION_UNCANCELED = 0;


	public function getReservations()
	{
		return API::response(Reservation::all());
    }

	public function getReservation($reservationId)
	{
		return API::response(Reservation::find($reservationId));
    }

    public function getFutureReservations(Request $request)
    {
        $params = $this->jsonParams($request);

        $validator = ApiUtil::validate($params, [
            "user_id" => "int|min:1",
            "shop_id" => "int|min:1",
        ]);

        if ($validator->fails()) {
            return API::responseErrorForm($validator);
        }

        $user_id = isset($params->user_id) ? $params->user_id : false;
        $shop_id = isset($params->shop_id) ? $params->shop_id : false;

        $reservations = Reservation::leftJoin('billreservation', function ($join) {
            $join->on('reservation.id', 'billreservation.reservation_id');
        })
            ->leftJoin('bill', function ($join) {
                $join->on('bill.id', 'billreservation.bill_id');
            })
            ->leftJoin('workspace', function ($join) {
                $join->on('reservation.workspace_id', 'workspace.id');
            })
            ->where("reservation.startdate", ">", DB::raw('now()'))
            ->where("cancelled", self::RESERVATION_UNCANCELED);

        if ($user_id) {
            $reservations = $reservations->where("bill.user_id", $user_id);
        }
        if ($shop_id) {
            $reservations = $reservations->where("workspace.shop_id", $shop_id);
        }

        $reservations->orderBy('reservation.startdate', 'ASC');

        $reservations = $reservations->get([
            "reservation.*"
        ]);

        return API::response($reservations);


    }

    public function getExceededReservations(Request $request)
    {

        $params = $this->jsonParams($request);

        $validator = ApiUtil::validate($params, [
            "shop_id" => "required|int|min:1"
        ]);

        if ($validator->fails()) {
            return API::responseErrorForm($validator);
        }

        $shop_id = $params->shop_id;

        $reservations = Reservation::leftJoin('workspace', function ($join) {
            $join->on('reservation.workspace_id', 'workspace.id');
        })
            ->where([
                "workspace.shop_id" => $shop_id,
                "reservation.completed" => self::RESERVATION_UNCOMPLETED
            ])
            ->where("enddate", "<", DB::raw('now()'))
            ->get([
                "reservation.*"
            ]);

        return API::response($reservations);

    }

    public function getCurrentUserReservation(Request $request)
    {
        $params = $this->jsonParams($request);

        $validator = ApiUtil::validate($params, [
            "user_id" => "required|int|min:1",
            "shop_id" => "required|int|min:1",
        ]);

        if ($validator->fails()) {
            return API::responseErrorForm($validator);
        }

        $user_id = $params->user_id;
        $shop_id = $params->shop_id;

        $reservation = Reservation::leftJoin('billreservation', function ($join) {
            $join->on('reservation.id', 'billreservation.reservation_id');
        })
            ->leftJoin('bill', function ($join) {
                $join->on('bill.id', 'billreservation.bill_id');
            })
            ->leftJoin('workspace', function ($join) {
                $join->on('reservation.workspace_id', 'workspace.id');
            })
            ->where("reservation.startdate", "<", DB::raw('now()'))
            ->where("reservation.enddate", ">", DB::raw('now()'))
            ->where([
                "workspace.shop_id" => $shop_id,
                "bill.user_id" => $user_id,
                "reservation.cancelled" => self::RESERVATION_UNCANCELED,
                "reservation.completed" => self::RESERVATION_UNCOMPLETED,
            ])
            ->first([
                "reservation.*"
            ]);
        return API::response($reservation, 200, true);
    }

    public function setCurrentReservationCompleted()
    {

        $params = json_decode(file_get_contents('php://input'));

        $validator = ApiUtil::validate($params, [
            "workspace_id" => "required|int|min:1",
        ]);

        if ($validator->fails()) {
            return API::responseErrorForm($validator);
        }

        $workspace_id = $params->workspace_id;

        $reservation = Reservation::where("workspace_id", $workspace_id)
            ->where("reservation.startdate", "<", DB::raw('now()'))
            ->where("reservation.enddate", ">", DB::raw('now()'))->first();

        if ($reservation->completed == 1) {

            throw new ReservationException(400);

        } else {

            $reservation->update([
                "completed" => self::RESERVATION_COMPLETED
            ]);

        }

        return API::response($reservation);
    }

    public function getIncompleteReservations(Request $request)
    {

        $params = $this->jsonParams($request);

        $validator = ApiUtil::validate($params, [
            "user_id" => "int|min:1",
            "shop_id" => "int|min:1",
        ]);

        $user_id = isset($params->user_id) ? $params->user_id : false;
        $shop_id = isset($params->shop_id) ? $params->shop_id : false;

        if ($validator->fails()) {
            return API::responseErrorForm($validator);
        }

        $reservations = Reservation::leftJoin('billreservation', function ($join) {
            $join->on('reservation.id', 'billreservation.reservation_id');
        })
            ->leftJoin('bill', function ($join) {
                $join->on('bill.id', 'billreservation.bill_id');
            })->where("reservation.enddate", ">", DB::raw('now()'));

        if ($user_id) {
            $reservations = $reservations
                ->where("bill.user_id", $user_id);
        }

        if ($shop_id) {
            $reservations = $reservations
                ->where("bill.shop_id", $shop_id);
        }


        $reservations = $reservations->get([
            "reservation.*"
        ]);

        return API::response($reservations);

    }

    public function getNextUserReservation(Request $request)
    {
        $params = $this->jsonParams($request);

        $validator = ApiUtil::validate($params, [
            "user_id" => "required|int|min:1",
            "shop_id" => "required|int|min:1",
        ]);

        if ($validator->fails()) {
            return API::responseErrorForm($validator);
        }

        $user_id = $params->user_id;
        $shop_id = $params->shop_id;

        $reservations = Reservation::leftJoin('billreservation', function ($join) {
            $join->on('reservation.id', 'billreservation.reservation_id');
        })
            ->leftJoin('bill', function ($join) {
                $join->on('bill.id', 'billreservation.bill_id');
            })->where("reservation.startdate", ">", DB::raw('now()'))
            ->orderBy("startdate")
            ->take(1);

        $reservations = $reservations
            ->where("bill.user_id", $user_id);


        $reservations = $reservations
            ->where("bill.shop_id", $shop_id);


        $reservations = $reservations->first([
            "reservation.*"
        ]);

        return API::response($reservations, 200, true);

    }

    public function storeAnonymousReservation()
    {
        $params = json_decode(file_get_contents('php://input'));

        $validator = ApiUtil::validate($params, [
            "enddate" => "required",
            "workspace_id" => "required|int|min:1",
        ]);

        if ($validator->fails()) {
            return API::responseErrorForm($validator);
        }

        $endDate = $params->enddate;
        $workspace_id = $params->workspace_id;

        $reservation = new Reservation;
        $reservation->startDate = DateUtil::getFrDateTime("Y-m-d H:m:s");
        $reservation->endDate = $endDate;
        $reservation->workspace_id = $workspace_id;

        if (DateUtil::getFrDateTime("Y-m-d H:i:s", $reservation->endDate) <= $reservation->startDate
        ) {
            throw new ReservationException(400);
        }

        if (!$this->isWorkspaceAvailable($workspace_id, $reservation->startDate, $reservation->endDate)) {
            throw new ReservationException(400);
        }

        Reservation::create($reservation->toArray());

        return API::response($reservation);
    }

    public function isWorkspaceAvailable($workspace_id, $startDate, $endDate)
    {
        $workspace = Reservation::where([
            "workspace_id" => $workspace_id,
        ])
            ->where("startdate", "<", $endDate)
            ->where("enddate", ">", $startDate)
            ->get();

        if ($workspace->count() == 0) {
            return true;
        }

        return false;
    }

    public function setReservationCompleted($reservation_id)
    {

        $validator = ApiUtil::validate(["reservation_id" => $reservation_id], [
            "reservation_id" => "required|int|min:1",
        ]);

        if ($validator->fails()) {
            return API::responseErrorForm($validator);
        }

        $reservation = Reservation::find($reservation_id);

        if ($reservation->completed == 1) {

            throw new ReservationException(400);

        } else {

            $reservation->update([
                "completed" => self::RESERVATION_COMPLETED
            ]);

        }

        return API::response($reservation);
    }

}