<?php

namespace App\Http\Controllers;

use App\Helpers\ApiHelper as API;

class FileController extends Controller
{
	/**
	 * handle form posted file upload
	 * copy file in public directory and return file URL
	 *
	 * @return \Illuminate\Http\JsonResponse file URL
	 */
	public function storeFile()
	{
		// get environnement variables
		$dirPath = env('API_FILES_DIR_PATH');
		$dirUrl  = env('API_FILES_DIR_URL');

		// get file data
		$fileData = $_FILES['file'];

		// create unique file name
		$fileName = 'file_'.uniqid();

		// get original file extension
		$fileExtension = preg_replace('/^.+\./', '', $fileData['name']);

		// todo: check allowed extensions
		// todo: check size limit
		// todo: check type

		// build file path
		$fileNameWithExtension = $fileName.'.'.$fileExtension;

		// move and rename file
		$result = rename($fileData['tmp_name'], $dirPath.$fileNameWithExtension);

		// check result
		if ($result === false)
		{
			// todo: return specific error
			return API::responseError();
		}

		// return file URL
		return API::response($dirUrl.$fileNameWithExtension);
	}
}