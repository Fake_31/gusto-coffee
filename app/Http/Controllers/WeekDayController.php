<?php

namespace App\Http\Controllers;

use App\Models\SocialMedia;
use App\Models\WeekDay;
use App\Util\ApiUtil;
use App\Helpers\ApiHelper as API;


class WeekDayController extends Controller
{
    public function getWeekDay($weekday_id = false)
    {
        if ($weekday_id) {

            $validator = ApiUtil::validate(["id" => $weekday_id], [
                "id" => "required|numeric|min:1"
            ]);

            if ($validator->fails()) {
                return API::responseErrorForm($validator);
            }

            $weekDay = WeekDay::find($weekday_id);

        } else {
            $weekDay = WeekDay::all();
        }

        return API::response($weekDay);

    }

    public function storeWeekDay()
    {
        $params = json_decode(file_get_contents('php://input'));

        $validator = ApiUtil::validate($params, [
            "id" => "required|int|min:1",
        ]);

        if ($validator->fails()) {
            return API::responseErrorForm($validator);
        }
        $weekDay = new WeekDay();
        $weekDay->fill((array)$params);
        $weekDay->save();

        return API::response($weekDay);
    }

    public function updateWeekDay($weekday_id)
    {
        $params = json_decode(file_get_contents('php://input'));
        $objectFromRoute = ApiUtil::getParamsUrlRequest($params);

        $validator = ApiUtil::validate($objectFromRoute, [
            "id" => "required|int|min:1",
        ]);

        if ($validator->fails()) {
            return API::responseErrorForm($validator);
        }

        $weekDay = WeekDay::find($weekday_id);

        if ($weekDay) {

            $weekDay->update([
                "id" => $objectFromRoute->id
            ]);

            return API::response($weekDay);

        } else {

            return API::responseError(400);

        }
    }

    public function deleteWeekDay($id)
    {
        $validator = ApiUtil::validate(["id" => $id], [
            "id" => "required|numeric|min:1"
        ]);

        if ($validator->fails()) {
            return API::responseErrorForm($validator);
        }

        $weekDay = WeekDay::find($id);

        if (!$weekDay) {

            return API::response();

        }

        $deleted = $weekDay->delete();

        if (!$deleted) {

            return API::responseError(304);

        }

        return API::response($weekDay);

    }

}
