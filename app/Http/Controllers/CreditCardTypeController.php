<?php

namespace App\Http\Controllers;


use App\Models\CreditCardType;
use App\Util\ApiUtil;
use App\Helpers\ApiHelper as API;


class CreditCardTypeController extends Controller
{
    public function getCreditCardtype($creditcardtype_id = false)
    {
        if ($creditcardtype_id) {

            $validator = ApiUtil::validate(["id" => $creditcardtype_id], [
                "id" => "required|int|min:1"
            ]);

            if ($validator->fails()) {
                return API::responseErrorForm($validator);
            }

            $creditcardtype = CreditCardType::find($creditcardtype_id);

        } else {
            $creditcardtype = CreditCardType::all();
        }

        return API::response($creditcardtype);

    }

    public function storeCreditCardtype()
    {
        $params = json_decode(file_get_contents('php://input'));

        $validator = ApiUtil::validate($params, [
            "label" => "required|string|min:1",
        ]);

        if ($validator->fails()) {
            return API::responseErrorForm($validator);
        }
        $creditcardtype = new CreditCardType();
        $creditcardtype->fill((array)$params);
        $creditcardtype->save();

        return API::response($creditcardtype);
    }

    public function updateCreditCardtype($creditcardtype_id)
    {
        $params = json_decode(file_get_contents('php://input'));
        $objectFromRoute = ApiUtil::getParamsUrlRequest($params);

        $validator = ApiUtil::validate($objectFromRoute, [
            "label" => "required|string|min:1",
        ]);

        if ($validator->fails()) {
            return API::responseErrorForm($validator);
        }

        $creditcardtype = CreditCardType::find($creditcardtype_id);

        if ($creditcardtype) {

            $creditcardtype->update([
                "label" => $objectFromRoute->label
            ]);

            return API::response($creditcardtype);

        } else {

            return API::responseError(400);

        }
    }

    public function deleteCreditCardtype($creditcardtype_id)
    {
        $validator = ApiUtil::validate(["id" => $creditcardtype_id], [
            "id" => "required|int|min:1"
        ]);

        if ($validator->fails()) {
            return API::responseErrorForm($validator);
        }

        $creditcardtype = CreditCardType::find($creditcardtype_id);

        if (!$creditcardtype) {

            return API::response();

        }

        $deleted = $creditcardtype->delete();

        if (!$deleted) {

            return API::responseError(304);

        }

        return API::response($creditcardtype);

    }

}
