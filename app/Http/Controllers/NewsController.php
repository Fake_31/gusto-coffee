<?php

namespace App\Http\Controllers;

use App\Helpers\ApiHelper as API;
use App\Models\News;
use App\Util\ApiUtil;
use App\Util\ArrayUtil;
use Illuminate\Http\Request;

class NewsController extends Controller
{

	public function getNews(Request $request)
	{

		$params = $this->jsonParams($request);

		$validator = ApiUtil::validate($params, [
			"shop_id"          => "int|min:1",
			"include_unlinked" => "boolean",
			"limit"            => "int|min:1",
		]);

		if ($validator->fails())
		{
			return API::responseErrorForm($validator);
		}

		$shop_id          = isset($params->shop_id) ? $params->shop_id : false;
		$include_unlinked = isset($params->include_unlinked) ? $params->include_unlinked : false;
		$limit            = isset($params->limit) ? $params->limit : false;

		$news = News::select();

		if ($shop_id)
		{
			$news->where("shop_id", $shop_id);

			if ($include_unlinked)
			{
				$news->orWhereNull('shop_id');
			}
		}

		if ($limit)
		{
			$news->take($limit);
		}

		$news->orderBy('eventdate', 'DESC');

		return API::response($news->get());
	}

	public function getNewsFromId($news_id)
	{
		$validator = ApiUtil::validate(["id_news" => $news_id], [
			"news_id" => "required|int|min:1",
		]);

		if ($validator->fails())
		{
			return API::responseErrorForm($validator);
		}

		$news = News::find($news_id);

		return API::response($news);
	}

	public function storeNews()
	{
		$params = json_decode(file_get_contents('php://input'));

		$validator = ApiUtil::validate($params, [
			"title"     => "required|string",
			"content"   => "required|string",
			"eventdate" => "required|date|date_format:Y-m-d",
			"picture"   => "nullable|string",
			"shop_id"   => "nullable|int|min:1",
		]);

		if ($validator->fails())
		{
			return API::responseErrorForm($validator);
		}
		/*
			Todo exceptions foreign key @shop_id
		*/

		$news = new News;
		$news->fill((array) $params);
		$news->creationdate = (new \DateTime('now', new \DateTimeZone('Europe/Paris')))->format('Y-m-d H:i:s');
		$news->save();

		return API::response($news);
	}

	public function updateNews($news_id)
	{
		$params = json_decode(file_get_contents('php://input'));
		$params = ArrayUtil::merge($params, ["id_news" => $news_id]);

		$validator = ApiUtil::validate($params, [
			"title"     => "required|string",
			"content"   => "required|string",
			"picture"   => "nullable|string",
			"shop_id"   => "nullable|int|min:1",
			"id_news"   => "required|int|min:1",
			"eventdate" => "required|date|date_format:Y-m-d",
		]);

		if ($validator->fails())
		{
			return API::responseErrorForm($validator);
		}

		$news = News::find($news_id);

		if ($news)
		{

			$news->fill((array) $params)
			     ->save();

			return API::response($news);
		}
		else
		{

			return API::responseError(206);
		}
	}

	public function deleteNews($news_id)
	{
		$validator = ApiUtil::validate(["id_news" => $news_id], [
			"id_news" => "required|int|min:1",
		]);

		if ($validator->fails())
		{
			return API::responseErrorForm($validator);
		}

		$news = News::find($news_id);

		if ($news)
		{

			$news->delete();
			return API::response($news);
		}
		else
		{

			return API::responseError(400);
		}
	}

}