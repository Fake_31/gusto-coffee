<?php

namespace App\Http\Controllers;

use App\Models\UserService;
use App\Util\ApiUtil;
use App\Helpers\ApiHelper as API;
use Illuminate\Support\Facades\DB;

class UserServiceController extends Controller
{

    public function getUserService($user_id = false, $service_id = false, $shop_id = false)
    {
        if ($user_id && !$service_id) {

            $validator = ApiUtil::validate(["user_id" => $user_id], [
                "user_id" => "required|int|min:1"
            ]);

            if ($validator->fails()) {
                return API::responseErrorForm($validator);
            }

            $userservice = UserService::where("user_id", $user_id)->get();

        } elseif($user_id && $service_id && !$shop_id) {

            $objectFromRoute = ApiUtil::getParamsUrlRequest();

            $validator = ApiUtil::validate($objectFromRoute, [
                "user_id" => "required|int|min:1",
                "service_id" => "required|int|min:1",
            ]);

            if ($validator->fails()) {
                return API::responseErrorForm($validator);
            }

            $userservice = UserService::where([
                "user_id" => $user_id,
                "service_id" => $service_id
            ])->get();

        } elseif($user_id && $service_id && $shop_id) {

            $objectFromRoute = ApiUtil::getParamsUrlRequest();

            $validator = ApiUtil::validate($objectFromRoute, [
                "user_id" => "required|int|min:1",
                "service_id" => "required|int|min:1",
                "shop_id" => "required|int|min:1",
            ]);

            if ($validator->fails()) {
                return API::responseErrorForm($validator);
            }

            $userservice = UserService::where([
                "user_id" => $user_id,
                "service_id" => $service_id,
                "shop_id" => $shop_id
            ])->get();

        } else {

            $userservice = UserService::all();

        }

        return API::response($userservice);

    }

    public function storeUserService()
    {
        $params = json_decode(file_get_contents('php://input'));
        $objectFromRoute = ApiUtil::getParamsUrlRequest($params);

        $validator = ApiUtil::validate($objectFromRoute, [
            "user_id" => "required|int|min:1",
            "service_id" => "required|int|min:1",
            "shop_id" => "required|int|min:1",
            "count" => "required|int|min:1",
        ]);

        if ($validator->fails()) {
            return API::responseErrorForm($validator);
        }

        $userservice = new UserService();
        $userservice->fill((array)$objectFromRoute);

        $userservicedb = UserService::where([
            "user_id" => $objectFromRoute->user_id,
            "service_id" => $objectFromRoute->service_id,
            "shop_id" => $objectFromRoute->shop_id
        ]);

        if($userservicedb->count() > 0) {

            $userservicedb->update([
                'count' => DB::raw("count+" . $objectFromRoute->count)
            ]);

        } else {

            $userservice->save();

        }

        $userservice->count = $userservicedb->first()->count;

        return API::response($userservice);
    }

    public function updateUserService($bill_id, $service_id)
    {
        $params = json_decode(file_get_contents('php://input'));
        $objectFromRoute = ApiUtil::getParamsUrlRequest($params);

        $validator = ApiUtil::validate($objectFromRoute, [
            "user_id" => "required|int|min:1",
            "service_id" => "required|int|min:1",
            "shop_id" => "required|int|min:1",
            "count" => "required|int|min:1",
        ]);

        if ($validator->fails()) {
            return API::responseErrorForm($validator);
        }

        $userservice = UserService::where([
            "user_id" => $objectFromRoute->user_id,
            "service_id" => $objectFromRoute->service_id,
            "shop_id" => $objectFromRoute->shop_id
        ]);

        $userserviceget = $userservice->get();

        if ($userserviceget->count() > 0) {

            $userservice->update([
                "count" => $params->count
            ]);

            return API::response($params);

        } else {

            return API::responseError(400);

        }
    }

    public function deleteUserService($bill_id, $service_id)
    {
        $objectFromRoute = ApiUtil::getParamsUrlRequest();

        $validator = ApiUtil::validate($objectFromRoute, [
            "user_id" => "required|int|min:1",
            "service_id" => "required|int|min:1",
            "shop_id" => "required|int|min:1",
        ]);

        if ($validator->fails()) {
            return API::responseErrorForm($validator);
        }

        $userservice = UserService::where([
            "user_id" => $objectFromRoute->user_id,
            "service_id" => $objectFromRoute->service_id,
            "shop_id" => $objectFromRoute->shop_id
        ]);
        $userserviceget = $userservice->get();

        if (!$userservice) {

            return API::response();

        }

        $deleted = $userservice->delete();

        if (!$deleted) {

            return API::responseError(400);

        }

        return API::response($userserviceget);

    }

}