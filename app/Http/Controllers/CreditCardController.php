<?php

namespace App\Http\Controllers;

use App\Models\CreditCard;
use App\Util\ApiUtil;
use Illuminate\Http\Request;
use App\Helpers\ApiHelper as API;

class CreditCardController extends Controller
{

	public function getCreditCard($creditCardId)
	{
		return API::response(CreditCard::find($creditCardId));
	}

    public function getCreditCards(Request $request)
    {
        $params = $this->jsonParams($request);

        $validator = ApiUtil::validate($params, [
            "user_id" => "int|min:1",
            "type_id" => "int|min:1"
        ]);

        if ($validator->fails()) {
            return API::responseErrorForm($validator);
        }

        $user_id = isset($params->user_id) ? $params->user_id : false;
        $type_id = isset($params->type_id) ? $params->type_id : false;


        $creditCard = CreditCard::select();

        if ($user_id) {

            $creditCard = $creditCard->where("user_id", $user_id);

        }
        if ($type_id) {

            $creditCard = $creditCard->where("type_id", $type_id);

        }

        return API::response($creditCard->get());

    }

    public function storeCreditCard()
    {
        $params = json_decode(file_get_contents('php://input'));

        $validator = ApiUtil::validate($params, [
            "number" => "required|string",
            "cryptogram" => "required|string",
            "month" => "required|int|min:1",
            "year" => "required|int|min:1",
            "type_id" => "required|int|min:1",
            "user_id" => "required|int|min:1",
        ]);

        if ($validator->fails()) {
            return API::responseErrorForm($validator);
        }

        $creditCard = new CreditCard;
        $creditCard->fill((array)$params)
            ->save();

        return API::response($creditCard);
    }

    public function updateCreditCard($id)
    {
        $params = json_decode(file_get_contents('php://input'));


        $validator = ApiUtil::validate($params, [
            "number" => "required|string",
            "cryptogram" => "required|string",
            "month" => "required|int|min:1",
            "year" => "required|int|min:1",
            "type_id" => "required|int|min:1",
            "user_id" => "required|int|min:1",
        ]);

        if ($validator->fails()) {
            return API::responseErrorForm($validator);
        }

        $creditCard = CreditCard::find($id);

        if (!$creditCard) {
            return API::responseError(400);
        }

        $creditCard->fill((array)$params)
            ->save();

        return API::response($creditCard);

    }

    public function deleteCreditCard($id)
    {
        $validator = ApiUtil::validate(["id" => $id], [
            "id" => "required|numeric|min:1"
        ]);

        if ($validator->fails()) {
            return API::responseErrorForm($validator);
        }

        $creditCard = CreditCard::find($id);

        if (!$creditCard) {
            return API::response();
        }

        $creditCard->delete();

        return API::response($creditCard);
    }
}