<?php

namespace App\Http\Controllers;

use App\Models\ServiceLabel;
use App\Util\ApiUtil;
use App\Util\ArrayUtil;
use App\Helpers\ApiHelper as API;

class ServicelLabelController extends Controller
{

    public function getServicesLabel($language_id = null, $service_id = null)
    {
        if ($language_id && !$service_id) {

            $validator = ApiUtil::validate([
                "language_id" => $language_id,
            ], [
                "language_id" => "required|int|min:1"
            ]);

            if ($validator->fails()) {
                return API::responseErrorForm($validator);
            }

            $servicesLabel = ServiceLabel::where([
                "language_id" => $language_id
            ])
                ->get();

        } elseif($language_id && $service_id) {

            $objectFromRoute = ApiUtil::getParamsUrlRequest();

            $validator = ApiUtil::validate($objectFromRoute, [
                "language_id" => "required|int|min:1",
                "service_id" => "required|int|min:1",
            ]);

            if ($validator->fails()) {
                return API::responseErrorForm($validator);
            }

            $servicesLabel = ServiceLabel::where([
                "language_id" => $language_id,
                "service_id" => $service_id
            ])->first();

        } else {

            $servicesLabel = ServiceLabel::all();

        }

        return API::response($servicesLabel);

    }

    public function deleteServiceLabel($language_id, $service_id)
    {
        $validator = ApiUtil::validate([
            "language_id" => $language_id,
            "service_id" => $service_id
        ], [
            "language_id" => "required|int|min:1",
            "service_id" => "required|int|min:1",
        ]);

        if ($validator->fails()) {
            return API::responseErrorForm($validator);
        }

        $serviceLabel = ServiceLabel::where([
            "language_id" => $language_id,
            "service_id" => $service_id
        ]);

        $serviceLabelSearched = $serviceLabel->first();

        if ($serviceLabel->delete() == 0) {
            return API::response();
        }

        return API::response($serviceLabelSearched);

    }

    public function deleteLabelByServices($service_id)
    {
        $validator = ApiUtil::validate([
            "service_id" => $service_id
        ], [
            "service_id" => "required|int|min:1",
        ]);

        if ($validator->fails()) {
            return API::responseErrorForm($validator);
        }

        $serviceLabel = ServiceLabel::where([
            "service_id" => $service_id
        ]);

        $serviceLabelSearched = $serviceLabel->get();

        if ($serviceLabel->delete() == 0) {
            return API::response();
        }

        return API::response($serviceLabelSearched);
    }

    public function updateServiceLabel($language_id, $service_id)
    {
        $params = json_decode(file_get_contents('php://input'));

        $params = ArrayUtil::merge($params, [
            "language_id" => $language_id,
            "service_id" => $service_id
        ]);

        $validator = ApiUtil::validate($params, [
            "language_id" => "required|int|min:1",
            "service_id" => "required|int|min:1",
            "label" => "string",
            "description" => "string",
        ]);

        if ($validator->fails()) {
            return API::responseErrorForm($validator);
        }

        $serviceLabel = ServiceLabel::where([
            "language_id" => $language_id,
            "service_id" => $service_id
        ]);

        $serviceLabelSearched = $serviceLabel
            ->first()
            ->fill((array)$params);

        $serviceLabel->update($serviceLabelSearched->toArray());

        return API::response($serviceLabelSearched);

    }

    public function storeServiceLabel($language_id, $service_id)
    {
        $params = json_decode(file_get_contents('php://input'));

        $params = ArrayUtil::merge($params, [
            "language_id" => $language_id,
            "service_id" => $service_id
        ]);

        $validator = ApiUtil::validate($params, [
            "language_id" => "required|int|min:1",
            "service_id" => "required|int|min:1",
            "label" => "required|string",
            "description" => "required|string",
        ]);

        if ($validator->fails()) {
            return API::responseErrorForm($validator);
        }

        $serviceLabel = new ServiceLabel;
        $serviceLabel->fill((array)$params)
            ->save();

        return API::response($serviceLabel);

    }
}