<?php

namespace App\Http\Controllers;

use App\Models\BlackItem;
use App\Models\Message;
use App\Util\ApiUtil;
use App\Util\DateUtil;
use Illuminate\Http\Request;
use App\Helpers\ApiHelper as API;

class MessageController extends Controller
{
	const MESSAGE_UNREADED = 0;

	const MESSAGE_READED = 1;

	public function getMessages(Request $request)
	{
		$params = $this->jsonParams($request);

		$validator = ApiUtil::validate($params, [
			"user_id"             => "int|min:1",
			"exclude_blacklisted" => "sometimes|boolean",
		]);

		if ($validator->fails())
		{
			return API::responseErrorForm($validator);
		}

		$user_id = $params->user_id;

		$message = Message::where(function ($query) use ($user_id)
		{
			$query->orWHere('sourceuser_id', $user_id);
			$query->orWHere('targetuser_id', $user_id);
		});

		$message->orderBY("creationdate");

		// exclude message from or to blacklisted users
		if (isset($params->exclude_blacklisted) && $params->exclude_blacklisted === true)
		{
			$blackItems = BlackItem::where('sourceuser_id', $user_id)
			                       ->orWHere('targetuser_id', $user_id)
			                       ->get();

			$blackListedUsersIds = [];
			foreach ($blackItems as $blackItem)
			{
				$blackListedUsersIds[] = $blackItem->targetuser_id;
			}
			if (!empty($blackListedUsersIds))
			{
				$message->whereNotIn('sourceuser_id', $blackListedUsersIds);
				$message->whereNotIn('targetuser_id', $blackListedUsersIds);
			}
		}

		return API::response($message->get());
	}


	public function getMessage($message_id)
	{

		$validator = ApiUtil::validate(["message_id" => $message_id], [
			"message_id" => "required|int|min:1",
		]);

		if ($validator->fails())
		{
			return API::responseErrorForm($validator);
		}

		$message = Message::find($message_id);

		return API::response($message);
	}

	public function getUnreadMessages(Request $request)
	{
		$params = $this->jsonParams($request);

		$validator = ApiUtil::validate($params, [
			"targetuser_id"       => "required|int|min:1",
			"min_id"              => "int|min:1",
			"exclude_blacklisted" => "sometimes|boolean",
		]);

		if ($validator->fails())
		{
			return API::responseErrorForm($validator);
		}

		$targetuser_id = $params->targetuser_id;
		$min_id        = isset($params->min_id) ? $params->min_id : false;

		$messages = Message::where([
			"targetuser_id" => $targetuser_id,
			"read"          => self::MESSAGE_UNREADED
		]);

		// exclude message from blacklisted users
		if (isset($params->exclude_blacklisted) && $params->exclude_blacklisted === true)
		{
			$blackItems          = BlackItem::where('sourceuser_id', $targetuser_id)->get();
			$blackListedUsersIds = [];
			foreach ($blackItems as $blackItem)
			{
				$blackListedUsersIds[] = $blackItem->targetuser_id;
			}
			if (!empty($blackListedUsersIds))
			{
				$messages->whereNotIn('sourceuser_id', $blackListedUsersIds);
			}
		}

		if ($min_id)
		{
			$messages = $messages->where("id", ">", $min_id);
		}

		$messages = $messages->get();

		return API::response($messages);
	}


	public function storeMessage(Request $request)
	{
		$params = json_decode(file_get_contents('php://input'));

		$validator = ApiUtil::validate($params, [
			"sourceuser_id" => "required|int|min:1",
			"targetuser_id" => "required|int|min:1",
			"content"       => "required|string",
		]);

		if ($validator->fails())
		{
			return API::responseErrorForm($validator);
		}

		$message = new Message;
		$message->fill($request->all());
		$message->creationdate = DateUtil::getFrDateTime("Y-m-d H:i:s");
		$message->read         = 0;

		$message->save();

		return API::response($message);
	}

	public function updateMessage($message_id)
	{
		$params = json_decode(file_get_contents('php://input'));

		$validator = ApiUtil::validate($params, [
			"sourceuser_id" => "int|min:1",
			"targetuser_id" => "int|min:1",
			"content"       => "required|string",
			"read"          => "boolean",
		]);

		if ($validator->fails())
		{
			return API::responseErrorForm($validator);
		}

		$message = Message::find($message_id);

		if ($message)
		{

			$newMessage = $message->fill((array) $params);
			$message->update($newMessage->toArray());
		}

		return API::response($message);
	}

	public function markMessagesAsRead()
	{
		$params = json_decode(file_get_contents('php://input'));

		$validator = ApiUtil::validate($params, [
			"ids"   => "required|array",
			'ids.*' => 'int'
		]);

		if ($validator->fails())
		{
			return API::responseErrorForm($validator);
		}

		$ids = $params->ids;

		$messages = Message::whereIn("id", $ids);

		$messages->update([
			"read" => self::MESSAGE_READED
		]);

		$messages = $messages->get();

		return API::response($messages);
	}


	public function deleteMessage($message_id)
	{
		$validator = ApiUtil::validate(["message_id" => $message_id], [
			"message_id" => "required|int|min:1",
		]);

		if ($validator->fails())
		{
			return API::responseErrorForm($validator);
		}

		$message = Message::find($message_id);

		if ($message)
		{
			$message->delete();
			return API::response($message);
		}
		else
		{
			return API::responseError(403);
		}
	}


}