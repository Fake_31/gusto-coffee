<?php

namespace App\Http\Controllers;

use App\Models\BlackItem;
use App\Util\ApiUtil;
use App\Helpers\ApiHelper as API;

class BlackItemController extends Controller
{
    /**
     * @SWG\Get(
     *     path="/blackitem",
     *     summary="Finds all BlackItem",
     *     tags={"BlackItem"},
     *     description="Find all BlackItem",
     *     operationId="getBlackItem",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="Api-Token",
     *         in="header",
     *         type="string",
     *         format="string"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="OK"
     *     ),
     *     deprecated=false
     * )
     */

    /**
     * @SWG\Get(
     *     path="/blackitem/{sourceuser_id}",
     *     summary="Finds BlackItem from a sourceuser id",
     *     tags={"BlackItem"},
     *     description="Finds BlackItem from a sourceuser id",
     *     operationId="getBlackItem",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         description="Source user ID",
     *         in="path",
     *         name="sourceuser_id",
     *         required=true,
     *         type="integer",
     *         format="int64"
     *     ),
     *     @SWG\Parameter(
     *         name="Api-Token",
     *         in="header",
     *         type="string",
     *         format="string"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="OK"
     *     ),
     *     @SWG\Response(
     *         response="403",
     *         description="Invalid request"
     *     ),
     *     deprecated=false
     * )
     */

    /**
     * @SWG\Get(
     *     path="/blackitem/{sourceuser_id}/{targetuser_id}",
     *     summary="Find BlackItem from a sourceuser id or a targetuser_id",
     *     tags={"BlackItem"},
     *     description="Find BlackItem from a sourceuser id or a targetuser_id",
     *     operationId="getBlackItem",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         description="Source user ID",
     *         in="path",
     *         name="sourceuser_id",
     *         required=true,
     *         type="integer",
     *         format="int64"
     *     ),
     *     @SWG\Parameter(
     *         description="Target user id",
     *         in="path",
     *         name="targetuser_id",
     *         required=true,
     *         type="integer",
     *         format="int64"
     *     ),
     *     @SWG\Parameter(
     *         name="Api-Token",
     *         in="header",
     *         type="string",
     *         format="string"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="OK"
     *     ),
     *     @SWG\Response(
     *         response="403",
     *         description="Country does not exist"
     *     ),
     *     deprecated=false
     * )
     */
    public function getBlackItem($sourceuser_id = false, $targetuser_id = false)
    {
        if ($sourceuser_id && !$targetuser_id) {

            $validator = ApiUtil::validate([
                "sourceuser_id" => $sourceuser_id,
            ], [
                "sourceuser_id" => "required|int|min:1",
            ]);

            if ($validator->fails()) {
                return API::responseErrorForm($validator);
            }

            $blackItem = BlackItem::where([
                "sourceuser_id" => $sourceuser_id
            ])
                ->get();

        } elseif($sourceuser_id && $targetuser_id) {

            $objectFromRoute = ApiUtil::getParamsUrlRequest();

            $validator = ApiUtil::validate($objectFromRoute, [
                "sourceuser_id" => "required|int|min:1",
                "targetuser_id" => "required|different:sourceuser_id|int|min:1"
            ]);

            if ($validator->fails()) {
                return API::responseErrorForm($validator);
            }

            $blackItem = BlackItem::where([
                "sourceuser_id" => $sourceuser_id,
                "targetuser_id" => $targetuser_id
            ])
                ->first();

        } else {

            $blackItem = BlackItem::all();

        }



        return API::response($blackItem);

    }

    public function storeBlackItem($sourceuser_id, $targetuser_id)
    {
        $params = ApiUtil::getParamsUrlRequest();

        $validator = ApiUtil::validate([
            "sourceuser_id" => $sourceuser_id,
            "targetuser_id" => $targetuser_id
        ], [
            "sourceuser_id" => "required|int|min:1",
            "targetuser_id" => "required|different:sourceuser_id|int|min:1"
        ]);

        if ($validator->fails()) {
            return API::responseErrorForm($validator);
        }

        $blackItem = new BlackItem;
        $blackItem->fill((array)$params)
            ->save();

        return API::response($blackItem);
    }

    public function deleteBlackItem($sourceuser_id, $targetuser_id)
    {
        $validator = ApiUtil::validate([
            "sourceuser_id" => $sourceuser_id,
            "targetuser_id" => $targetuser_id
        ], [
            "sourceuser_id" => "required|int|min:1",
            "targetuser_id" => "required|different:sourceuser_id|int|min:1"
        ]);

        if ($validator->fails()) {
            return API::responseErrorForm($validator);
        }

        $blackItem = BlackItem::where([
            "sourceuser_id" => $sourceuser_id,
            "targetuser_id" => $targetuser_id
        ]);

        $blackItemSearched = $blackItem->first();

        if ($blackItem->delete() == 0) {
            return API::response();
        }

        return API::response($blackItemSearched);
    }
}