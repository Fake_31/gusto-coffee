<?php

namespace App\Http\Controllers;

use App\Models\BillService;
use App\Util\ApiUtil;
use App\Helpers\ApiHelper as API;

class BillServiceController extends Controller
{

    /**
     * @SWG\Get(
     *     path="/billservice",
     *     summary="Finds all BillService",
     *     tags={"BillService"},
     *     description="Find all BillReservation",
     *     operationId="getBillService",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="Api-Token",
     *         in="header",
     *         type="string",
     *         format="string"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="OK"
     *     ),
     *     @SWG\Response(
     *         response="403",
     *         description="Invalid request"
     *     ),
     *     deprecated=false
     * )
     */

    /**
     * @SWG\Get(
     *     path="/billservice/{bill_id}",
     *     summary="Find BillService from a bill id",
     *     tags={"BillService"},
     *     description="Finds billreservations from a bill id",
     *     operationId="getBillService",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         description="ID of bill to return",
     *         in="path",
     *         name="bill_id",
     *         required=true,
     *         type="integer",
     *         format="int64"
     *     ),
     *     @SWG\Parameter(
     *         name="Api-Token",
     *         in="header",
     *         type="string",
     *         format="string"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="OK"
     *     ),
     *     @SWG\Response(
     *         response="403",
     *         description="Invalid request"
     *     ),
     *     deprecated=false
     * )
     */

    /**
     * @SWG\Get(
     *     path="/billservice/{bill_id}/{service_id}",
     *     summary="Find BillService from  bill id and reservation_id",
     *     tags={"BillService"},
     *     description="Finds BillService from  bill id and service_id",
     *     operationId="getBillService",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         description="ID of bill to return",
     *         in="path",
     *         name="bill_id",
     *         required=true,
     *         type="integer",
     *         format="int64"
     *     ),
     *     @SWG\Parameter(
     *         description="Service id",
     *         in="path",
     *         name="service_id",
     *         required=true,
     *         type="integer",
     *         format="int64"
     *     ),
     *     @SWG\Parameter(
     *         name="Api-Token",
     *         in="header",
     *         type="string",
     *         format="string"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="OK"
     *     ),
     *     @SWG\Response(
     *         response="403",
     *         description="Invalid request"
     *     ),
     *     deprecated=false
     * )
     */
    public function getBillService($bill_id = false, $service_id = false)
    {
        if ($bill_id && !$service_id) {

            $validator = ApiUtil::validate(["id" => $bill_id], [
                "id" => "required|int|min:1"
            ]);

            if ($validator->fails()) {
                return API::responseErrorForm($validator);
            }

            $billservice = BillService::where("bill_id", $bill_id)->get();

        } elseif($bill_id && $service_id) {

            $objectFromRoute = ApiUtil::getParamsUrlRequest();

            $validator = ApiUtil::validate($objectFromRoute, [
                "bill_id" => "required|int|min:1",
                "service_id" => "required|int|min:1",
            ]);

            if ($validator->fails()) {
                return API::responseErrorForm($validator);
            }
            
            $billservice = BillService::where([
                "bill_id" => $bill_id,
                "service_id" => $service_id
            ])->get();

        } else {

            $billservice = BillService::all();

        }

        return API::response($billservice);

    }


}