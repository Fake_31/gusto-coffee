<?php

namespace App\Http\Controllers;

use App\Models\SocialMedia;
use App\Models\UserType;
use App\Models\WeekDay;
use App\Util\ApiUtil;
use App\Helpers\ApiHelper as API;


class UserTypeController extends Controller
{
    public function getUserType($usertype_id = false)
    {
        if ($usertype_id) {

            $validator = ApiUtil::validate(["id" => $usertype_id], [
                "id" => "required|int|min:1"
            ]);

            if ($validator->fails()) {
                return API::responseErrorForm($validator);
            }

            $usertype = UserType::find($usertype_id);

        } else {
            $usertype = UserType::all();
        }

        return API::response($usertype);

    }

    public function storeUserType()
    {
        $params = json_decode(file_get_contents('php://input'));

        $validator = ApiUtil::validate($params, [
            "label" => "required|string|min:1",
        ]);

        if ($validator->fails()) {
            return API::responseErrorForm($validator);
        }
        $usertype = new UserType();
        $usertype->fill((array)$params);
        $usertype->save();

        return API::response($usertype);
    }

    public function updateUserType($usertype_id)
    {
        $params = json_decode(file_get_contents('php://input'));
        $objectFromRoute = ApiUtil::getParamsUrlRequest($params);

        $validator = ApiUtil::validate($objectFromRoute, [
            "label" => "required|string|min:1",
        ]);

        if ($validator->fails()) {
            return API::responseErrorForm($validator);
        }

        $usertype = UserType::find($usertype_id);

        if ($usertype) {

            $usertype->update([
                "label" => $objectFromRoute->label
            ]);

            return API::response($usertype);

        } else {

            return API::responseError(400);

        }
    }

    public function deleteUserType($usertype_id)
    {
        $validator = ApiUtil::validate(["id" => $usertype_id], [
            "id" => "required|int|min:1"
        ]);

        if ($validator->fails()) {
            return API::responseErrorForm($validator);
        }

        $usertype = UserType::find($usertype_id);

        if (!$usertype) {

            return API::response();

        }

        $deleted = $usertype->delete();

        if (!$deleted) {

            return API::responseError(304);

        }

        return API::response($usertype);

    }

}
