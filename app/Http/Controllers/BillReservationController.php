<?php

namespace App\Http\Controllers;

use App\Models\BillReservation;
use App\Util\ApiUtil;
use App\Helpers\ApiHelper as API;

class BillReservationController extends Controller
{
    /**
     * @SWG\Get(
     *     path="/billreservation",
     *     summary="Finds all BillReservation",
     *     tags={"BillReservation"},
     *     description="Find all BillReservation",
     *     operationId="getBillReservation",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="Api-Token",
     *         in="header",
     *         type="string",
     *         format="string"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="OK"
     *     ),
     *     deprecated=false
     * )
     */

    /**
     * @SWG\Get(
     *     path="/billreservation/{bill_id}",
     *     summary="Finds billreservations from a bill id",
     *     tags={"BillReservation"},
     *     description="Finds billreservations from a bill id",
     *     operationId="getBillReservation",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         description="ID of bill to return",
     *         in="path",
     *         name="bill_id",
     *         required=true,
     *         type="integer",
     *         format="int64"
     *     ),
     *     @SWG\Parameter(
     *         name="Api-Token",
     *         in="header",
     *         type="string",
     *         format="string"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="OK"
     *     ),
     *     @SWG\Response(
     *         response="403",
     *         description="Invalid request"
     *     ),
     *     deprecated=false
     * )
     */

    /**
     * @SWG\Get(
     *     path="/billreservation/{bill_id}/{reservation_id}",
     *     summary="Finds billreservations from  bill id and reservation_id",
     *     tags={"BillReservation"},
     *     description="Finds billreservations from  bill id and reservation_id",
     *     operationId="getBillReservation",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         description="ID of bill to return",
     *         in="path",
     *         name="bill_id",
     *         required=true,
     *         type="integer",
     *         format="int64"
     *     ),
     *     @SWG\Parameter(
     *         description="Reservation id",
     *         in="path",
     *         name="reservation_id",
     *         required=true,
     *         type="integer",
     *         format="int64"
     *     ),
     *     @SWG\Parameter(
     *         name="Api-Token",
     *         in="header",
     *         type="string",
     *         format="string"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="OK"
     *     ),
     *     @SWG\Response(
     *         response="403",
     *         description="Country does not exist"
     *     ),
     *     deprecated=false
     * )
     */
    public function getBillReservation($bill_id = false, $reservation_id = false)
    {
        if ($bill_id && !$reservation_id) {

            $validator = ApiUtil::validate(["bill_id" => $bill_id], [
                "bill_id" => "required|int|min:1"
            ]);

            if ($validator->fails()) {
                return API::responseErrorForm($validator);
            }

            $billreservation = BillReservation::where("bill_id", $bill_id)->get();

        } elseif ($bill_id && $reservation_id) {

            $objectFromRoute = ApiUtil::getParamsUrlRequest();

            $validator = ApiUtil::validate($objectFromRoute, [
                "bill_id" => "required|int|min:1",
                "reservation_id" => "required|int|min:1",
            ]);

            if ($validator->fails()) {
                return API::responseErrorForm($validator);
            }

            $billreservation = BillReservation::where([
                "bill_id" => $bill_id,
                "reservation_id" => $reservation_id
            ])->get();

        } else {

            $billreservation = BillReservation::all();

        }

        return API::response($billreservation);

    }

}