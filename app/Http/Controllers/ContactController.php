<?php

namespace App\Http\Controllers;

use App\Helpers\ApiHelper as API;
use App\Jobs\SendMail;
use App\Util\ApiUtil;
use App\Models\Mail;

class ContactController extends Controller
{
    public function sendEmail()
    {
        $params = json_decode(file_get_contents('php://input'));

        $validator = ApiUtil::validate($params, [
            "email" => "required|email",
            "subject" => "required|string|min:1",
            "content" => "required|string|min:15",
        ]);

        if ($validator->fails()) {
            return API::responseErrorForm($validator);
        }

        $data = array(
            'email' => $params->email,
            'subject' => $params->subject,
            'content' => $params->content
        );

        // Prépare email to queue
        $mail = new Mail;
        $mail->setTo("kevin.pech31@gmail.com");
        $mail->setTitle("[Gusto Coffe] Contact");
        $mail->setContentVariables($data);
        $mail->setTemplate("emails.contact");

        // Send email to queue
        $this->dispatch(new SendMail($mail));

        return API::response(true);

    }

}