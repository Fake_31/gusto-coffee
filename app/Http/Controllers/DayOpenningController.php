<?php

namespace App\Http\Controllers;

use App\Models\DayOpenning;
use App\Util\ApiUtil;
use App\Util\ArrayUtil;
use App\Helpers\ApiHelper as API;

class DayOpenningController extends Controller
{
    public function getDayOpenning($shop_id = null, $weekday_id = null)
    {
        if ($shop_id && !$weekday_id) {

            $validator = ApiUtil::validate([
                "shop_id" => $shop_id,
            ], [
                "shop_id" => "required|int|min:1",
            ]);

            if ($validator->fails()) {
                return API::responseErrorForm($validator);
            }

            $dayOpenning = DayOpenning::where([
                "shop_id" => $shop_id
            ])
                ->get();

        } elseif($shop_id && $weekday_id) {

            $objectFromRoute = ApiUtil::getParamsUrlRequest();

            $validator = ApiUtil::validate($objectFromRoute, [
                "shop_id" => "required|int|min:1",
                "weekday_id" => "required|int|min:1",
            ]);

            if ($validator->fails()) {
                return API::responseErrorForm($validator);
            }

            $dayOpenning = DayOpenning::where([
                "shop_id" => $shop_id,
                "weekday_id" => $weekday_id
            ])->first();

        } else {

            $dayOpenning = DayOpenning::all();

        }

        return API::response($dayOpenning);
    }

    public function deleteDayopenning($shop_id, $weekday_id)
    {
        $validator = ApiUtil::validate([
            "shop_id" => $shop_id,
            "weekday_id" => $weekday_id
        ], [
            "shop_id" => "required|int|min:1",
            "weekday_id" => "required|int|min:1",
        ]);

        if ($validator->fails()) {
            return API::responseErrorForm($validator);
        }

        $dayOpenning = DayOpenning::where([
            "shop_id" => $shop_id,
            "weekday_id" => $weekday_id
        ]);

        $dayOpenningSaved = $dayOpenning->first();
        $dayOpenningDeleted = $dayOpenning->delete();

        if ($dayOpenningDeleted == 1) {
            return API::response($dayOpenningSaved);
        } else {
            return API::responseError(206);
        }
    }

    public function storeDayOpenning($shop_id, $weekday_id)
    {
        /*
         * Todo : Check if storeDayOpenning already exist
         */

        $params = json_decode(file_get_contents('php://input'));
        $params = ArrayUtil::merge($params, [
            "shop_id" => $shop_id,
            "weekday_id" => $weekday_id
        ]);

        $validator = ApiUtil::validate($params, [
            "closed" => "required|boolean",
            "fullhourstart" => "required|numeric|min:0.1|max:24",
            "fullhourend" => "required|numeric|min:0.1|max:24",
            "peakhourstart" => "required|numeric|min:0.1|max:24",
            "peakhourend" => "required|numeric|min:0.1|max:24",
            "shop_id" => "required|int|min:1",
            "weekday_id" => "required|int|min:1",
        ]);

        if ($validator->fails()) {
            return API::responseErrorForm($validator);
        }

        $dayOpenning = new DayOpenning;
        $dayOpenning->fill((array)$params)
            ->save();

        return API::response($dayOpenning);

    }

    public function updateDayOpenning($shop_id, $weekday_id)
    {
        $params = json_decode(file_get_contents('php://input'));

        $params = ArrayUtil::merge($params, [
            "shop_id" => $shop_id,
            "weekday_id" => $weekday_id
        ]);

        $validator = ApiUtil::validate($params, [
            "closed" => "boolean",
            "fullhourstart" => "numeric|min:0.1|max:24",
            "fullhourend" => "numeric|min:0.1|max:24",
            "peakhourstart" => "numeric|min:0.1|max:24",
            "peakhourend" => "numeric|min:0.1|max:24",
            "shop_id" => "int|min:1",
            "weekday_id" => "int|min:1",
        ]);

        if ($validator->fails()) {
            return API::responseErrorForm($validator);
        }

        $dayOpenning = DayOpenning::where([
            "shop_id" => $params->shop_id,
            "weekday_id" => $params->weekday_id
        ]);

        $dayOpenningSearched = $dayOpenning
            ->first()
            ->fill((array)$params);

        $dayOpenning->update($dayOpenningSearched->toArray());

        return API::response($dayOpenningSearched);

    }
}