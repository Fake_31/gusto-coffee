<?php

namespace App\Http\Controllers;

use App\Models\Shop;
use App\Util\ApiUtil;
use App\Helpers\ApiHelper as API;
use Illuminate\Http\Request;


class ShopController extends Controller
{
    public function getShop(Request $request, $shop_id = false)
    {
        if ($shop_id) {

            $validator = ApiUtil::validate(["id" => $shop_id], [
                "id" => "required|numeric|min:1"
            ]);

            if ($validator->fails()) {
                return API::responseErrorForm($validator);
            }

            $shop = Shop::find($shop_id);

	        return API::response($shop);

        } else {

            $params = $this->jsonParams($request);

            $validator = ApiUtil::validate($params, [
	            "user_id"     => "sometimes|int|min:1",
	            "language_id" => "sometimes|int|min:1",
            ]);

            if ($validator->fails()) {
                return API::responseErrorForm($validator);
            }

            $shop = Shop::select();

            if(isset($params->user_id)) {

                $shop = $shop->where("user_id", $params->user_id);
            }

            if(isset($params->language_id)) {
                $shop = $shop->where("language_id", $params->language_id);
            }

            return API::response($shop->get());
        }
    }

    public function storeShop()
    {
        $params = json_decode(file_get_contents('php://input'));

        $validator = ApiUtil::validate($params, [
            "label" => "required|string|min:1",
            "address" => "required|string|min:1",
            "latitude" => "numeric|min:0",
            "longitude" => "numeric|min:0",
            "phone" => "required|string|min:1",
            "published" => "required|boolean",
            "user_id" => "required|int|min:1",
            "language_id" => "required|int|min:1",
        ]);

        if ($validator->fails()) {
            return API::responseErrorForm($validator);
        }

        $shop = new Shop;
        $shop->fill((array)$params);
        $shop->save();

        return API::response($shop);
    }

    public function updateShop($shop_id)
    {
        $params = json_decode(file_get_contents('php://input'));
        $objectFromRoute = ApiUtil::getParamsUrlRequest($params);

        $validator = ApiUtil::validate($objectFromRoute, [
            "label" => "required|string|min:1",
            "address" => "required|string|min:1",
            "latitude" => "numeric|min:0",
            "longitude" => "numeric|min:0",
            "phone" => "required|string|min:1",
            "published" => "required|boolean",
            "user_id" => "required|int|min:1",
            "language_id" => "required|int|min:1",
        ]);

        if ($validator->fails()) {
            return API::responseErrorForm($validator);
        }

        $shop = Shop::find($shop_id);

        if ($shop) {

            $shop->update([
                "label" => $objectFromRoute->label,
                "address" => $objectFromRoute->address,
                "latitude" => $objectFromRoute->latitude,
                "longitude" => $objectFromRoute->longitude,
                "phone" => $objectFromRoute->phone,
                "published" => $objectFromRoute->published,
                "user_id" => $objectFromRoute->user_id,
                "language_id" => $objectFromRoute->language_id,
            ]);

            return API::response($shop);

        } else {

            return API::responseError(400);

        }
    }

    public function deleteShop($shop_id)
    {
        $validator = ApiUtil::validate(["id" => $shop_id], [
            "id" => "required|numeric|min:1"
        ]);

        if ($validator->fails()) {
            return API::responseErrorForm($validator);
        }

        $shop = Shop::find($shop_id);

        if (!$shop) {

            return API::response();

        }

        $deleted = $shop->delete();

        if (!$deleted) {

            return API::responseError(304);

        }

        return API::response($shop);

    }

}
