<?php

namespace App\Http\Controllers;

use App\Exceptions\UserException;
use App\Helpers\ApiHelper as API;
use App\Http\Middleware\Authenticate;
use App\Models\BlackItem;
use App\Models\Reservation;
use App\Models\BlackItem;
use App\Models\Shop;
use App\Models\User;
use App\Util\ApiUtil;
use App\Util\DateUtil;
use App\Util\StringUtil;
use Illuminate\Http\Request;
use App\Jobs\SendMail;
use App\Models\Mail;

class UserController extends Controller
{

    const USER_TYPE_USER = 1;

    const USER_TYPE_BARMAN = 2;

    const USER_TYPE_MANAGER_ = 3;

    const USER_TYPE_ADMIN = 4;

    const EMAIL_DESACTIVATED_STATE = 1;

    const EMAIL_ACTIVATED_STATE = 0;

    const EMAIL_DEFAULT_STATE = 0;

    /**
     * @api               {get} /api/v1/user/login Try to authenticate user
     * @apiDescription    This request will
     *                          - Check if data are valid,
     *                          - Generate token with user'data and send to client
     *
     * @apiVersion        1.0.0
     * @apiName           User-login
     * @apiGroup          User
     *
     *
     * @apiSuccessExample Success-Response:
     *      HTTP/1.1 200 OK
     *     {
     *        user : User,
     *        token: string
     *     }
     * }
     */
    public function login(Request $request)
    {
        $params = $this->jsonParams($request);

        $validator = ApiUtil::validate($params, [
            "login" => "required|string",
            "password" => "required|string",
        ]);

        if ($validator->fails()) {
            return API::responseErrorForm($validator);
        }

        $login = $params->login;
        $password = $this->getEncryptedPassword($params->password);

        $user = User::where([
            "email" => $login,
            "password" => $password
        ])
            ->first();

        if ($user) {

            if (!$this->isEmailChecked($user)) {
                throw new UserException(403, 1);
            }
            if (!$this->isActive($user)) {
                throw new UserException(403, 2);
            }

            $response = [
                "user" => $user,
                "token" => Authenticate::createTokenFromUser($user)
            ];

            return API::response($response);

        } else {
            throw new UserException(403, 3);
        }

    }

    public function getUsers(Request $request)
    {


        $params = $this->jsonParams($request);

        $validator = ApiUtil::validate($params, [
	        "ids"                 => "nullable|array|min:1",
	        "usertype_id"         => "nullable|int|min:1",
	        "shop_id"             => "nullable|int|min:1",
	        "tag_ids"             => "nullable|array|min:1",
	        "name"                => "nullable|string|min:1",
	        "email"               => "nullable|string|min:1",
	        "exclude_blacklisted" => "sometimes|nullable|int|min:1",
        ]);

        if ($validator->fails()) {
            return API::responseErrorForm($validator);
        }

        $ids = isset($params->ids) ? $params->ids : false;
        $usertype_id = isset($params->usertype_id) ? $params->usertype_id : false;
        $shop_id = isset($params->shop_id) ? $params->shop_id : false;
        $name = isset($params->name) ? $params->name : false;
        $email = isset($params->email) ? $params->email : false;
        $tag_ids = isset($params->tag_ids) ? $params->tag_ids : false;
        $blackListSourceUserId = isset($params->exclude_blacklisted) ? $params->exclude_blacklisted : false;

        $user = User::select();

        if ($ids && count($ids) > 0) {
            $user = $user->whereIn("user.id", $ids);
        }

        if ($tag_ids && count($tag_ids) > 0) {

            $user = $user->leftJoin('usertag', function ($join) {
                $join->on('usertag.user_id', 'user.id');
            });

            $user = $user->orWhereIn("usertag.tag_id", $tag_ids);

        }

        if ($name) {
            $user = $user->where("user.name", 'like', "%" . $name . "%");
        }

        if ($email) {
            $user = $user->where("user.email", 'like', "%" . $email . "%");
        }

        if ($usertype_id) {
            $user = $user->where("user.usertype_id", $usertype_id);
        }

        if ($shop_id) {

            $user = $user->leftJoin('shop', function ($join) {
                $join->on('shop.user_id', 'user.id');
            });

            $user = $user->where("shop.id", $shop_id);
        }

        if ($blackListSourceUserId)
        {
	        $blackItems = BlackItem::where('sourceuser_id', $blackListSourceUserId)->get();
	        $blackListedUsersIds = [];
	        foreach ($blackItems as $blackItem)
	        {
		        $blackListedUsersIds[] = $blackItem->targetuser_id;
	        }
	        if (!empty($blackListedUsersIds))
	        {
		        $user->whereNotIn('id', $blackListedUsersIds);
	        }
        }

        $user = $user->groupBy("user.id")->get([
            "user.*"
        ]);

        return API::response($user);

    }

    public function getUser($user_id)
    {

        $validator = ApiUtil::validate(['user_id' => $user_id], [
            "user_id" => "required|int|min:1",
        ]);

        if ($validator->fails()) {
            return API::responseErrorForm($validator);
        }

        $user = User::find($user_id);

        return API::response($user);

    }

    public function getWorkspacesUsersReservations(Request $request)
    {
        $params = $this->jsonParams($request);

        $validator = ApiUtil::validate($params, [
            "workspace_id" => "required|int|min:1",
            "ranges" => "required|array|min:1"
        ]);

        if ($validator->fails()) {
            return API::responseErrorForm($validator);
        }

        $workspace_id = $params->workspace_id;
        $ranges = $params->ranges;

        $data = [
            "users" => [],
            "reservations" => []
        ];


        foreach ($ranges AS $range) {

            // Check if the object is well formed
            if (!isset($range->start) || !isset($range->end) ||
                !DateUtil::getFrDateTime('Y-m-d G:i:s', $range->start) || !DateUtil::getFrDateTime('Y-m-d G:i:s', $range->end)) {

                throw new UserException(403, 6);
            }

            $reservations = Reservation::where("workspace_id", $workspace_id)
                ->where(function ($query) use ($range) {
                    $query->whereBetween('reservation.startdate', [$range->start, $range->end])
                        ->orWhereBetween('reservation.enddate', [$range->start, $range->end]);
                })
                ->get();

            if ($reservations->count() > 0) {

                foreach ($reservations AS $reservation) {

                    $reservation_id = $reservation->id;

                    $user = User::leftJoin('bill', function ($join) {
                        $join->on('bill.user_id', 'user.id');
                    })
                        ->leftJoin('billreservation', function ($join) {
                            $join->on('billreservation.bill_id', 'bill.id');
                        })
                        ->where("billreservation.reservation_id", $reservation_id)
                        ->first(["user.*"]);

                    $userResult = isset($user) ? $user->toArray() : null;

                    array_push($data["reservations"], $reservation->toArray());
                    array_push($data["users"], $userResult);
                }
            }
        }

        return API::response($data);

    }

    public function getEmails()
    {

        $emails = User::get([
            "email"
        ]);

        header('Content-Type: application/excel');
        header('Content-Disposition: attachment; filename="emails.csv"');

        $fp = fopen('php://output', 'w');
        fputcsv($fp, ['email']);

        foreach ($emails as $email) {
            fputcsv($fp, [$email->email]);
        }

        fclose($fp);

    }

    public function getUserFromToken(Request $request)
    {

        $params = $this->jsonParams($request);

        $validator = ApiUtil::validate($params, [
            "verificationtoken" => "required|string"
        ]);

        $token = json_decode($params->verificationtoken);

        if ($validator->fails()) {
            return API::responseErrorForm($validator);
        }

        $user = User::where("email", $token->email)->first();

        return API::response($user);
    }

    public function passwordRecovery(Request $request)
    {
        $params = $this->jsonParams($request);

        $validator = ApiUtil::validate($params, [
            "email" => "required|email"
        ]);

        if ($validator->fails()) {
            return API::responseErrorForm($validator);
        }

        $email = $params->email;
        $token = $this->generateVerificationToken();

        $appUrl = url() . "/user/validate/" . $token;


        $user = User::where("email", $email)
            ->first();

        if ($user) {

            $data = array(
                'email' => $email,
                'url' => $appUrl
            );

            // Prépare email to queue
            $mail = new Mail;
            $mail->setTo($email);
            $mail->setTitle("Nouveau mot de passe");
            $mail->setContentVariables($data);
            $mail->setTemplate("emails.reset-password");

            // Send email to queue
            $this->dispatch(new SendMail($mail));

            $user->verificationtoken = $token;

            $user->save();

        } else {
            throw new UserException(403, 3);
        }

        return API::response($user);

    }

    public function validationFromToken(Request $request)
    {

        $params = $this->jsonParams($request);

        $validator = ApiUtil::validate($params, [
            "verificationtoken" => "required|string"
        ]);

        if ($validator->fails()) {
            return API::responseErrorForm($validator);
        }

        $verificationtoken = $params->verificationtoken;

        $user = User::where("verificationtoken", $verificationtoken)->first();

        if ($user) {

            if ($this->isEmailChecked($user)) {
                throw  new UserException(403, 4);
            }

            $user->verificationtoken = null;
            $user->emailchecked = 1;
            $user->save();

        } else {
            throw  new UserException(403, 5);
        }

        return API::response($user);

    }

    public function bannishUser($id)
    {

        $validator = ApiUtil::validate(["id" => $id], [
            "id" => "required|numeric|min:1"
        ]);

        if ($validator->fails()) {
            return API::responseErrorForm($validator);
        }

        $user = User::find($id);

        if ($user) {

            if (!$this->isActive($user)) {
                throw new UserException(403, 2);
            }

            $user = $this->desactivateUser($user);
            $user->save();

        }

        return API::response($user);

    }

    public function clearUser($id)
    {

        $validator = ApiUtil::validate(["id" => $id], [
            "id" => "required|numeric|min:1"
        ]);

        if ($validator->fails()) {
            return API::responseErrorForm($validator);
        }

        $user = User::find($id);

        if ($user) {

            $user = $this->clearUserInformations($user);
            $user->save();

        }

        return API::response($user);
    }


    public function createUser()
    {
        $params = json_decode(file_get_contents('php://input'));

        $validator = ApiUtil::validate($params, [
	        "email"             => "required|email",
	        "password"          => "required|string",
	        "name"              => "string",
	        "firstname"         => "string",
	        "usertype_id"       => "int",
	        "companyname"       => "nullable|string",
	        "address"           => "nullable|string",
	        "zipcode"           => "nullable|string",
	        "avatar"            => "nullable|string",
	        "notificationdelay" => "nullable|numeric|min:0.1",
	        "country_id"        => "nullable|int",
	        "linkedin"          => "nullable|string",
	        "emailchecked"      => "nullable|boolean",
	        "shop_id"           => "nullable|int",
        ]);

        if ($validator->fails()) {
            return API::responseErrorForm($validator);
        }

        $token = $this->generateVerificationToken();

        $user = new User;
        $user->fill((array)$params);
        $user->password = $this->getEncryptedPassword($params->password);
        $user->usertype_id = isset($params->usertype_id) ? $params->usertype_id : self::USER_TYPE_USER;
        $user->emailchecked = isset($params->emailchecked) ? $params->emailchecked : self::EMAIL_DEFAULT_STATE;
        $shop_id = isset($params->shop_id) ? $params->shop_id : false;
        $user->creationdate = DateUtil::getFrDateTime("Y-m-d");
        $user->verificationtoken = $token;


        $userAdded = User::create($user->toArray());

        $appUrl = url() . "/user/validate/" . $token;
        $data = array(
            'name' => $user->name,
            'firstname' => $user->firstname,
            'url' => $appUrl
        );

        // Prépare email to queue
        $mail = new Mail;
        $mail->setTo($user->email);
        $mail->setTitle("[Gusto-Coffee] Inscription");
        $mail->setContentVariables($data);
        $mail->setTemplate("emails.subscription");

        // Send email to queue
        $this->dispatch(new SendMail($mail));

        if ($userAdded && $shop_id) {

            $shop = Shop::find($shop_id);

            if ($shop) {

                $shop->user_id = $userAdded->id;
                $shop->save();

            } else {

                return API::response();

            }

        }

        return API::response($userAdded);

    }

    public function updateUser($id)
    {
        $params = json_decode(file_get_contents('php://input'));

        $validator = ApiUtil::validate($params, [
	        "email"             => "email",
	        "password"          => "sometimes|string",
	        "name"              => "string",
	        "firstname"         => "string",
	        "companyname"       => "nullable|string",
	        "address"           => "nullable|string",
	        "zipcode"           => "nullable|string",
	        "avatar"            => "nullable|string",
	        "notificationdelay" => "nullable|numeric|min:0.1",
	        "country_id"        => "nullable|int",
	        "linkedin"          => "nullable|string",
	        "emailchecked"      => "nullable|boolean",
        ]);

        if ($validator->fails()) {
            return API::responseErrorForm($validator);
        }

        $user = new User;
        $user->fill((array)$params);

        if (isset($params->password))
        {
	        $user->password = $this->getEncryptedPassword($params->password);
        }

        $userFromDb = User::find($id);

        if ($userFromDb) {

            $userFromDb->update($user->toArray());
            return API::response($userFromDb);

        }

        return API::responseError();


    }


    private function desactivateUser($user)
    {

        $user->desactivated = self::EMAIL_DESACTIVATED_STATE;

        return $user;
    }

    private function clearUserInformations($user)
    {

        $user->password = "";
        $user->name = "";
        $user->firstname = "";
        $user->address = "";
        $user->email = $this->anonymizeEmail($user->email);
        $user->verificationtoken = "";

        $this->desactivateUser($user);

        return $user;

    }

    private function anonymizeEmail($email)
    {
	    $date         = DateUtil::getFrDateTime("Ymd");
	    $randomString = StringUtil::getRandomString(10, true, true, false);
	    $prefix       = "anonym.".$date.".".$randomString;

	    $emailAnonymized = preg_replace('/^[^@]+/', $prefix, $email);

	    return $emailAnonymized;
    }

    private function getEncryptedPassword($password)
    {
        return md5($password);
    }

    private function generateVerificationToken($nb = 20)
    {
        $string = "";
        $chaine = "a0b1c2d3e4f5g6h7i8j9klmnpqrstuvwxy123456789";
        srand((double)microtime() * 1000000);
        for ($i = 0; $i < $nb; $i++) {
            $string .= $chaine[rand() % strlen($chaine)];
        }
        return $string;
    }

    protected function isActive($user)
    {
        if ($user->desactivated) {
            return false;
        }
        return true;
    }

    protected function isEmailChecked($user)
    {
        if ($user->emailchecked) {
            return true;
        }
        return false;
    }

}
