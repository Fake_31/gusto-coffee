<?php

namespace App\Http\Controllers;

use App\Models\PaymentMethod;
use App\Util\ApiUtil;
use App\Helpers\ApiHelper as API;

class PaymentMethodController extends Controller
{

    public function getPaymentMethod($paymentmethod_id = false)
    {
        if ($paymentmethod_id) {

            $validator = ApiUtil::validate(["id" => $paymentmethod_id], [
                "id" => "required|int|min:1"
            ]);

            if ($validator->fails()) {
                return API::responseErrorForm($validator);
            }

            $paymentmethod = PaymentMethod::find($paymentmethod_id);

        } else {
            $paymentmethod = PaymentMethod::all();
        }

        return API::response($paymentmethod);

    }

    public function storePaymentMethod()
    {
        $params = json_decode(file_get_contents('php://input'));

        $validator = ApiUtil::validate($params, [
            "label" => "required|string|min:1",
        ]);

        if ($validator->fails()) {
            return API::responseErrorForm($validator);
        }
        $paymentmethod = new PaymentMethod();
        $paymentmethod->fill((array)$params);
        $paymentmethod->save();

        return API::response($paymentmethod);
    }

    public function updatePaymentMethod($paymentmethod_id)
    {
        $params = json_decode(file_get_contents('php://input'));
        $objectFromRoute = ApiUtil::getParamsUrlRequest($params);

        $validator = ApiUtil::validate($objectFromRoute, [
            "label" => "required|string|min:1",
        ]);

        if ($validator->fails()) {
            return API::responseErrorForm($validator);
        }

        $paymentmethod = PaymentMethod::find($paymentmethod_id);

        if ($paymentmethod) {

            $paymentmethod->update([
                "label" => $objectFromRoute->label
            ]);

            return API::response($paymentmethod);

        } else {

            return API::responseError(400);

        }
    }

    public function deletePaymentMethod($paymentmethod_id)
    {
        $validator = ApiUtil::validate(["id" => $paymentmethod_id], [
            "id" => "required|int|min:1"
        ]);

        if ($validator->fails()) {
            return API::responseErrorForm($validator);
        }

        $paymentmethod = PaymentMethod::find($paymentmethod_id);

        if (!$paymentmethod) {

            return API::response();

        }

        $deleted = $paymentmethod->delete();

        if (!$deleted) {

            return API::responseError(304);

        }

        return API::response($paymentmethod);

    }

}