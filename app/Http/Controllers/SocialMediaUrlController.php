<?php

namespace App\Http\Controllers;

use App\Models\SocialMediaUrl;
use App\Util\ApiUtil;
use App\Util\ArrayUtil;
use App\Helpers\ApiHelper as API;

class SocialMediaUrlController extends Controller
{
    public function getShopSocialMediaUrl($shop_id = null, $socialmedia_id = null)
    {
        if ($shop_id && !$socialmedia_id) {

            $validator = ApiUtil::validate([
                "shop_id" => $shop_id,
            ], [
                "shop_id" => "required|int|min:1",
            ]);

            if ($validator->fails()) {
                return API::responseErrorForm($validator);
            }

            $socialMediaUrl = SocialMediaUrl::where([
                "shop_id" => $shop_id
            ])
                ->get();

        } elseif ($shop_id && $socialmedia_id) {

            $objectFromRoute = ApiUtil::getParamsUrlRequest();

            $validator = ApiUtil::validate($objectFromRoute, [
                "shop_id" => "required|int|min:1",
                "socialmedia_id" => "required|int|min:1",
            ]);

            if ($validator->fails()) {
                return API::responseErrorForm($validator);
            }

            $socialMediaUrl = SocialMediaUrl::where([
                "shop_id" => $shop_id,
                "socialmedia_id" => $socialmedia_id
            ])
                ->first();

        } else {

            $socialMediaUrl = SocialMediaUrl::all();

        }

        return API::response($socialMediaUrl);
    }

    public function storeSocialMediaUrl($shop_id, $socialmedia_id)
    {
        /*
         * Todo : Check if socialMediaUrl already exist
         */

        $params = json_decode(file_get_contents('php://input'));
        $params = ArrayUtil::merge($params, [
            "shop_id" => $shop_id,
            "socialmedia_id" => $socialmedia_id
        ]);

        $validator = ApiUtil::validate($params, [
            "url" => "required|string",
            "shop_id" => "required|numeric|min:1",
            "socialmedia_id" => "required|int|min:1",
        ]);

        if ($validator->fails()) {
            return API::responseErrorForm($validator);
        }

        $socialMediaUrl = new SocialMediaUrl;
        $socialMediaUrl->fill((array)$params)
            ->save();

        return API::response($socialMediaUrl);

    }

    public function updateSocialMediaUrl($shop_id, $socialmedia_id)
    {
        $params = json_decode(file_get_contents('php://input'));

        $params = ArrayUtil::merge($params, [
            "shop_id" => $shop_id,
            "socialmedia_id" => $socialmedia_id
        ]);

        $validator = ApiUtil::validate($params, [
            "url" => "required|string",
            "shop_id" => "required|numeric|min:1",
            "socialmedia_id" => "required|int|min:1",
        ]);

        if ($validator->fails()) {
            return API::responseErrorForm($validator);
        }

        $socialMediaUrl = SocialMediaUrl::where([
            "shop_id" => $params->shop_id,
            "socialmedia_id" => $params->socialmedia_id
        ]);

        $socialMediaUrlSearched = $socialMediaUrl
            ->first()
            ->fill((array)$params);

        $socialMediaUrl->update($socialMediaUrlSearched->toArray());

        return API::response($socialMediaUrlSearched);

    }

    public function deleteSocialMediaUrl($shop_id, $socialmedia_id)
    {
        $validator = ApiUtil::validate([
            "shop_id" => $shop_id,
            "socialmedia_id" => $socialmedia_id
        ], [
            "shop_id" => "required|int|min:1",
            "socialmedia_id" => "required|int|min:1",
        ]);

        if ($validator->fails()) {
            return API::responseErrorForm($validator);
        }

        $socialMediaUrl = SocialMediaUrl::where([
            "shop_id" => $shop_id,
            "socialmedia_id" => $socialmedia_id
        ]);

        $socialMediaUrlSaved = $socialMediaUrl->first();
        $socialMediaUrlDeleted = $socialMediaUrl->delete();

        if ($socialMediaUrlDeleted == 1) {
            return API::response($socialMediaUrlSaved);
        } else {
            return API::responseError(206);
        }
    }
}