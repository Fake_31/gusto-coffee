<?php

namespace App\Http\Controllers;

use App\Helpers\ApiHelper as API;
use App\Models\Tag;
use App\Util\ApiUtil;
use Illuminate\Http\Request;

class TagController extends Controller
{

    public function getTags(Request $request)
    {
	    $params = $this->jsonParams($request);
	    if ($params && isset($params->ids))
	    {
		    $validator = ApiUtil::validate($params, [
			    "ids" => "array"
		    ]);

		    if ($validator->fails())
		    {
			    return API::responseErrorForm($validator);
		    }

		    $tags = Tag::find($params->ids);
	    }
	    else
	    {
		    $tags = Tag::all();
	    }

	    return API::response($tags);
    }

    public function getTag($id)
    {
        $validator = ApiUtil::validate(["id" => $id], [
            "id" => "required|numeric|min:1"
        ]);

        if ($validator->fails()) {
            return API::responseErrorForm($validator);
        }

        $tag = Tag::find($id);

        return API::response($tag);
    }

    public function storeTag()
    {

        $params = json_decode(file_get_contents('php://input'));
        $requestLabel = $params->label;

        $validator = ApiUtil::validate($params, [
            "label" => "required|string"
        ]);

        if ($validator->fails()) {
            return API::responseErrorForm($validator);

        }

        $tag = Tag::firstOrNew([
            "label" => $requestLabel
        ]);

        if ($tag->exists) {
            return API::responseError(409);

        } else {

            $tag->save();
            return API::response($tag);
        }


    }

    public function updateTag($id)
    {
        $params = json_decode(file_get_contents('php://input'));
        $label = $params->label;

        $objectFromRoute = ApiUtil::getParamsUrlRequest($params);

        $validator = ApiUtil::validate($objectFromRoute, [
            "id" => "required|numeric|min:0",
            "label" => "required|string|"
        ]);

        if ($validator->fails()) {
            return API::responseErrorForm($validator);
        }

        $tag = Tag::find($id);

        if ($tag) {

            $tag->label = $label;

            $tag->save();
            return API::response($tag);

        } else {

            return API::responseError(206);

        }

    }

    public function deleteTag($id)
    {
        /*
            Todo suppression en cascade des clefs étrangères
        */

        $validator = ApiUtil::validate(["id" => $id], [
            'id' => 'required|numeric|min:0',
        ]);

        if ($validator->fails()) {
            return API::responseErrorForm($validator);
        }


        $tag = Tag::find($id);

        if ($tag) {

            $tag->delete();
            return API::response($tag);

        } else {
            return API::responseError(400);
        }
    }

}