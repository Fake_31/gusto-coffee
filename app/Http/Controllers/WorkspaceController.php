<?php

namespace App\Http\Controllers;

use App\Models\Workspace;
use App\Helpers\ApiHelper as API;
use App\Util\ApiUtil;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class WorkspaceController extends Controller
{
	const RESERVION_CANCELLED = 1;
	const RESERVION_COMPLETED = 1;

	public function getWorkspaces(Request $request)
	{

		$params = $this->jsonParams($request);

		if (isset($params))
		{

			$validator = ApiUtil::validate($params, [
				"shop_id" => "required|int|min:1"
			]);

			if ($validator->fails())
			{
				return API::responseErrorForm($validator);
			}

			$shop_id    = $params->shop_id;
			$workspaces = Workspace::where("shop_id", $shop_id);
		}
		else
		{
			$workspaces = Workspace::select();
		}

		return API::response($workspaces->get());
	}

	public function getWorkspace($workspace_id)
	{

		$validator = ApiUtil::validate(["workspace_id" => $workspace_id], [
			"workspace_id" => "required|int|min:1"
		]);

		if ($validator->fails())
		{
			return API::responseErrorForm($validator);
		}

		return API::response(Workspace::find($workspace_id));
	}

	public function storeWorkspace()
	{
		$params = json_decode(file_get_contents('php://input'));

		$validator = ApiUtil::validate($params, [
			"label"       => "required|string|min:1",
			"placescount" => "required|int|min:1",
			"shop_id"     => "required|int|min:1",
		]);

		if ($validator->fails())
		{
			return API::responseErrorForm($validator);
		}

		$workspace = new Workspace();
		$workspace->fill((array) $params);
		$workspace->save();

		return API::response($workspace);
	}

	public function updateWorkspace()
	{
		$params          = json_decode(file_get_contents('php://input'));
		$objectFromRoute = ApiUtil::getParamsUrlRequest($params);

		$validator = ApiUtil::validate($objectFromRoute, [
			"label"        => "required|string|min:1",
			"placescount"  => "required|int|min:1",
			"shop_id"      => "required|int|min:1",
			"workspace_id" => "required|int|min:1",
		]);

		if ($validator->fails())
		{
			return API::responseErrorForm($validator);
		}

		$workspace = Workspace::find($objectFromRoute->workspace_id);

		if ($workspace)
		{

			$workspace->fill((array) $objectFromRoute);
			$workspace->save();

			return API::response($workspace);
		}
		else
		{

			return API::responseError(400);
		}
	}

	public function deleteWorkspace($workspace_id)
	{
		$validator = ApiUtil::validate(["workspace_id" => $workspace_id], [
			'workspace_id' => 'required|integer|min:1',
		]);

		if ($validator->fails())
		{
			return API::responseErrorForm($validator);
		}

		$workspace = Workspace::find($workspace_id);

		if (!$workspace)
		{

			return API::response(null);
		}

		$deleted = $workspace->delete();

		if (!$deleted)
		{

			return API::responseError(304);
		}

		return API::response($workspace);
	}

	public function getAvailables(Request $request)
	{

		$params = $this->jsonParams($request);

		$validator = ApiUtil::validate($params, [
			"shop_id" => "required|int|min:1",
			"ranges"  => "required|array|min:1",
		]);

		if ($validator->fails())
		{
			return API::responseErrorForm($validator);
		}

		$shop_id = $params->shop_id;
		$ranges  = $params->ranges;

		$workspaces = Workspace::leftJoin('reservation', function ($join)
		{
			$join->on('workspace.id', 'reservation.workspace_id');
		});

		$workspaces = $workspaces->where("workspace.shop_id", $shop_id);

		// reservations filter
		$workspaces->where(function ($query) use ($ranges)
		{
			$query->whereNull('reservation.id');

			$query->orWhere(function ($query) use ($ranges)
			{
				$query->where("reservation.cancelled", "!=", WorkspaceController::RESERVION_CANCELLED)
				      ->where("reservation.completed", "!=", WorkspaceController::RESERVION_COMPLETED);

				foreach ($ranges as $range)
				{
					$query->whereNotBetween('reservation.startdate', [$range->start, $range->end])
					      ->whereNotBetween('reservation.enddate', [$range->start, $range->end]);
				}
			});
		});;

		$workspaces
			->orderBy("workspace.id", "desc")
			->groupBy("workspace.id");

		$result = $workspaces->get(["workspace.*"]);

		return API::response($result);
	}

	public function isAvailable(Request $request, $workspace_id)
	{
		$params          = $this->jsonParams($request);
		$objectFromRoute = ApiUtil::getParamsUrlRequest($params);
		$validator       = ApiUtil::validate($objectFromRoute, [
			"range"        => "required",
			"workspace_id" => "required|int|min:1",
		]);

		if ($validator->fails())
		{
			return API::responseErrorForm($validator);
		}

		$range = $params->range;

		$workspace = Workspace::leftJoin('reservation', function ($join)
		{
			$join->on('workspace.id', 'reservation.workspace_id');
		})
		                      ->where("workspace.id", $workspace_id)
		                      ->where("reservation.cancelled", "!=", WorkspaceController::RESERVION_CANCELLED)
		                      ->whereNotBetween('startdate', [$range->start, $range->end])
		                      ->whereNotBetween('enddate', [$range->start, $range->end]);

		$workspace = $workspace->first();

		if ($workspace == null)
		{
			return API::response(false);
		}

		return API::response(true);
	}

	public function getOccupied(Request $request)
	{
		$params = $this->jsonParams($request);

		$validator = ApiUtil::validate($params, [
			"shop_id" => "required|int|min:1",
			"ranges"  => "required|array|min:1",
		]);

		if ($validator->fails())
		{
			return API::responseErrorForm($validator);
		}

		$shop_id = $params->shop_id;
		$ranges  = $params->ranges;

		$workspaces = Workspace::leftJoin('reservation', function ($join)
		{
			$join->on('workspace.id', 'reservation.workspace_id');
		});

		$workspaces = $workspaces->where("workspace.shop_id", $shop_id);
		$workspaces = $workspaces->where("reservation.cancelled", "!=", WorkspaceController::RESERVION_CANCELLED);

		foreach ($ranges as $range)
		{
			$workspaces = $workspaces->whereBetween('startdate', [$range->start, $range->end])
			                         ->whereBetween('enddate', [$range->start, $range->end]);
		}

		$workspaces = $workspaces->select('workspace.*');
		$workspaces = $workspaces->orderBy("workspace.id", "desc")
		                         ->groupBy("workspace.id")
		                         ->get();

		return API::response($workspaces);
	}

	public function getOccupiedNow(Request $request)
	{
		$params = $this->jsonParams($request);

		$validator = ApiUtil::validate($params, [
			"shop_id" => "required|int|min:1",
		]);

		if ($validator->fails())
		{
			return API::responseErrorForm($validator);
		}

		$shop_id = $params->shop_id;

		$workspaces = Workspace::join('reservation', function ($join)
		{
			$join->on('workspace.id', 'reservation.workspace_id');
		})
		                       ->where("workspace.shop_id", $shop_id)
		                       ->where("reservation.cancelled", "!=", WorkspaceController::RESERVION_CANCELLED)
		                       ->where("reservation.completed", "!=", WorkspaceController::RESERVION_COMPLETED)
		                       ->where('startdate', '<', DB::raw('now() + INTERVAL 1 HOUR'))
		                       ->select('workspace.*')
		                       ->orderBy("workspace.id", "desc")
		                       ->get();

		return API::response($workspaces);
	}

}