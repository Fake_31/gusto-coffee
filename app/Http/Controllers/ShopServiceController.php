<?php

namespace App\Http\Controllers;

use App\Models\ShopService;
use App\Util\ApiUtil;
use App\Helpers\ApiHelper as API;

class ShopServiceController extends Controller
{
    public function getShopServices($shop_id = null, $service_id = null)
    {
        if ($shop_id && !$service_id) {

            $validator = ApiUtil::validate([
                "shop_id" => $shop_id,
            ], [
                "shop_id" => "required|int|min:1"
            ]);

            if ($validator->fails()) {
                return API::responseErrorForm($validator);
            }

            $shopServices = ShopService::where([
                "shop_id" => $shop_id
            ])
                ->get();

        } elseif ($shop_id && $service_id) {

            $objectFromRoute = ApiUtil::getParamsUrlRequest();

            $validator = ApiUtil::validate($objectFromRoute, [
                "shop_id" => "required|int|min:1",
                "service_id" => "required|int|min:1"
            ]);

            if ($validator->fails()) {
                return API::responseErrorForm($validator);
            }

            $shopServices = ShopService::where([
                "shop_id" => $shop_id,
                "service_id" => $service_id
            ])->first();

        } else {

            $shopServices = ShopService::all();

        }

        return API::response($shopServices);
    }

    public function storeShopService($shop_id, $service_id)
    {
        $params = ApiUtil::getParamsUrlRequest();

        $validator = ApiUtil::validate([
            "shop_id" => $shop_id,
            "service_id" => $service_id
        ], [
            "shop_id" => "required|int|min:1",
            "service_id" => "required|int|min:1"
        ]);

        if ($validator->fails()) {
            return API::responseErrorForm($validator);
        }

        $shopService = new ShopService;
        $shopService->fill((array)$params)
            ->save();

        return API::response($shopService);
    }

    public function deleteShopService($shop_id, $service_id)
    {
        $validator = ApiUtil::validate([
            "shop_id" => $shop_id,
            "service_id" => $service_id
        ], [
            "shop_id" => "required|int|min:1",
            "service_id" => "required|int|min:1"
        ]);

        if ($validator->fails()) {
            return API::responseErrorForm($validator);
        }

        $shopService = ShopService::where([
            "shop_id" => $shop_id,
            "service_id" => $service_id
        ]);

        $shopServiceSearched = $shopService->first();

        if ($shopService->delete() == 0) {
            return API::response();
        }

        return API::response($shopServiceSearched);
    }
}