<?php

namespace App\Http\Controllers;

use App\Models\ClosingDay;
use App\Util\ApiUtil;
use Illuminate\Http\Request;
use App\Helpers\ApiHelper as API;

class ClosingDayController extends Controller
{
	public function getClosingDay($closingDayId)
	{
		return API::response(ClosingDay::find($closingDayId));
	}

    public function getClosingDays(Request $request)
    {
        $params = $this->jsonParams($request);

        $validator = ApiUtil::validate($params, [
            "shop_id" => "int|min:1",
        ]);

        if ($validator->fails()) {
            return API::responseErrorForm($validator);
        }

        $shop_id = isset($params->shop_id) ? $params->shop_id : false;

        if ($shop_id) {
            $closingday = ClosingDay::where("shop_id", $shop_id)->get();
        } else {
            $closingday = ClosingDay::all();
        }

        return API::response($closingday);
    }

    public function storeClosingDay()
    {
        /*
         * Todo : Check if date already stored for this shop
         */
        $params = json_decode(file_get_contents('php://input'));

        $validator = ApiUtil::validate($params, [
            "shop_id" => "required|int|min:1",
            "dates" => "required|array|min:1",
            "dates.*" => "required|date|date_format:Y-m-d",
        ]);

        if ($validator->fails()) {
            return API::responseErrorForm($validator);
        }

        $shop_id = $params->shop_id;
        $dates = $params->dates;

        $closingdayArray = [];

        foreach ($dates as $date) {

            $closingday = new ClosingDay;
            $closingday->date = $date;
            $closingday->shop_id = $shop_id;

            array_push($closingdayArray, $closingday->toArray());
        }

        ClosingDay::insert($closingdayArray);

        return API::response($closingdayArray);
    }

    public function deleteClosingDay($closingday_id)
    {

        $validator = ApiUtil::validate(["closingday_id" => $closingday_id], [
            "closingday_id" => "required|int|min:1",
        ]);

        if ($validator->fails()) {
            return API::responseErrorForm($validator);
        }

        $closingday = ClosingDay::find($closingday_id);

        if($closingday) {
            $closingday->delete();

            return API::response($closingday);
        }
        
        return API::responseError(403);

    }

}