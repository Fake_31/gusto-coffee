<?php

namespace App\Http\Controllers;

use App\Models\WeekDayLabel;
use App\Util\ApiUtil;
use App\Util\ArrayUtil;
use App\Helpers\ApiHelper as API;

class WeekDayLabelController extends Controller
{
    public function getWeekDayLabel($language_id = null, $weekday_id = null)
    {
        if ($language_id && !$weekday_id) {

            $validator = ApiUtil::validate([
                "language_id" => $language_id,
            ], [
                "language_id" => "required|int|min:1",
            ]);

            if ($validator->fails()) {
                return API::responseErrorForm($validator);
            }

            $weekDayLabel = WeekDayLabel::where([
                "language_id" => $language_id
            ])
                ->get();

        } elseif ($language_id && $weekday_id) {

            $objectFromRoute = ApiUtil::getParamsUrlRequest();

            $validator = ApiUtil::validate($objectFromRoute, [
                "language_id" => "required|int|min:1",
                "weekday_id" => "required|int|min:1",
            ]);

            if ($validator->fails()) {
                return API::responseErrorForm($validator);
            }

            $weekDayLabel = WeekDayLabel::where([
                "language_id" => $language_id,
                "weekday_id" => $weekday_id
            ])
                ->first();

        } else {

            $weekDayLabel = WeekDayLabel::all();

        }

        return API::response($weekDayLabel);
    }

    public function storeDay($language_id, $weekday_id)
    {
        /*
         * Todo : Check if weekDayLabel already exist
         */
        $params = json_decode(file_get_contents('php://input'));
        $params = ArrayUtil::merge($params, [
            'language_id' => $language_id,
            'weekday_id' => $weekday_id,
        ]);

        $validator = ApiUtil::validate($params, [
            "language_id" => "required|int|min:1",
            "weekday_id" => "required|int|min:1",
            "label" => "required|string",
        ]);

        if ($validator->fails()) {
            return API::responseErrorForm($validator);
        }

        $weekDayLabel = new WeekDayLabel;
        $weekDayLabel->fill((array)$params)
            ->save();

        return API::response($weekDayLabel);

    }

    public function updateDay($language_id, $weekday_id)
    {
        $params = json_decode(file_get_contents('php://input'));
        $params = ArrayUtil::merge($params, [
            'language_id' => $language_id,
            'weekday_id' => $weekday_id,
        ]);

        $validator = ApiUtil::validate($params, [
            "language_id" => "required|int|min:1",
            "weekday_id" => "required|int|min:1",
            "label" => "required|string",
        ]);

        if ($validator->fails()) {
            return API::responseErrorForm($validator);
        }

        $weekDayLabel = WeekDayLabel::where([
            "language_id" => $params->language_id,
            "weekday_id" => $params->weekday_id
        ]);
        $weekDayLabelSearched = $weekDayLabel->first();
        $weekDayLabel->update([
            "label" => $params->label
        ]);

        $weekDayLabelSearched->label = $params->label;

        return API::response($weekDayLabelSearched);

    }

    public function deleteDay($language_id, $weekday_id)
    {
        $validator = ApiUtil::validate([
            "language_id" => $language_id,
            "weekday_id" => $weekday_id
        ], [
            "language_id" => "required|int|min:1",
            "weekday_id" => "required|int|min:1",
        ]);

        if ($validator->fails()) {
            return API::responseErrorForm($validator);
        }

        $weekDayLabel = WeekDayLabel::where([
            "language_id" => $language_id,
            "weekday_id" => $weekday_id
        ]);

        $weekDayLabelSaved = $weekDayLabel->first();
        $weekDayLabelDeleted = $weekDayLabel->delete();

        if ($weekDayLabelDeleted == 1) {
            return API::response($weekDayLabelSaved);
        } else {
            return API::responseError(206);
        }
    }
}