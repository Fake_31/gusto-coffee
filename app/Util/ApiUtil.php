<?php

/**
 * Created by PhpStorm.
 * User: kev
 * Date: 20/05/17
 * Time: 12:51
 */

namespace App\Util;

use Illuminate\Support\Facades\Validator;

class ApiUtil
{

    protected static $messages = [
        'array' => 'error{key}',
        'min' => 'error{key}',
        'max' => 'error{key}',
        'string' => 'error{key}',
        'numeric' => 'error{key}',
        'exists' => 'error{key}',
        'required' => 'error{key}',
    ];

    public static function validate($params, $rules)
    {

        $validator = Validator::make((array)$params, $rules, self::$messages);

        return $validator;
    }

    public static function getParamsUrlRequest($array = false)
    {
        $obj = app('request')->route()[2];

        if ($array) {
            foreach ($array as $key => $value) {
                $obj[$key] = $value;
            }
        }
        return (object) $obj;
    }

}