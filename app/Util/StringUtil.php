<?php

/**
 * Created by PhpStorm.
 * User: kev
 * Date: 20/05/17
 * Time: 12:51
 */

namespace App\Util;

class StringUtil
{

    public static function getRandomString($nb, $optUseMaj = false, $opdUseNb = false, $optUseSpecials = false) {

        $string = "";
        $chaine = "abcdefghijklmnpqrstuvwxy";

        if($optUseMaj) {
            $chaine = $chaine."ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        }
        if($opdUseNb) {
            $chaine = $chaine."123456789";
        }
        if($optUseSpecials) {
            $chaine = $chaine."-_=";
        }

        srand((double)microtime()*1000000);

        for($i=0; $i<$nb; $i++) {
            $string .= $chaine[rand()%strlen($chaine)];
        }

        return $string;

    }


}