<?php

/**
 * Created by PhpStorm.
 * User: kev
 * Date: 20/05/17
 * Time: 12:51
 */

namespace App\Util;

class DateUtil
{
    public static function getFrDateTime($format = false, $dateParam = false, $returnDate = false, $timezone = "+0") {
        
        if($dateParam != false) {
            $date = new \DateTime($dateParam);
        } else {
            $date = new \DateTime();
        }

        $dateTimeZone = new \DateTimeZone($timezone);
        $date->setTimezone($dateTimeZone);

        if (!$returnDate && $format) {
            return $date->format($format);
        }

        return $date;

    }

    public static function isNull($date)
    {

        $date = self::getFrDateTime("Y-m-d H:m:s", $date, true);

        if ($date->getTimestamp() < 0) {
            return false;
        }

        return true;

    }

}