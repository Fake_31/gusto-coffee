<?php

/**
 * Created by PhpStorm.
 * User: kev
 * Date: 20/05/17
 * Time: 12:51
 */

namespace App\Util;

use App\Models\PaymentMethod;
use App\Models\Reservation;
use App\Models\Service;
use App\Models\Shop;
use App\Models\Tag;
use App\Models\User;
use App\Models\WeekDay;
use App\Models\Workspace;
use Faker\Factory AS Faker;

class SeedUtil
{

    private $faker;

    /**
     * SeedUtil constructor.
     */
    public function __construct()
    {
        $this->faker = Faker::create();
    }

    public function getRandomUserTypeId()
    {
        return $this->faker->optional($weight = 0.9, $default = 1)->numberBetween(2, 4);
    }

    function getRandomCreditCardTypeId()
    {
        return $this->faker->numberBetween(1, 2);
    }

    public static function getUsersId()
    {
        return User::where("usertype_id", 1)->get(["id"]);
    }

    public static function getManagersId()
    {
        return User::where("usertype_id", 3)->get(["id"]);
    }

    public static function getFirstManagersId()
    {
        return User::where("usertype_id", 3)->first(["id"]);
    }

    public static function getTagsId()
    {
        return Tag::select()->get(["id"]);
    }

    public static function getWeeksDay()
    {
        return WeekDay::all();
    }

    public static function getRequiredServices()
    {
        return Service::where("optional", 1)->get(["id"]);
    }

    public static function getShopIds()
    {
        return Shop::all("id");
    }

    public static function getServicesIds()
    {
        return Service::all("id");
    }

    public static function getPaymentsMethodsIds()
    {
        return PaymentMethod::all("id");
    }

    public static function getWorkspacesIds()
    {
        return Workspace::all("id");
    }

    public static function isWorspaceAvailable($workspaceId, $startDate, $endDate)
    {

        $reservation = Reservation::select()
            ->leftJoin('workspace', function ($join) {
                $join->on('reservation.workspace_id', 'workspace.id');
            })
            ->whereBetween("reservation.startdate", [$startDate, $endDate])
            ->whereBetween("reservation.enddate", [$startDate, $endDate])
            ->first();

        if ($reservation) {
            return false;
        }

        return true;
    }

    public static function random($entity)
    {
        if ($entity->count() > 1) {

            $rand = rand(0, $entity->count() - 1);
            return $entity[$rand]->id;

        } else {

            return $entity[0]->id;

        }
    }

}