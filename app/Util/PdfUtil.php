<?php
/**
 * Created by PhpStorm.
 * User: kev
 * Date: 22/08/17
 * Time: 18:54
 */

namespace App\Util;

use App\Exceptions\PdfException;
use Illuminate\Support\Facades\App;

class PdfUtil
{
    const STORAGE_FOLDER = "../storage/app/Bill";
    const COMPLETED_DIR = "/Completed";
    const REFOUNDED_DIR = "/Refounded";

    public static function streamPdf($bill_id, $completed = true)
    {
        $stateDir = $completed ? self::COMPLETED_DIR : self::REFOUNDED_DIR;
        $filename = self::STORAGE_FOLDER . "/" . $stateDir . "/" . $bill_id . ".pdf";

        try {
            $content = file_get_contents($filename);
        } catch (\ErrorException $e) {
            throw new PdfException(403, 1);
        }

        header("Content-Disposition: inline; filename=$bill_id.pdf");
        header("Content-type: application/pdf");
        header('Cache-Control: private, max-age=0, must-revalidate');
        header('Pragma: public');

        echo $content;
    }

    public static function savePdf($bill_id, $html)
    {
        $pdf = App::make('dompdf.wrapper');
        $pdf->loadHTML($html);

        $path = self::STORAGE_FOLDER . self::COMPLETED_DIR;

        file_put_contents($path . "/" . $bill_id . ".pdf", $pdf->output());

        return true;
    }


}