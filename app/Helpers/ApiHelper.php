<?php

namespace App\Helpers;

use App\Util\ValidatorUtil;
use \Illuminate\Http\Request;

class ApiHelper
{
    static private $ERROR_NO_CONTENT = 206;
    static public $BASE_URL_RESPONSE = "Laravel Framework Lumen (5.4.6) (Laravel Components 5.4.*)";

    public static function response($data = [], $status = 200, $allowNull = false)
    {
        if (!is_null($data) || $allowNull) {
            $response = array("response" => $data);

            return response()->json($response, $status);

        } else {

            return self::responseError();
        }
    }

    public static function responseError($status = 403, $entity = null, $entityErrorCode = null)
    {
        $errorCode = $entityErrorCode ? $entityErrorCode : $status;
        $response = array("response" => array("contentid" => "error" . ucfirst($entity) . $errorCode));
        return response()->json($response, $status);
    }

    public static function apiResponse($message, $status = 500)
    {
        $response = array("response" => array("message" => $message));
        return response()->json($response, $status);
    }

    public static function responseErrorForm($validator, $status = 206)
    {
        $errorsResponseApi = array();

        $errorMessages = $validator->errors()->getMessages();

        foreach ($errorMessages as $key => $message) {

            array_push($errorsResponseApi, array(
                "field" => $key,
                "contentid" => $message[0]
            ));

        }


        $response = array("errors" => $errorsResponseApi);
        array_push($errorsResponseApi, $response);

        return self::response($response, 422);

    }

    public static function validationError($validator)
    {
        return self::response(null, $validator->errors(), self::$ERROR_NO_CONTENT);
    }

    public static function getRequestIP(Request $request)
    {
        if ($request->hasHeader('X-Forwarded-For')) {
            return $request->header('X-Forwarded-For');
        }

        if ($request->ip()) {
            return $request->ip();
        }

        return "N/A";
    }

}
