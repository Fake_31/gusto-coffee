<?php namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Console\Commands\TestDatabaseConnection;
use App\Console\Commands\CreateDatabase;

class CommandServiceProvider extends ServiceProvider
{

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('command.test.database.connection', function()
        {
            return new TestDatabaseConnection;
        });
            $this->app->singleton('command.create.database', function()
            {
                return new CreateDatabase;
            });

        $this->commands(
            'command.create.database'
        );
    }
}
