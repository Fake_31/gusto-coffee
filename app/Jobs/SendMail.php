<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Support\Facades\Mail;


class SendMail extends Job
{
    protected $mail;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($mail)
    {
        $this->mail = $mail;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $mail = $this->mail;

        Mail::send($this->mail->getTemplate(), $this->mail->getContentVariables(), function ($message) use ($mail) {
            $message->to($mail->getTo())->subject($mail->getTitle());
        });
    }
}