<?php

use App\Helpers\ApiHelper as API;

class BaseTest extends TestCase
{
    public function testApiRoot()
    {
      $response = $this->call('GET', env("API_DEFAULT_ROUTE"));
      $json = json_decode($response->getContent());

      $this->assertEquals($json->response->message, API::$BASE_URL_RESPONSE);

    }
}
