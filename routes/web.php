<?php

use App\Helpers\ApiHelper as API;
use App\Http\Middleware\Authenticate;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

// authentication middlewares
// used to restrict routes access to a given access level
// anonymous < user < barman < manager < admin
const ANONYMOUS_AUTHENTICATION = 'auth:'.Authenticate::ACCESS_LEVEL_ANONYMOUS;
const USER_AUTHENTICATION      = 'auth:'.Authenticate::ACCESS_LEVEL_USER;
const BARMAN_AUTHENTICATION    = 'auth:'.Authenticate::ACCESS_LEVEL_BARMAN;
const MANAGER_AUTHENTICATION   = 'auth:'.Authenticate::ACCESS_LEVEL_MANAGER;
const ADMIN_AUTHENTICATION     = 'auth:'.Authenticate::ACCESS_LEVEL_ADMIN;

$app->group([
	'prefix'     => env('API_DEFAULT_ROUTE', "/"),
	// check for each route that request was sent by the app
	'middleware' => ANONYMOUS_AUTHENTICATION
], function () use ($app)
{
	$app->get('/', function ()
	{
		return API::apiResponse(API::$BASE_URL_RESPONSE);
	});

	$app->group(['prefix' => 'workspace'], function () use ($app)
	{
		$app->get('available', 'WorkspaceController@getAvailables');
		$app->get('available/{workspace_id}', 'WorkspaceController@isAvailable');
		$app->get('occupied', 'WorkspaceController@getOccupied');
		$app->get('occupied/now', 'WorkspaceController@getOccupiedNow');
		$app->get('', 'WorkspaceController@getWorkspaces');
		$app->get('/{workspace_id}', 'WorkspaceController@getWorkspace');

		$app->group(['middleware' => MANAGER_AUTHENTICATION], function () use ($app)
		{
			$app->post('', 'WorkspaceController@storeWorkspace');
			$app->put('/{workspace_id}', 'WorkspaceController@updateWorkspace');
			$app->delete('/{workspace_id}', 'WorkspaceController@deleteWorkspace');
		});
	});

	$app->group(['prefix' => 'bill', 'middleware' => USER_AUTHENTICATION], function () use ($app)
	{
		$app->get('/user/recurrent/{user_id}', 'BillController@getRecurrentOrder');
		$app->get('/', 'BillController@getBills');
		$app->get('/{id}', 'BillController@getBill');
		$app->put('/cancel/{id}', 'BillController@cancelBill');
		$app->post('/', 'BillController@storeBill');
	});

	$app->group(['prefix' => 'user'], function () use ($app)
	{
		$app->get('login', 'UserController@login');
		$app->get('email', ['uses' => 'UserController@getEmails', 'middleware' => ADMIN_AUTHENTICATION]);
		$app->get('/workspace', 'UserController@getWorkspacesUsersReservations');
		$app->get('/token', 'UserController@getUserFromToken');
		$app->get('/password/recovery', 'UserController@passwordRecovery');
		$app->put('/validation', 'UserController@validationFromToken');
		$app->get('/', 'UserController@getUsers');
		$app->post('/', 'UserController@createUser');

		$app->group(['middleware' => USER_AUTHENTICATION], function () use ($app)
		{
			$app->get('/{user_id}', 'UserController@getUser');
			$app->put('/bannish/{id}', 'UserController@bannishUser');
			$app->put('/clear/{id}', 'UserController@clearUser');
			$app->put('/{id}', 'UserController@updateUser');
		});
	});

	$app->group(['prefix' => 'tag'], function () use ($app)
	{
		$app->get('/', 'TagController@getTags');
		$app->get('/{id}', 'TagController@getTag');
		$app->post('/', 'TagController@storeTag');
		$app->delete('/{id}', 'TagController@deleteTag');
	});

	$app->group(['prefix' => 'language'], function () use ($app)
	{
		$app->get('/', 'LanguageController@getAll');
		$app->get('/code/', 'LanguageController@getLanguageFromCode');
		$app->get('/{id}', 'LanguageController@getLanguage');

		$app->group(['middleware' => ADMIN_AUTHENTICATION], function () use ($app)
		{
			$app->post('/', 'LanguageController@storeLanguage');
			$app->put('/{id}', 'LanguageController@updateLanguage');
			$app->delete('/{id}', 'LanguageController@deleteLanguage');
		});
	});

	$app->group(['prefix' => 'news'], function () use ($app)
	{
		$app->get('/', 'NewsController@getNews');
		$app->get('/{news_id}', 'NewsController@getNewsFromId');

		$app->group(['middleware' => MANAGER_AUTHENTICATION], function () use ($app)
		{
			$app->post('/', 'NewsController@storeNews');
			$app->put('/{news_id}', 'NewsController@updateNews');
			$app->delete('/{news_id}', 'NewsController@deleteNews');
		});
	});

	$app->group(['prefix' => 'configuration'], function () use ($app)
	{
		$app->get('/', 'ConfigurationController@getConfiguration');

		$app->put('/', ['uses' => 'ConfigurationController@updateConfiguration', 'middleware' => ADMIN_AUTHENTICATION]);
	});

	$app->group(['prefix' => 'message', 'middleware' => USER_AUTHENTICATION], function () use ($app)
	{
		$app->get('/unread', 'MessageController@getUnreadMessages');
		$app->get('/{message_id}', 'MessageController@getMessage');
		$app->get('/', 'MessageController@getMessages');

		$app->post('/', 'MessageController@storeMessage');
		$app->put('/read', 'MessageController@markMessagesAsRead');
		$app->put('/{message_id}', 'MessageController@updateMessage');
		$app->delete('/{message_id}', 'MessageController@deleteMessage');
	});

	$app->group(['prefix' => 'creditcard', 'middleware' => USER_AUTHENTICATION], function () use ($app)
	{
		$app->get('/', 'CreditCardController@getCreditCards');
		$app->get('/{id}', 'CreditCardController@getCreditCard');

		$app->post('/', 'CreditCardController@storeCreditCard');
		$app->put('/{id}', 'CreditCardController@updateCreditCard');
		$app->delete('/{id}', 'CreditCardController@deleteCreditCard');
	});

	$app->group(['prefix' => 'closingday'], function () use ($app)
	{
		$app->get('/', 'ClosingDayController@getClosingDays');
		$app->get('/{id}', 'ClosingDayController@getClosingDay');

		$app->group(['middleware' => MANAGER_AUTHENTICATION], function () use ($app)
		{
			$app->post('/', 'ClosingDayController@storeClosingDay');
			$app->delete('/{closingday_id}', 'ClosingDayController@deleteClosingDay');
		});
	});


	$app->group(['prefix' => 'service'], function () use ($app)
	{
		$app->get('/', 'ServiceController@getShopsService');
		$app->post('/', 'ServiceController@storeService');
		$app->get('/user', 'ServiceController@getUserServices');
		$app->get('/{id}', 'ServiceController@getService');

		$app->put('/user/consume', ['uses' => 'ServiceController@consumeService', 'middleware' => BARMAN_AUTHENTICATION]);

		$app->group(['middleware' => ADMIN_AUTHENTICATION], function () use ($app)
		{
			$app->put('/{id}', 'ServiceController@updateService');
			$app->delete('/{id}', 'ServiceController@deleteService');
		});
	});

	$app->group(['prefix' => 'reservation', 'middleware' => USER_AUTHENTICATION], function () use ($app)
	{
		$app->get('/future', 'ReservationController@getFutureReservations');
		$app->get('/next/user', 'ReservationController@getNextUserReservation');
		$app->get('/current/user', 'ReservationController@getCurrentUserReservation');

		$app->group(['middleware' => BARMAN_AUTHENTICATION], function () use ($app)
		{
			$app->get('/exceeded', 'ReservationController@getExceededReservations');
			$app->get('/incomplete', 'ReservationController@getIncompleteReservations');
			$app->put('/complete/{reservation_id}', 'ReservationController@setReservationCompleted');
			$app->put('/current/complete', 'ReservationController@setCurrentReservationCompleted');
			$app->post('/anonymous', 'ReservationController@storeAnonymousReservation');
		});

		$app->get('/', 'ReservationController@getReservations');
		$app->get('/{id}', 'ReservationController@getReservation');
	});

	$app->group(['prefix' => 'weekdaylabel'], function () use ($app)
	{
		$app->get('/', 'WeekDayLabelController@getWeekDayLabel');
		$app->get('/{language_id}[/{weekday_id}]', 'WeekDayLabelController@getWeekDayLabel');

		$app->group(['middleware' => ADMIN_AUTHENTICATION], function () use ($app)
		{
			$app->delete('/{language_id}/{weekday_id}', 'WeekDayLabelController@deleteDay');
			$app->post('/{language_id}/{weekday_id}', 'WeekDayLabelController@storeDay');
			$app->put('/{language_id}/{weekday_id}', 'WeekDayLabelController@updateDay');
		});
	});

	$app->group(['prefix' => 'dayopenning'], function () use ($app)
	{
		$app->get('/', 'DayOpenningController@getDayOpenning');
		$app->get('/{shop_id}[/{weekday_id}]', 'DayOpenningController@getDayOpenning');

		$app->group(['middleware' => MANAGER_AUTHENTICATION], function () use ($app)
		{
			$app->delete('/{shop_id}/{weekday_id}', 'DayOpenningController@deleteDayopenning');
			$app->post('/{shop_id}/{weekday_id}', 'DayOpenningController@storeDayOpenning');
			$app->put('/{shop_id}/{weekday_id}', 'DayOpenningController@updateDayOpenning');
		});
	});

	$app->group(['prefix' => 'socialmediaurl'], function () use ($app)
	{
		$app->get('/', 'SocialMediaUrlController@getShopSocialMediaUrl');
		$app->get('/{shop_id}[/{socialmedia_id}]', 'SocialMediaUrlController@getShopSocialMediaUrl');

		$app->group(['middleware' => MANAGER_AUTHENTICATION], function () use ($app)
		{
			$app->delete('/{shop_id}/{socialmedia_id}', 'SocialMediaUrlController@deleteSocialMediaUrl');
			$app->post('/{shop_id}/{socialmedia_id}', 'SocialMediaUrlController@storeSocialMediaUrl');
			$app->put('/{shop_id}/{socialmedia_id}', 'SocialMediaUrlController@updateSocialMediaUrl');
		});
	});

	$app->group(['prefix' => 'usertag', 'middleware' => USER_AUTHENTICATION], function () use ($app)
	{
		$app->get('/', 'UserTagController@getUserTag');
		$app->get('/{user_id}[/{tag_id}]', 'UserTagController@getUserTag');
		$app->delete('/{user_id}/{tag_id}', 'UserTagController@deteleUserTag');
		$app->post('/{user_id}/{tag_id}', 'UserTagController@storeUserTag');
	});

	$app->group(['prefix' => 'servicelabel'], function () use ($app)
	{
		$app->get('/', 'ServicelLabelController@getServicesLabel');
		$app->get('/{language_id}[/{service_id}]', 'ServicelLabelController@getServicesLabel');

		$app->group(['middleware' => ADMIN_AUTHENTICATION], function () use ($app)
		{
			$app->delete('/{language_id}/{service_id}', 'ServicelLabelController@deleteServiceLabel');
			$app->delete('/{service_id}', 'ServicelLabelController@deleteLabelByServices');
			$app->put('/{language_id}/{service_id}', 'ServicelLabelController@updateServiceLabel');
			$app->post('/{language_id}/{service_id}', 'ServicelLabelController@storeServiceLabel');
		});
	});

	$app->group(['prefix' => 'content'], function () use ($app)
	{
		$app->get('/', 'ContentController@getContents');
		$app->get('/{language_id}[/{contentid}]', 'ContentController@getContents');

		$app->group(['middleware' => ADMIN_AUTHENTICATION], function () use ($app)
		{
			$app->delete('/{language_id}/{content_id}', 'ContentController@deleteContent');
			$app->put('/{language_id}/{content_id}', 'ContentController@updateContent');
			$app->post('/{language_id}/{content_id}', 'ContentController@storeContent');
		});
	});

	$app->group(['prefix' => 'blackitem', 'middleware' => USER_AUTHENTICATION], function () use ($app)
	{
		$app->get('/', 'BlackItemController@getBlackItem');
		$app->get('/{sourceuser_id}[/{targetuser_id}]', 'BlackItemController@getBlackItem');
		$app->delete('/{sourceuser_id}/{targetuser_id}', 'BlackItemController@deleteBlackItem');
		$app->post('/{sourceuser_id}/{targetuser_id}', 'BlackItemController@storeBlackItem');
	});

	$app->group(['prefix' => 'shopservice'], function () use ($app)
	{
		$app->get('/', 'ShopServiceController@getShopServices');
		$app->get('/{shop_id}[/{service_id}]', 'ShopServiceController@getShopServices');

		$app->group(['middleware' => MANAGER_AUTHENTICATION], function () use ($app)
		{
			$app->post('/{shop_id}/{service_id}', 'ShopServiceController@storeShopService');
			$app->delete('/{shop_id}/{service_id}', 'ShopServiceController@deleteShopService');
		});
	});


	$app->group(['prefix' => 'socialmedia'], function () use ($app)
	{
		$app->get('/{socialmedia_id}', 'SocialMediaController@getSocialMedia');
		$app->get('/', 'SocialMediaController@getSocialMedia');

		$app->group(['middleware' => ADMIN_AUTHENTICATION], function () use ($app)
		{
			$app->post('/', 'SocialMediaController@storeSocialMedia');
			$app->put('/{socialmedia_id}', 'SocialMediaController@updateSocialMedia');
			$app->delete('/{socialmedia_id}', 'SocialMediaController@deleteSocialMedia');
		});
	});

	$app->group(['prefix' => 'weekday'], function () use ($app)
	{
		$app->get('/{weekday_id}', 'WeekDayController@getWeekDay');
		$app->get('/', 'WeekDayController@getWeekDay');
	});

	$app->group(['prefix' => 'country'], function () use ($app)
	{
		$app->get('/{country_id}', 'CountryController@getCountry');
		$app->get('/', 'CountryController@getCountry');
	});

	$app->group(['prefix' => 'usertype'], function () use ($app)
	{
		$app->get('/{usertype_id}', 'UserTypeController@getUserType');
		$app->get('/', 'UserTypeController@getUserType');
	});

	$app->group(['prefix' => 'creditcardtype', 'middleware' => USER_AUTHENTICATION], function () use ($app)
	{
		$app->get('/{creditcardtype_id}', 'CreditCardTypeController@getCreditCardtype');
		$app->get('/', 'CreditCardTypeController@getCreditCardtype');
	});

	$app->group(['prefix' => 'paymentmethod', 'middleware' => USER_AUTHENTICATION], function () use ($app)
	{
		$app->get('/{paymentmethod_id}', 'PaymentMethodController@getPaymentMethod');
		$app->get('/', 'PaymentMethodController@getPaymentMethod');
	});

	$app->group(['prefix' => 'billreservation', 'middleware' => USER_AUTHENTICATION], function () use ($app)
	{
		$app->get('/', 'BillReservationController@getBillReservation');
		$app->get('/{bill_id}[/{reservation_id}]', 'BillReservationController@getBillReservation');
	});

	$app->group(['prefix' => 'billservice', 'middleware' => USER_AUTHENTICATION], function () use ($app)
	{
		$app->get('/', 'BillServiceController@getBillService');
		$app->get('/{bill_id}[/{service_id}]', 'BillServiceController@getBillService');
	});

	$app->group(['prefix' => 'shop'], function () use ($app)
	{
		$app->get('/{shop_id}', 'ShopController@getShop');
		$app->get('/', 'ShopController@getShop');
		$app->put('/{shop_id}', ['uses' => 'ShopController@updateShop', 'middleware' => MANAGER_AUTHENTICATION]);

		$app->group(['middleware' => ADMIN_AUTHENTICATION], function () use ($app)
		{
			$app->post('/', 'ShopController@storeShop');
			$app->delete('/{shop_id}', 'ShopController@deleteShop');
		});
	});

	$app->post('/file', 'FileController@storeFile');
	$app->post('/contact', 'ContactController@sendEmail');
});
